# User Profile Management

APIs for managing user profile and phone numbers

## api/all-book




> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/all-book" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/all-book"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
{
    "message": {
        "books": [
            {
                "id": 1,
                "isbn": "9789966346544",
                "title": "When no one is watching",
                "description": "Richard is an adolescent boy who is confronted with so many temptations. First, he tries to\r\ncheat in an exam after failing to\r\nrevise.",
                "file_type": null,
                "edition": 1,
                "publisher": "Moran (E. A.) Publishers Limited",
                "copyright": null,
                "language": "English",
                "price": "180",
                "author_id": null,
                "authors": [
                    "Ibrahim Omondi"
                ],
                "published": true,
                "publication_date": "2018-12-01",
                "embargo_date": null,
                "category_id": null,
                "level_two_category_id": 1,
                "level_three_category_id": 6,
                "level_four_category_id": null,
                "tag": null,
                "tags": [
                    {
                        "id": 1,
                        "slug": "grade-7",
                        "name": "Grade 7",
                        "suggest": false,
                        "count": 2,
                        "tag_group_id": null,
                        "description": null
                    }
                ],
                "image": "20210902201056.jpg",
                "created_at": "2021-09-02T20:10:56.000000Z",
                "updated_at": "2021-09-02T20:10:56.000000Z",
                "tagged": [
                    {
                        "id": 1,
                        "taggable_id": 1,
                        "taggable_type": "App\\Models\\Book",
                        "tag_name": "Grade 7",
                        "tag_slug": "grade-7",
                        "tag": {
                            "id": 1,
                            "slug": "grade-7",
                            "name": "Grade 7",
                            "suggest": false,
                            "count": 2,
                            "tag_group_id": null,
                            "description": null
                        }
                    }
                ]
            },
            {
                "id": 2,
                "isbn": "9789966346575",
                "title": "A taste of bitterness",
                "description": "In A Taste of Bitterness, Imelda is confronted with very tough choices. On one hand, there are the\r\nclassmates who expect her to join",
                "file_type": null,
                "edition": 1,
                "publisher": "Moran (E. A.) Publishers Limited",
                "copyright": "Moran (E. A.) Publishers Limited",
                "language": "English",
                "price": "200",
                "author_id": null,
                "authors": [
                    "Merceline Kakoro"
                ],
                "published": true,
                "publication_date": "2013-01-01",
                "embargo_date": null,
                "category_id": null,
                "level_two_category_id": 1,
                "level_three_category_id": 6,
                "level_four_category_id": null,
                "tag": null,
                "tags": [
                    {
                        "id": 2,
                        "slug": "grade-8",
                        "name": "Grade 8",
                        "suggest": false,
                        "count": 1,
                        "tag_group_id": null,
                        "description": null
                    }
                ],
                "image": "20210902202524.jpg",
                "created_at": "2021-09-02T20:25:24.000000Z",
                "updated_at": "2021-09-02T20:25:24.000000Z",
                "tagged": [
                    {
                        "id": 2,
                        "taggable_id": 2,
                        "taggable_type": "App\\Models\\Book",
                        "tag_name": "Grade 8",
                        "tag_slug": "grade-8",
                        "tag": {
                            "id": 2,
                            "slug": "grade-8",
                            "name": "Grade 8",
                            "suggest": false,
                            "count": 1,
                            "tag_group_id": null,
                            "description": null
                        }
                    }
                ]
            },
            {
                "id": 3,
                "isbn": "9789966346544",
                "title": "When no one is watching",
                "description": "When no one is watching",
                "file_type": null,
                "edition": 3,
                "publisher": "Moran (E. A.) Publishers Limited",
                "copyright": "Moran (E. A.) Publishers Limited",
                "language": "english",
                "price": "186",
                "author_id": null,
                "authors": [
                    "Ibrahim Omondi"
                ],
                "published": true,
                "publication_date": "2018-12-01",
                "embargo_date": null,
                "category_id": null,
                "level_two_category_id": 1,
                "level_three_category_id": 6,
                "level_four_category_id": null,
                "tag": null,
                "tags": [
                    {
                        "id": 1,
                        "slug": "grade-7",
                        "name": "Grade 7",
                        "suggest": false,
                        "count": 2,
                        "tag_group_id": null,
                        "description": null
                    }
                ],
                "image": "2073773129.jpg",
                "created_at": "2021-09-03T06:30:46.000000Z",
                "updated_at": "2021-09-03T06:30:46.000000Z",
                "tagged": [
                    {
                        "id": 3,
                        "taggable_id": 3,
                        "taggable_type": "App\\Models\\Book",
                        "tag_name": "Grade 7",
                        "tag_slug": "grade-7",
                        "tag": {
                            "id": 1,
                            "slug": "grade-7",
                            "name": "Grade 7",
                            "suggest": false,
                            "count": 2,
                            "tag_group_id": null,
                            "description": null
                        }
                    }
                ]
            },
            {
                "id": 4,
                "isbn": "9789966346285",
                "title": "Hannah and Tabitha\tGod is not selfish",
                "description": "These stories teach us that God answers our prayers.Hannah did not have a child",
                "file_type": null,
                "edition": 1,
                "publisher": "Moran (E. A.) Publishers Limited",
                "copyright": "Moran (E. A.) Publishers Limited",
                "language": "English",
                "price": "136",
                "author_id": null,
                "authors": [
                    "Dancan Sabwa,Leah Rotich"
                ],
                "published": true,
                "publication_date": "2013-01-01",
                "embargo_date": null,
                "category_id": null,
                "level_two_category_id": 1,
                "level_three_category_id": 4,
                "level_four_category_id": null,
                "tag": null,
                "tags": [],
                "image": "338489698.jpg",
                "created_at": "2021-09-10T09:35:38.000000Z",
                "updated_at": "2021-09-10T09:35:38.000000Z",
                "tagged": []
            }
        ]
    }
}
```
<div id="execution-results-GETapi-all-book" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-all-book"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-all-book"></code></pre>
</div>
<div id="execution-error-GETapi-all-book" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-all-book"></code></pre>
</div>
<form id="form-GETapi-all-book" data-method="GET" data-path="api/all-book" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-all-book', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-all-book" onclick="tryItOut('GETapi-all-book');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-all-book" onclick="cancelTryOut('GETapi-all-book');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-all-book" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/all-book</code></b>
</p>
</form>



