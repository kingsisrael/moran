<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/css/bootstrap.min.css"
          integrity="sha512-oc9+XSs1H243/FRN9Rw62Fn8EtxjEYWHXRvjS43YtueEewbS6ObfXcJNyohjHqVKFPoXXUxwc+q1K7Dee6vv9g=="
          crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
            integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/js/bootstrap.bundle.min.js"
            integrity="sha512-iceXjjbmB2rwoX93Ka6HAHP+B76IY1z0o3h+N1PeDtRSsyeetU3/0QKJqGyPJcX63zysNehggFwMC/bi7dvMig=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/crypto-js.min.js"
            integrity="sha512-nOQuvD9nKirvxDdvQ9OMqe2dgapbPB7vYAMrzJihw5m+aNcf0dX53m6YxM4LgA9u8e9eg9QX+/+mPu8kCNpV2A=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="https://checkout.jambopay.com/jambopay-styles-checkout.min.css" />
    <!-- <link rel="stylesheet" href="http://196.50.21.51:16220/jambopay-styles-checkout.min.css" /> -->

</head>


<script type="text/javascript" defer src="https://checkout.jambopay.com/jambopay-js-checkout.min.js"></script>
<!-- <script type="text/javascript" defer src="http://196.50.21.51:16220/jambopay-js-checkout.min.js"></script>  -->


<style>
    .font-weight-bold {
        font-weight: 500 !important;
    }

    .picture-div {
        height: 200px;
    }

    .detail-div {
        height: 80px;
    }

    .pointable {
        cursor: pointer;
    }
</style>
<body onload="deletePost()">

<div>
    <div class="p-4">

    </div>
    <div class="offset-md-3 col-md-6" style="display: none;">
        <div class="form-group">
            <label for="Amount">Book Title</label>
            <input type="text" class="form-control" id="book_name" value="" readonly>
        </div>
        <div class="form-group">
            <label for="Amount">Amount</label>
            <input type="number" class="form-control" id="Amount"  readonly>
        </div>
        <div class="form-group">
            <label for="Currency">Currency</label>
            <input type="text" id="Currency" class="form-control" value="KES">
        </div>
        <div class="form-group">
            <label for="CustomerEmail">Customer Email</label>
            <input type="email" class="form-control" id="CustomerEmail" value="user@jambopay.com">
        </div>
        <div class="form-group">
            <label for="CustomerPhone">Customer Phone</label>
            <input type="text" class="form-control" id="CustomerPhone" value="" readonly>
        </div>
        <div class="form-group">
            <label for="SuccessUrl">Success Url</label>
            <input type="text" class="form-control" id="SuccessUrl" value="https://moranpublishers.co.ke/successful-transaction">
        </div>
        <div class="form-group">
            <label for="CancelledUrl">Cancelled Url</label>
            <input type="text" class="form-control" id="CancelledUrl" value="https://moranpublilshers.co.ke/cancel-transaction">
        </div>
        <div class="form-group">
            <label for="token">Access Token</label>
            <textarea class="form-control" id="token"></textarea>
        </div>
        <div class="form-group">
            <button id="jambopay-checkout-btn" class="btn btn-primary">Checkout</button>
        </div>
    </div>

</div>

</div>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function deletePost() {
        var parts = window.location.href.split("/");
        var user_id = parts[parts.length - 4];
        var amount = parts[parts.length - 3];
        var book_id = parts[parts.length - 2];
        var name_book = decodeURI(parts[parts.length - 1]);

        var CustomerPhone='0727429744';
        var user_email='user@jambopay.com';

        document.getElementById('Amount').value = amount;
        document.getElementById('CustomerPhone').value = CustomerPhone;
        document.getElementById('book_name').value = decodeURI(name_book);

        const getRandomOrderNumber = () => {
            const rand = Math.floor(Math.random() * 10000) + 1000
            console.log(rand);
            return rand;
        }
        let _token   = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '/pay',
            type:"POST",
            data:{
                user_id:user_id,
                amount:amount,
                rand:getRandomOrderNumber().toString(),
                book_id:book_id,
                name_book:name_book,
                _token: _token
            },
            success:function(response){
                if(response.success){
                    const merchantDetails = {
                        OrderId: response.message,
                        CustomerEmail: response.user.email,
                        Currency: 'KES',
                        CustomerPhone: response.user.phone_number,
                        OrderAmount: response.book.price,
                        // BusinessEmail: 'demo@webtribe.com',
                        BusinessEmail: 'commerce@moranpublishers.co.ke',
                        CancelledUrl: 'https://moranpublishers.co.ke/v1/api/cancel-transaction',
                        CallBackUrl: 'https://moranpublishers.co.ke/checkout',
                        Description: 'Checkout description',
                        Checksum: 'sample checksum',
                        PhoneNumber: response.user.phone_number
                    }
                    // const demoClientKey = 'd3m07r183'
                    const demoClientKey = "4abb3108464d4cf4adaa8e5197ed6433ec38354657f7448f99a17f81f3979cf4dfaafb72515c418b9c46b1d85d46bc2e"
                    const checkSumVars = merchantDetails.CustomerEmail + merchantDetails.OrderId + merchantDetails.OrderAmount +
                    merchantDetails.Currency + merchantDetails.BusinessEmail + merchantDetails.CallBackUrl + merchantDetails.CancelledUrl + demoClientKey;
                    merchantDetails.Checksum = CryptoJS.SHA256(checkSumVars).toString();
                    // var tken="QWZOXH4wOc8V6slSXXbXVlYNphMBtw8h7dXyCO2wgxz1MIQKMOc1ooeS8S84wXKosn-5IXWsN162atfy4Ylsv1uoTAOhXTgsNDtSDt6cpAkk_-yHXk7u918PWWrNMEJpLk8FudqoGUaw_eS3ZHwNqG7bszPibc5UY7p6TlCoduNbS7HrGgRFGYHG5FIOyhQ9uc5RuWxgjQtvT9z4nk-W0y_RZYbJH2udLURZIor8cywJf55KKRPlZvl3mI164gibU3YY2JziLLGFzxVns2l9cg0jvLIfZp5wkOLoVX-MG0dGcc0YOurA3dnImxsn_6hb6gViB3qhmlj_0DwkqBBH4_LOozUnyO7Chyge7K_FY_Ie4xTyhnkhDWub0zuHtWkvgDnH0JKJbsShxzLyNu7BhnV0D6s-wKLU3AQo4zQO79zDqnf67I-aolbsOdtvT-EVwwSUaK2g1X6wosnW13zKaufxD0JOXBYLgu-WXuZRQg8krx_mbRuPC2oMG46ygyvn";
                    const merchantCredetials = {
                        token: response.token,
                        secretKey: 'secretKey'
                    }
                    const themeDetails = {
                        // primaryColor: '#925223',
                        // secondaryColor: '#000',
                        // textColor: '#47bbe9',
                        // successButtonColor: '#4A160B',
                        // successButtonTextColor: '#FFF',
                        // cancelButtonTextColor: '#345',
                        // payBtnLoaderColor: "#3F8EFC",
                        // cancelBtnLoaderColor: "#3F8EFC",
                        // activeModeOfPaymentColor: "#fff",
                    }
                    // alert(OrderId);
                    jambopayCheckout(merchantDetails, merchantCredetials, themeDetails);
                }else{
                    alert("Error")
                }
            },
            error:function(error){
                console.log(error)
            }
        });
    }
    
    $("#jambopay-checkout-btn").click(function(e){
        e.preventDefault();
    });
</script>

</body>
</html>
