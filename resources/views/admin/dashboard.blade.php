@extends('admin.layout.master')
@section('sub-header')
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Dashboard</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                {{-- <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div> --}}
                {{-- <span class="text-muted font-weight-bold mr-4">#XRS-45670</span>
                <a href="#" class="btn btn-light-warning font-weight-bolder btn-sm">Add New</a> --}}
                <!--end::Actions-->
            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <!--begin::Actions-->
                {{-- <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1" id="weekly-graphs">Weekly</a> --}}
                {{-- <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1" id="monthly-graphs">Month</a> --}}
                {{-- <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1" id="yearly-graphs">Year</a> --}}
                <!--end::Actions-->
                <!--begin::Daterange-->
                <a href="#" class="btn btn-sm btn-light font-weight-bold mr-2" id="kt_dashboard_daterangepicker" data-toggle="tooltip" title="Select dashboard daterange" data-placement="left">
                    <span class="text-muted font-size-base font-weight-bold mr-2" id="kt_dashboard_daterangepicker_title">Today</span>
                    <span class="text-primary font-size-base font-weight-bolder" id="kt_dashboard_daterangepicker_date">{{ now()->format('d m Y') }}</span>
                </a>
                <!--end::Daterange-->
                <!--begin::Dropdowns-->
                {{-- <div class="dropdown dropdown-inline" data-toggle="tooltip" title="Quick actions" data-placement="left">
                    <a href="#" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="svg-icon svg-icon-success svg-icon-lg">
                            <!--begin::Svg Icon | path:assets/media/svg/icons/Files/File-plus.svg-->
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon points="0 0 24 0 24 24 0 24" />
                                    <path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                    <path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" fill="#000000" />
                                </g>
                            </svg>
                            <!--end::Svg Icon-->
                        </span>
                    </a>
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-md dropdown-menu-right py-3">
                        <!--begin::Navigation-->
                        <ul class="navi navi-hover py-5">
                            <li class="navi-item">
                                <a href="#" class="navi-link">
														<span class="navi-icon">
															<i class="flaticon2-drop"></i>
														</span>
                                    <span class="navi-text">New Group</span>
                                </a>
                            </li>
                            <li class="navi-item">
                                <a href="#" class="navi-link">
														<span class="navi-icon">
															<i class="flaticon2-list-3"></i>
														</span>
                                    <span class="navi-text">Contacts</span>
                                </a>
                            </li>
                            <li class="navi-item">
                                <a href="#" class="navi-link">
														<span class="navi-icon">
															<i class="flaticon2-rocket-1"></i>
														</span>
                                    <span class="navi-text">Groups</span>
                                    <span class="navi-link-badge">
															<span class="label label-light-primary label-inline font-weight-bold">new</span>
														</span>
                                </a>
                            </li>
                            <li class="navi-item">
                                <a href="#" class="navi-link">
														<span class="navi-icon">
															<i class="flaticon2-bell-2"></i>
														</span>
                                    <span class="navi-text">Calls</span>
                                </a>
                            </li>
                            <li class="navi-item">
                                <a href="#" class="navi-link">
														<span class="navi-icon">
															<i class="flaticon2-gear"></i>
														</span>
                                    <span class="navi-text">Settings</span>
                                </a>
                            </li>
                            <li class="navi-separator my-3"></li>
                            <li class="navi-item">
                                <a href="#" class="navi-link">
														<span class="navi-icon">
															<i class="flaticon2-magnifier-tool"></i>
														</span>
                                    <span class="navi-text">Help</span>
                                </a>
                            </li>
                            <li class="navi-item">
                                <a href="#" class="navi-link">
														<span class="navi-icon">
															<i class="flaticon2-bell-2"></i>
														</span>
                                    <span class="navi-text">Privacy</span>
                                    <span class="navi-link-badge">
															<span class="label label-light-danger label-rounded font-weight-bold">5</span>
														</span>
                                </a>
                            </li>
                        </ul>
                        <!--end::Navigation-->
                    </div>
                </div> --}}
                <!--end::Dropdowns-->
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
@endsection
<style>
    .tile-stats .icon i {
        margin: 0;
        font-size: 60px;
        line-height: 0;
        vertical-align: bottom;
        padding: 0;
    }
    .tile-stats .icon {
        width: 20px;
        height: 20px;
        color: #BAB8B8;
        position: absolute;
        right: 53px;
        top: 22px;
        z-index: 1;
    }
    .tile-stats {
        position: relative;
        display: block;
        margin-bottom: 12px;
        /*border: 1px solid #E4E4E4;*/
        -webkit-border-radius: 5px;
        overflow: hidden;
        padding-bottom: 5px;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 5px;
        -moz-background-clip: padding;
        border-radius: 5px;
        background-clip: padding-box;
        background: #FFF;
        -webkit-transition: all 300ms ease-in-out;
        transition: all 300ms ease-in-out;
    }
    .tile-stats .icon {
        width: 20px;
        height: 20px;
        color: #BAB8B8;
        position: absolute;
        right: 53px;
        top: 22px;
        z-index: 1;
    }
    .tile-stats .count, .tile-stats h3, .tile-stats p {
        position: relative;
        margin: 0;
        margin-left: 10px;
        z-index: 5;
        padding: 0;
    }
    .tile-stats .count {
        font-size: 38px;
        font-weight: bold;
        line-height: 1.65857143;
    }
    .tile-stats .count {
        font-size: 38px;
        font-weight: bold;
        line-height: 1.65857143;
    }
    .tile-stats .icon i {
        margin: 0;
        font-size: 60px;
        line-height: 0;
        vertical-align: bottom;
        padding: 0;
    }
</style>
@section('content')
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6  ">
            <div class="glow hvr-glow animated flipInY col bg-light-warning px-6 py-8 rounded-xl mr-7 mb-7">
                <span class="svg-icon svg-icon-3x svg-icon-warning d-block my-2">
                    <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Media/Equalizer.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"></rect>
                            <rect fill="#000000" opacity="0.3" x="13" y="4" width="3" height="16" rx="1.5"></rect>
                            <rect fill="#000000" x="8" y="9" width="3" height="11" rx="1.5"></rect>
                            <rect fill="#000000" x="18" y="11" width="3" height="9" rx="1.5"></rect>
                            <rect fill="#000000" x="3" y="13" width="3" height="7" rx="1.5"></rect>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                    <span class="text-dark-75 font-weight-bolder font-size-h2 ml-18">{{ $total_weekly_payments }} Ksh</span>
                </span>
                <a href="{{route('admin.weekly-sales')}}" class="text-warning font-weight-bold font-size-h6">Weekly Sales</a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 ">
            <div class="glow hvr-glow animated flipInY  col bg-light-primary px-6 py-8 rounded-xl mb-7">
                <span class="svg-icon svg-icon-3x svg-icon-primary d-block my-2">
                    <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Communication/Add-user.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <polygon points="0 0 24 0 24 24 0 24"></polygon>
                            <path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                            <path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero"></path>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                    <span class="text-dark-75 font-weight-bolder font-size-h2 ml-18">{{$userCount}}</span>
                </span>
                <a href="{{route('admin.app-users.index')}}" class="text-primary font-weight-bold font-size-h6 mt-2">Total Users</a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="glow hvr-glow animated flipInY col bg-light-danger px-6 py-8 rounded-xl mr-7">
                <span class="svg-icon svg-icon-3x svg-icon-danger d-block my-2">
                    <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Design/Layers.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <polygon points="0 0 24 0 24 24 0 24"></polygon>
                            <path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
                            <path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"></path>
                        </g>
                    </svg>
                    <span class="text-dark-75 font-weight-bolder font-size-h2 ml-18">{{$books}}</span>
                </span>
                <a href="{{route('admin.books-bought')}}" class="text-danger font-weight-bold font-size-h6 mt-2">Books Bought</a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="glow hvr-glow animated flipInY col bg-light-success px-6 py-8 rounded-xl">
                <span class="svg-icon svg-icon-3x svg-icon-success d-block my-2">
                    <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Communication/Urgent-mail.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"></rect>
                            <path d="M12.7037037,14 L15.6666667,10 L13.4444444,10 L13.4444444,6 L9,12 L11.2222222,12 L11.2222222,14 L6,14 C5.44771525,14 5,13.5522847 5,13 L5,3 C5,2.44771525 5.44771525,2 6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,13 C19,13.5522847 18.5522847,14 18,14 L12.7037037,14 Z" fill="#000000" opacity="0.3"></path>
                            <path d="M9.80428954,10.9142091 L9,12 L11.2222222,12 L11.2222222,16 L15.6666667,10 L15.4615385,10 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 L9.80428954,10.9142091 Z" fill="#000000"></path>
                        </g>
                    </svg>
                    <span class="text-dark-75 font-weight-bolder font-size-h2 ml-18">{{ $total_sales }} Ksh</span>
                </span>
                <a href="{{route('admin.mpesa.index')}}" class="text-success font-weight-bold font-size-h6 mt-2">Cumulative Sales</a>
            </div>
        </div>
    </div>

    <div class="row" style=" border: 2px solid #eee;">
        <div class="col-lg-6 col-xxl-6" style="background-color: white; opacity: .8;border: 2px solid #eee;">
            <canvas id="mixed-chart" width="700" height="500" style="width: 700px; height: 500px;"></canvas>
        </div>
        <div class="col-lg-6 col-xxl-6" style="background-color: white;border: 2px solid #eee;">
            <canvas id="barChart"width="700" height="500" style="width: 700px; height: 500px;"></canvas>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Monthly Transacted Amount : {{$current}}</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="{{route('admin.dashboard.disbursed_payments_filter')}}" type="button" title="Filter payment records by past months" class="btn btn-secondary mr-2 font-weight-bolder">
                            <span class="svg-icon svg-icon-md">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <circle fill="#000000" cx="9" cy="15" r="6" />
                                        <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span> Filter
                        </a>
                    </div>

                    {{-- <div class="card-toolbar">
                       @if(auth()->user()->hasRole('admin'))
                           <form  class="form-inline row justify-content-center" method="post" action="{{route('admin.dashboard.post')}}">
                               @csrf
                               <div class="col-md-4" style=" margin-left: 90px !important;">
                                   <label for="month">Select Month</label>
                                   <div class="input-group">
                                       <input name='month' id='month' class="form-control" value="{{$current}}" type="month" min="2021-04" max="{{now()->format('Y-m')}}">
                                   </div>
                               </div>
                               <div class="col-md-3">
                                   <label for="region">Select Channel</label>
                                   <div class="input-group">
                                       <select class="js-example-basic-single form-control{{ $errors->has('channel') ? ' is-invalid' : '' }}" name="channel" id="channel" required>
                                           <option value="all">All</option>
                                           <option value="MPESA">MPESA</option>
                                           <option value="CARD">CARD</option>
                                           <option value="PAYPAL">PAYPAL</option>
                                       </select>
                                   </div>
                                   @if ($errors->has('channel'))
                                       <span class="text-danger" role="alert">
                                       <strong>{{ $errors->first('channel') }}</strong>
                                   </span>
                                   @endif
                               </div>
                               <div class="col-md-1">
                                   <button class="btn btn-primary">Filter</button>
                               </div>
                           </form>
                       @endif
                    </div> --}}
                </div>
                <div class="card-block">
                    <canvas id="transactionsChart" width="100%" height="40"></canvas>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="card">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Top 10 Most bought books</h3>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-head-custom table-vertical-center" id="most-bought-books">
                            <thead style="font-weight: bold">
                            <tr>
                                <th>Image</th>
                                <th>EAN</th>
                                <th>Title</th>
                                <th>Edition</th>
                                <th>Publication Date</th>
                                <th>Copyright</th>
                                <th>Language</th>
                                <th>Price</th>
                                <th>Copies Bought</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
    <script src="{{asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}"></script>
    <script>
        $('#most-bought-books').DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            ajax: APP_URL +'/admin/datatables/get-most-bought-books',
            dom: 'Bfrtip',
            columns: [
                // {data: 'image', name: 'image'},
                {
                    data: 'image',
                    name: 'image',
                    render: function(data, type, full, meta){
                        return "<img src={{ \Illuminate\Support\Facades\URL::to('/') }}/images/" + data + " width='90' />";
                    },
                    orderable: false
                },
                {data: 'isbn', name: 'isbn'},
                {data: 'title', name: 'title'},
                {data: 'edition', name: 'edition'},
                {data: 'publication_date', name: 'publication_date'},
                {data: 'copyright', name: 'copyright'},
                {data: 'language', name: 'language'},
                {data: 'price', name: 'price'},
                {data: 'total', name: 'total'},
            ],
        });
        let monthlyTransactionsChartData = {!! json_encode($transactionPerMonthChartData) !!}
        new Chart(document.getElementById("mixed-chart"), {
            type: 'bar',
            data: {
                labels: monthlyTransactionsChartData.months.reverse(),
                datasets: [
                    // {
                    //     label: "Transactions (Ksh.)",
                    //     type: "line",
                    //     borderColor: "#ee2a2b",
                    //     data: [408,547,675,734],
                    //     fill: false
                    // },
                    {
                        label: "Transactions Count",
                        type: "line",
                        borderColor: "#ee2a2b",
                        data: monthlyTransactionsChartData.transactionsCount.reverse(),
                        fill: false
                    },
                    // {
                    //     label: "Transactions Count",
                    //     type: "bar",
                    //     backgroundColor: "rgba(238,42,43, 0.8)",
                    //     data: [408,547,675,734],
                    // },
                    {
                        label: "Transactions (Ksh)",
                        type: "bar",
                        backgroundColor: "rgb(28, 69, 123)",
                        backgroundColorHover: "#1c457b",
                        data: monthlyTransactionsChartData.totalAmount.reverse()
                    }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: 'Revenue Growth Charts'
                },
                legend: { display: false }
            }
        });
    </script>
    <script>
        let chartData = {!! json_encode($usersPerMonthChartData) !!}
        new Chart(document.getElementById("barChart"), {
            type: 'bar',
            data: {
                labels: chartData.months.reverse(),
                datasets: [
                    {
                        label: "Total Users",
                        type: "bar",
                        backgroundColor: "#1c457b",
                        backgroundColorHover: "#1c457b",
                        data: chartData.totalUsers.reverse()
                    }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: 'Users Growth Bar Graph'
                },
                legend: { display: false }
            }
        });
    </script>
    {{-- <script>
        var ctx = document.getElementById("pieChart");
        var data = {
            datasets: [{
                data: [120, 50, 140, 180, 100],
                backgroundColor: [
                    "#1c457b",
                    "#9B59B6",
                    "#ee2a2b",
                    "#3498DB"
                ],
                label: 'My dataset' // for legend
            }],
            labels: [
                "Main Category",
                "Level Two Category",
                "Level Three Category",
                "Level Four category",
            ]
        };

        var pieChart = new Chart(ctx, {
            data: data,
            type: 'pie',
            otpions: {
                legend: false
            }
        });

    </script> --}}
    <script>
        /**
         * Transactions Graph
         **/
        ( function ( $ ) {
            let dailyTransactionsChart = {!! json_encode($dailyTransactions) !!}
            var repaymentsChart = {
                init: function () {
                    // -- Set new default font family and font color to mimic Bootstrap's default styling
                    Chart.defaults.global.defaultFontFamily = 'Poppins';
                    Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
                    Chart.defaults.global.defaultFontColor = '#292b2c';
                    this.ajaxGetPaymentsMonthlyData();
                },

                ajaxGetPaymentsMonthlyData: function () {
                    // var payments_month_array
                    // console.log( payments_month_array );
                    repaymentsChart.createCompletedPaymentsChart();
                },

                /**
                 * Created the Completed Payments Chart
                 */
                createCompletedPaymentsChart: function () {
                    var ctx = document.getElementById("transactionsChart");
                    var myLineChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: dailyTransactionsChart.days.reverse(), // The response got from the ajax request containing all month names in the database
                            datasets: [
                                {
                                    label: "Books Count",
                                    type: 'line',
                                    lineTension: 0.3,
                                    backgroundColor: "rgba(234,99,99,0.72)",
                                    borderColor: "rgba(238,42,43, 0.8)",
                                    pointBorderColor: "#fff",
                                    pointBackgroundColor: "rgba(238,42,43, 0.8)",
                                    pointRadius: 5,
                                    pointHoverRadius: 5,
                                    pointHoverBackgroundColor: "rgba(219,141,13, 0.8)",
                                    pointHitRadius: 20,
                                    pointBorderWidth: 2,
                                    yAxisID: 'A',
                                    data: dailyTransactionsChart.totalBooks.reverse() // The response got from the ajax request containing data for the completed jobs in the corresponding months
                                },
                                {
                                    label: "Total Paid (Ksh.)",
                                    type: 'bar',
                                    backgroundColor: "rgb(28, 69, 123)",
                                    borderColor: "rgba(162,31,37, 0.8)",
                                    pointRadius: 5,
                                    pointBackgroundColor: "rgba(171,53,58, 0.8)",
                                    pointBorderColor: "rgba(255,255,255,0.8)",
                                    pointHoverRadius: 5,
                                    pointHoverBackgroundColor: "rgba(171,53,58, 0.8)",
                                    pointHitRadius: 20,
                                    pointBorderWidth: 2,
                                    yAxisID: 'B',
                                    data: dailyTransactionsChart.totalAmount.reverse() // The response got from the ajax request containing data for the completed jobs in the corresponding months
                                }
                            ],
                        },
                        options: {
                            scales: {
                                xAxes: [{
                                    time: {
                                        unit: 'date'
                                    },
                                    gridLines: {
                                        display: false
                                    },
                                    ticks: {
                                        maxTicksLimit: 7
                                    }
                                }],
                                yAxes: [{
                                    id:'A',
                                    position: 'right',
                                    ticks: {
                                        min: 0,
                                        max: Math.max(...dailyTransactionsChart.totalBooks), // The response got from the ajax request containing max limit for y axis
                                        maxTicksLimit: 5
                                    },
                                    gridLines: {
                                        display:false
                                    }
                                },
                                    {id:'B',
                                        position: 'left',
                                        ticks: {
                                            min: 0,
                                            max: Math.max(...dailyTransactionsChart.totalAmount), // The response got from the ajax request containing max limit for y axis
                                            maxTicksLimit: 5
                                        },
                                        gridLines: {
                                            color: "rgba(0, 0, 0, .125)",
                                        }
                                    },
                                ],
                            },
                            legend: {
                                display: true
                            }
                        }
                    });
                }
            };
            repaymentsChart.init();
        } )( jQuery );
    </script>

@endsection
