@extends('admin.layout.master')
@section('styles')
    <style>

    </style>
@endsection
@section('content')
    <div class="card card-custom" style="margin-top: -5%;">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Registered Moran Publishers Books
                </h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                <a href="{{ route('admin.catalogue-books.create') }}" type="button" class="btn btn-primary font-weight-bolder">
                    <span class="svg-icon svg-icon-md">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24" />
                                <circle fill="#000000" cx="9" cy="15" r="6" />
                                <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>Register New Book
                </a>
                <!--end::Button-->
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-head-custom table-vertical-center" id="example">
                                <thead style="font-weight: bold">
                                <tr>
                                    <th>No</th>
                                    <th>Image</th>
                                    <th>EAN</th>
                                    <th>Title</th>
                                    <th>Edition</th>
                                    <th>Publication Date</th>
                                    <th>Copyright</th>
                                    {{--<th>Tag</th>--}}
                                    <th>Language</th>
                                    <th>Price</th>
                                    <th>Pages</th>
                                    <th>File Uploaded</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Image</th>
                                    <th>EAN</th>
                                    <th>Title</th>
                                    <th>Edition</th>
                                    <th>Publication Date</th>
                                    <th>Copyright</th>
                                    {{--<th>Tag</th>--}}
                                    <th>Language</th>
                                    <th>Price</th>
                                    <th>Pages</th>
                                    <th>File Uploaded</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
           $('#example').DataTable({
                orderCellsTop: true,
                fixedHeader: true,
                ajax: APP_URL +'/admin/datatables/get-app-books',
                dom: 'Bfrtip',
                buttons: [
                    'colvis','copy', 'excel', 'print',

                    {
                        extend: 'csvHtml5',
                        text: 'CSV',
                        exportOptions: {
                            columns: [0,1, 2,3,4, 5,6],
                        },
                        footer: true,
                    },
                    'pageLength'
                ],
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false, orderable: false},
                    // {data: 'image', name: 'image'},
                    {
                        data: 'image',
                        name: 'image',
                        render: function(data, type, full, meta){
                            return "<img src={{ \Illuminate\Support\Facades\URL::to('/') }}/images/" + data + " width='90' />";
                        },
                        orderable: false
                    },
                    {data: 'isbn', name: 'isbn'},
                    {data: 'title', name: 'title'},
                    {data: 'edition', name: 'edition'},
                    {data: 'publication_date', name: 'publication_date'},
                    {data: 'copyright', name: 'copyright'},
                    {data: 'language', name: 'language'},
                    {data: 'price', name: 'price'},
                    {data: 'pages', name: 'pages'},
                    {data: 'hasFile', name: 'hasFile'},
                    {data: 'Status', name: 'Status'},
                    {data: 'action', name: 'action'},
                ],
            });
    </script>
@endsection

