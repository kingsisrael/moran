@extends('admin.layout.master')
@section('styles')
    <style>
        .status-links {
            display: flex;
            margin: 5px 30px;
        }
        .status-links > a > .badge-danger {
            margin-left: 5px;
        }
    </style>
@endsection
@section('content')
    <!--end::Notice-->
    <div class="row" style="margin-top: -6%;">
        <div class="col bg-light-danger px-6 py-8 rounded-xl mr-7 mb-7">
            <span class="svg-icon svg-icon-3x svg-icon-info d-block my-2">
                <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Media/Equalizer.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"></rect>
                        <rect fill="#000000" opacity="0.3" x="13" y="4" width="3" height="16" rx="1.5"></rect>
                        <rect fill="#000000" x="8" y="9" width="3" height="11" rx="1.5"></rect>
                        <rect fill="#000000" x="18" y="11" width="3" height="9" rx="1.5"></rect>
                        <rect fill="#000000" x="3" y="13" width="3" height="7" rx="1.5"></rect>
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
            <a href="#" class="text-info font-weight-bold font-size-h6">Total Mpesa Transactions: {{ $transactionsCount }}</a>
        </div>
    </div>
    <!--begin::Card-->
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Transactions</h3>
            </div>
        </div>
        <div class="status-links">
            <a href="{{ route('admin.mpesa.index') }}">
                <span class="badge badge-success">View Completed Transactions</span>
            </a>
            <a href="{{ route('admin.mpesa.transactions.failed') }}">
                <span class="badge badge-danger">View Failed Transactions</span>
            </a>
        </div>
        <!--end::Button-->
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-bordered table-hover table-checkable mt-10" id="kt_datatable" style="margin-top: 13px !important">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Order Number</th>
                    <th>User Details</th>
                    <th>Book</th>
                    <th>Transaction ID</th>
                    <th>Amount Paid (KSH)</th>
                    <th>Payer PhoneNumber</th>
                    <th>Date Paid</th>
                    <th>Status</th>
                </tr>
                </thead>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->
    <div id="studentModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" id="student_form">
                    {{ csrf_field() }}
                    @csrf
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Transaction</h4>
                    </div>
                    <div class="modal-body">
                        <span id="form_output"></span>
                        <div class="form-group">
                            <label>Transaction ID</label>
                            <input type="text" name="mpesaReceiptNumber" placeholder="Mpesa Receipt Number" id="mpesaReceiptNumber" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Amount</label>
                            <input type="text" name="amount" placeholder="Amount" id="amount" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="text" name="phone_number" placeholder="Phone Number" id="phone_number" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <select required class="form-control form-control-lg form-control-solid" id="transaction-status" name="status">
                                <option value="1">Completed</option>
                                <option value="0">Failed</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id" value="" />
                        <input type="hidden" name="button_action" id="button_action" value="insert" />
                        <input type="submit" name="submit" id="action" value="Add" class="btn btn-info" />

                        <div class="form-group">
                            <button type="button" class="btn btn-light btn-elevate mr-3" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        'use strict';
        var KTDatatablesDataSourceAjaxClient = function() {
            var initTable1 = function() {
                var table = $('#kt_datatable').DataTable({
                    dom: 'Bfrtip',
                    "processing": true,
                    "serverSide": true,
                    buttons: [
                        {
                            extend: 'copyHtml5'
                        }, 
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                columns: ':visible'
                            },
                        },
                        {
                            extend: 'pdfHtml5',
                            exportOptions: 
                                {
                                    columns: ':visible'
                                },
                            orientation: 'landscape',
                            pageSize: 'TABLOID'
                        },
                        'colvis','pageLength'
                    ],
                    // "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    responsive: true,
                    ajax: {
                        url: '{!! route('admin.mpesa.data') !!}',
                        data: function (d) {
                            d.region_id = $('select[name=region_id]').val();
                            d.raw_material_id = $('select[name=raw_material_id]').val();
                        }
                    },
                    columns: [
                        {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false, orderable: false},
                        {data: 'order_number', name: 'order_number'},
                        {data: 'user_details', name: 'user_details'},
                        {data: 'book_name', name: 'order_number'},
                        {data: 'mpesaReceiptNumber', name: 'mpesaReceiptNumber'},
                        {data: 'amount', name: 'amount'},
                        {data: 'phoneNumber', name: 'phoneNumber'},
                        {data: 'transactionDate', name: 'transactionDate'},
                        {data: 'status', name: 'status'},
                    ],
                    columnDefs: [
                    ],
                });
                $('#filter_form').on('submit', function(e) {
                    table.draw();
                    e.preventDefault();
                });
            };
            return {
                //main function to initiate the module
                init: function() {
                    initTable1();
                },
            };
        }();
        jQuery(document).ready(function() {
            KTDatatablesDataSourceAjaxClient.init();
        });

        $(document).on('click', '.edit-transaction', function(e){
            e.preventDefault()
            var id = $(this).attr("id");
            $('#form_output').html('');
            $.ajax({
                url:"{{route('admin.ajaxdata.fetchtransaction')}}",
                method:'get',
                data:{id:id},
                dataType:'json',
                success:function(data)
                {
                    console.log(data);
                    $('#mpesaReceiptNumber').val(data.mpesaReceiptNumber);
                    $('#transaction-status > option[value="'+data.status+'"]').attr('selected', true);
                    $('#phone_number').val(data.phone_number);
                    $('#amount').val(data.amount);
                    $('#id').val(id);
                    $('#studentModal').modal('show');
                    $('#action').val('Edit');
                    $('.modal-title').text('Edit Transaction');
                    $('#button_action').val('update');
                }
            })
        });

        $('#student_form').on('submit', function(event){
            event.preventDefault();
            var form_data = $(this).serialize();
            console.log(form_data)
            $.ajax({
                url:"{{ route('admin.ajaxdata.updatetransaction') }}",
                method:"POST",
                data:form_data,
                dataType:"json",
                success:function(data)
                {
                    if(data.error.length > 0)
                    {
                        var error_html = '';
                        for(var count = 0; count < data.error.length; count++)
                        {
                            error_html += '<div class="alert alert-danger">'+data.error[count]+'</div>';
                        }
                        $('#form_output').html(error_html);
                    }
                    else
                    {
                        $('#form_output').html(data.success);
                        $('#kt_datatable').DataTable().ajax.reload()
                        setTimeout(() => {
                            $('#studentModal').modal('hide');
                            $('#student_form')[0].reset();
                        }, 3000);
                    }
                }
            })
        });
    </script>
@endsection


