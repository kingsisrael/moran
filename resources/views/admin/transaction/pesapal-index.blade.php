@extends('admin.layout.master')
@section('content')
    <!--end::Notice-->
    <div class="row" style="margin-top: -6%;">
        <div class="col bg-light-info px-6 py-8 rounded-xl mr-7 mb-7">
            <span class="svg-icon svg-icon-3x svg-icon-info d-block my-2">
                <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Media/Equalizer.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"></rect>
                        <rect fill="#000000" opacity="0.3" x="13" y="4" width="3" height="16" rx="1.5"></rect>
                        <rect fill="#000000" x="8" y="9" width="3" height="11" rx="1.5"></rect>
                        <rect fill="#000000" x="18" y="11" width="3" height="9" rx="1.5"></rect>
                        <rect fill="#000000" x="3" y="13" width="3" height="7" rx="1.5"></rect>
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
            <a href="#" class="text-info font-weight-bold font-size-h6">Total Transactions: {{ $transactionCount }}</a>
        </div>
    </div>
    <!--begin::Card-->
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Transactions</h3>
            </div>
        </div>
        <!--end::Button-->
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-bordered table-hover table-checkable mt-10" id="kt_datatable" style="margin-top: 13px !important">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Order NUmber</th>
                    <th>User</th>
                    <th>Book</th>
                    <th>Status</th>
                    <th>Receipt</th>
                    <th>Amount</th>
                    <th>Channel</th>
                    <th>Currency</th>
                    <th>Date</th>
                </tr>
                </thead>
            </table>
            <!--end: Datatable-->
        </div>
    </div>

    <!--end::Card-->
@endsection

@section('scripts')
    <script>
        'use strict';
        var KTDatatablesDataSourceAjaxClient = function() {
            var initTable1 = function() {
                var table = $('#kt_datatable').DataTable({
                    dom: 'Bfrtip',
                    "processing": true,
                    "serverSide": true,
                    buttons: [{extend: 'copyHtml5'}, {
                        extend: 'excelHtml5',
                        exportOptions: {columns: ':visible'},
                    },
                        {
                            extend: 'pdfHtml5',
                            exportOptions: {columns: ':visible'},
                            orientation: 'landscape',
                            pageSize: 'TABLOID'
                        },
                        'colvis','pageLength'],
                    responsive: true,
                    ajax: {
                        url: '{!! route('admin.pesapal.data') !!}',
                        data: function (d) {
                            d.region_id = $('select[name=region_id]').val();
                            d.raw_material_id = $('select[name=raw_material_id]').val();
                        }
                    },
                    columns: [
                        {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false, orderable: false},
                        {data: 'Order_Id', name: 'Order_Id'},
                        {data: 'user_details', name: 'user_details'},
                        {data: 'book_name', name: 'book_name'},
                        {data: 'Status', name: 'Status'},
                        {data: 'Receipt', name: 'Receipt'},
                        {data: 'Amount', name: 'Amount'},
                        {data: 'channel', name: 'channel'},
                        {data: 'Currency', name: 'Currency'},
                        {data: 'Timestamp', name: 'Timestamp'},


                    ],
                    columnDefs: [
                    ],
                });
                $('#filter_form').on('submit', function(e) {
                    table.draw();
                    e.preventDefault();
                });
            };
            return {
                //main function to initiate the module
                init: function() {
                    initTable1();
                },
            };
        }();
        jQuery(document).ready(function() {
            KTDatatablesDataSourceAjaxClient.init();
        });
    </script>
@endsection


