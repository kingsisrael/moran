@extends('admin.layout.master')
@section('styles')
@endsection
@section('content')
    <div class="d-flex flex-row" style="margin-top: -6%;">
        <div class="flex-row-fluid">
            <!--begin::Card-->
            <div class="card card-custom gutter-bs">
                <!--end::Header-->
                <!--Begin::Body-->
                <div class="card-body px-0">
                    <div class="tab-content pt-5">
                        <!--begin::Tab Content-->
                        <div class="tab-pane active" id="kt_apps_contacts_view_tab_2" role="tabpanel">
                            <div class="card card-custom"style="margin-top: -6%;">
                                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                    <div class="card-title">
                                        <h5 class="card-label">Level Two Category</h5>
                                    </div>
                                    <div class="card-toolbar">
                                        <!--begin::Button-->
                                        <button type="button" class="btn btn-primary font-weight-bolder" name="add" id="add_data">
											<span class="svg-icon svg-icon-md">
												<!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<rect x="0" y="0" width="24" height="24" />
														<circle fill="#000000" cx="9" cy="15" r="6" />
														<path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
													</g>
												</svg>
                                                <!--end::Svg Icon-->
											</span>Create Level Two Category
                                        </button>
                                    </div>
                                </div>
                                <!--end::Button-->
                                <div class="card-body">
                                    <!--begin: Datatable-->
                                    <table class="table table-bordered table-hover table-checkable mt-10" id="kt_datatable-two" style="margin-top: 13px !important">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Main Category Name</th>
                                            <th>Category Name</th>
                                            {{-- <th>Has Sub Category</th> --}}
                                            <th>Date Created</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                    </table>
                                    <!--end: Datatable-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Content-->
    </div>

    <div id="studentModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" id="student_form" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @csrf
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Level Two Category</h4>
                    </div>
                    <div class="modal-body">
                        <span id="form_output"></span>
                        <div class="form-group">
                            <label>Main Category</label>
                            <select name="category_id" id="category_id" class="form-control form-control-lg form-control-solid category select2" required>
                                @foreach($category as $main)
                                    <option value='{{ $main->id }}'>{{ $main->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Level Two Category Name</label>
                            <input type="text" name="name" id="name" class="form-control" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id" value="" />
                        <input type="hidden" name="button_action" id="button_action" value="insert" />
                        <input type="submit" name="submit" id="action" value="Add" class="btn btn-info" />

                        <div class="form-group">
                            <button type="button" class="btn btn-light btn-elevate mr-3" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="confirmModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title">Confirmation</h2>
                </div>
                <div class="modal-body">
                    <h4 align="center" style="margin:0;">Are you sure you want to remove this data?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" name="ok_button" id="ok_button" class="btn btn-danger">OK</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#kt_datatable-two').DataTable({
                responsive: true,
                destroy: true,
                retrieve: true,
                stateSave: true,
                "bDestroy": true,
                dom: 'Bfrtip',
                buttons: [{extend: 'copyHtml5'}, {
                    extend: 'excelHtml5',
                    exportOptions: {columns: ':visible'},
                },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {columns: ':visible'},
                        orientation: 'landscape',
                        pageSize: 'TABLOID'
                    },
                    'colvis','pageLength'],
                ajax: {
                    url: '{{route('admin.get-catalogue-two')}}',
                    type: 'GET',
                    data: {
                        pagination: {
                            perpage: 1000,
                        },
                    },
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false, orderable: false},
                    {data: 'category', name: 'category'},
                    {data: 'name', name: 'name'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action'},

                ],
                columnDefs: [
                    {
                        targets: -3,
                        render: function(data, type, full, meta) {
                            var is_active = {
                                false: {'title': 'No', 'class': ' label-light-danger'},
                                true: {'title': 'Yes', 'class': ' label-light-success'},
                                3: {'title': 'Direct', 'state': 'success'},
                            };

                            if (typeof is_active[data] === 'undefined') {
                                return data;
                            }
                            return '<span class="label label-lg font-weight-bold' + is_active[data].class + ' label-inline">' + is_active[data].title + '</span>';

                        },
                    },
                ],

            });

            // Add Level Two Category Form
            $('#add_data').click(function(){
                $('#studentModal').modal('show');
                $('#student_form')[0].reset();
                $('#form_output').html('');
                $('#button_action').val('insert');
                $('#action').val('Add');
                $('.modal-title').text('Add Level Two Category');
            });

            $('#student_form').on('submit', function(event){
                event.preventDefault();
                var form_data = new FormData(this);
                $.ajax({
                    url:"{{ route('admin.ajaxdata.postdatatwo') }}",
                    method:"POST",
                    data:form_data,
                   cache:false,
               contentType: false,
               processData: false,
                    success:function(data)
                    {
                        let response = JSON.parse(data);
                        if(response.error.length > 0)
                        {
                            var error_html = '';
                            for(var count = 0; count < data.error.length; count++)
                            {
                                error_html += '<div class="alert alert-danger">'+data.error[count]+'</div>';
                            }
                            $('#form_output').html(error_html);
                        }
                        else
                        {
                            $('#form_output').html(response.success);
                            $('#student_form')[0].reset();
                            $('#action').val('Add');
                            $('.modal-title').text('Add Main Category');
                            $('#button_action').val('insert');
                            $('#kt_datatable-two').DataTable().ajax.reload();
                            setTimeout(() => {
                                $('#studentModal').modal('hide');
                            }, 2000);
                        }
                    }
                })
            });

            $(document).on('click', '.edit', function(){
                var id = $(this).attr("id");
                $('#form_output').html('');
                $.ajax({
                    url:"{{route('admin.ajaxdata.two')}}",
                    method:'get',
                    data:{id:id},
                    dataType:'json',
                    success:function(data)
                    {
                        $('#name').val(data.name);
                        $('#sub_category').val(data.category);
                        $('#id').val(id);
                        $('#studentModal').modal('show');
                        $('#action').val('Update');
                        $('.modal-title').text('Edit Data');
                        $('#button_action').val('update');
                    }
                })
            });

            var user_id;

            $(document).on('click', '.delete', function(e){
                e.preventDefault()
                category_id = $(this).attr('id');
                $('#confirmModal').modal('show');
            });

            $('#ok_button').click(function(){
                $.ajax({
                    url:"{{route('admin.leveltwocategory.delete')}}",
                    method:'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data:{id: category_id},
                    dataType:'json',
                    beforeSend:function(){
                        $('#ok_button').text('Deleting...');
                    },
                    success:function(data)
                    {
                        $('#confirmModal').modal('hide');
                        $('#kt_datatable-two').DataTable().ajax.reload();
                        alert('Data Deleted');
                    }
                })
            });
        });
    </script>
@endsection

