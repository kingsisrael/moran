@extends('admin.layout.master')
@section('content')
    <!--end::Notice-->
    <!--begin::Card-->
    <div class="card card-custom" style="margin-top: -6%;">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Books Bought Details</h3>
            </div>
        </div>
        <!--end::Button-->
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-bordered table-hover table-checkable mt-10" id="kt_datatable" style="margin-top: 13px !important">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>User Name</th>
                    <th>Order Number</th>
                    <th>Book</th>
                    <th>Channel</th>
                    <th>Transaction ID</th>
                    <th>Amount Paid (KSH)</th>
                    <th>Payer PhoneNumber</th>
                    <th>Date Paid</th>
                    <th>Status</th>
                </tr>
                </thead>
            </table>
            <!--end: Datatable-->
        </div>
    </div>

    <!--end::Card-->
@endsection

@section('scripts')
    <script>
        'use strict';
        var KTDatatablesDataSourceAjaxClient = function() {
            var initTable1 = function() {
                var table = $('#kt_datatable').DataTable({
                    dom: 'Bfrtip',
                    "processing": true,
                    "serverSide": true,
                    buttons: [{extend: 'copyHtml5'}, {
                        extend: 'excelHtml5',
                        exportOptions: {columns: ':visible'},
                    },
                        {
                            extend: 'pdfHtml5',
                            exportOptions: {columns: ':visible'},
                            orientation: 'landscape',
                            pageSize: 'TABLOID'
                        },
                        'colvis','pageLength'],
                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    responsive: true,
                    ajax: {
                        url: '{!! route('admin.mpesa.data') !!}',
                        data: function (d) {
                            d.region_id = $('select[name=region_id]').val();
                            d.raw_material_id = $('select[name=raw_material_id]').val();
                        }
                    },
                    order:[0, 'asc'],
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'user_details', name: 'user_details'},
                        {data: 'order_number', name: 'order_number'},
                        {data: 'book_name', name: 'order_number'},
                        {data: 'channel', name: 'channel'},
                        {data: 'mpesaReceiptNumber', name: 'mpesaReceiptNumber'},
                        {data: 'amount', name: 'amount'},
                        {data: 'phoneNumber', name: 'phoneNumber'},
                        {data: 'transactionDate', name: 'transactionDate'},
                        {data: 'status', name: 'status'},
                        // {data: 'action', name: 'action', orderable: false, searchable: false}
                    ],
                    columnDefs: [
                    ],
                });
                $('#filter_form').on('submit', function(e) {
                    table.draw();
                    e.preventDefault();
                });
            };
            return {
                //main function to initiate the module
                init: function() {
                    initTable1();
                },
            };
        }();
        jQuery(document).ready(function() {
            KTDatatablesDataSourceAjaxClient.init();
        });
    </script>
@endsection


