@extends('admin.layout.master')
@section('styles')
@endsection
@section('content')
    <!--end::Notice-->
    <!--begin::Card-->
    <div class="card card-custom" style="margin-top: -5%;">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Registered System Admin
                </h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                <a href="{{ route('admin.app-admins.create') }}" type="button" class="btn btn-primary font-weight-bolder">
                        <span class="svg-icon svg-icon-md">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24" />
                                <circle fill="#000000" cx="9" cy="15" r="6" />
                                <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
                            </g>
                        </svg>
                            <!--end::Svg Icon-->
                    </span>Register New System Admin
                </a>
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-bordered table-hover table-checkable mt-10 datatable" id="kt_datatable" style="margin-top: 13px !important">
                <thead>
                <tr>
                    <th>#</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th>Role</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="smallModal" role="dialog">
        <div class="modal-dialog" style="min-height: 800px">
            <div class="modal-content">
                <form method="POST" action="" id="editStatus">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="POST">
                    <div class="modal-header">
                        <div class="modal-title h4">Status update for selected Admin</div>
                    </div>

                    <div class="modal-body overlay overlay-block cursor-default">
                        <div class="table-responsive">
                            <!--Table-->
                            <table class="table table-head-custom table-vertical-center overflow-hidden">
                                <thead>
                                <tr>
                                    <th>FIRSTNAME</th>
                                    <th>LASTNAME</th>
                                    <th>Role</th>
                                    <th>STATUS</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{--                                @foreach ($users as $user)--}}
                                <tr>
                                    <td id="fname"></td>
                                    <td id="lname"></td>
                                    <td id="role">
                                    </td>
                                    <td id="status">
                                        <span class="label label-lg label-inline label-light-danger"></span>
                                    </td>
                                </tr>
                                {{--                                @endforeach--}}
                                </tbody>
                                <!--Table body-->
                            </table>
                        </div>
                    </div>
                    <div class="form modal-footer">
                        <div class="form-group mr-2">
                            <select class="form-control" name="status" id="statusValue">
                                <option value="true">Active</option>
                                <option value="false">Suspended</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-light btn-elevate mr-3" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary btn-elevate">Update Status</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="smallModal" role="dialog">
        <div class="modal-dialog" style="min-height: 800px">
            <div class="modal-content">
                <form method="POST" action="" id="editStatus">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="POST">
                    <div class="modal-header">
                        <div class="modal-title h4">Status update for selected Admin</div>
                    </div>

                    <div class="modal-body overlay overlay-block cursor-default">
                        <div class="table-responsive">
                            <!--Table-->
                        </div>
                    </div>
                    <div class="form modal-footer">
                        <div class="form-group mr-2">
                            <select class="form-control" name="status" id="statusValue">
                                <option value="true">Active</option>
                                <option value="false">Suspended</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-light btn-elevate mr-3" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary btn-elevate">Update Status</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Delete Product Modal -->
    <div class="modal" id="DeleteProductModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Admin User Delete</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <h4>Are you sure want to delete this User?</h4>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="SubmitDeleteProductForm">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            var dataTable = $('#kt_datatable').DataTable({
                responsive: true,
                ajax: {
                    url: APP_URL +'/admin/datatables/get-admin-users',
                    type: 'GET',
                    data: {
                        pagination: {
                            perpage: 50,
                        },
                    },
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'first_name', name: 'first_name'},
                    {data: 'last_name', name: 'last_name'},
                    {data: 'email', name: 'email'},
                    {data: 'phone_number', name: 'phone_number'},
                    {data: 'role', name: 'role'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action'},
                ],
                columnDefs: [
                    {
                        width: '75px',
                        targets: -2,
                        render: function(data, type, full, meta) {
                            var is_active = {
                                false: {'title': 'Suspended', 'class': ' label-light-danger'},
                                true: {'title': 'Active', 'class': ' label-light-success'},
                                3: {'title': 'Direct', 'state': 'success'},
                            };

                            if (typeof is_active[data] === 'undefined') {
                                return data;
                            }
                            return '<span class="label label-lg font-weight-bold' + is_active[data].class + ' label-inline">' + is_active[data].title + '</span>';

                        },
                    },
                ],
            });

            // Get single product in EditModel
            $('.modelClose').on('click', function(){
                $('#EditProductModal').hide();
            });
            // Delete product Ajax request.
            var deleteID;
            $('body').on('click', '#getDeleteId', function(){
                deleteID = $(this).data('id');
               // $('#fname').html($(this).data('id'))
                // $('#lname').html(data['last_name'])
                // $('#role').html(data['role'])
            })
            $('#SubmitDeleteProductForm').click(function(e) {
                e.preventDefault();
                var id = deleteID;
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "delete-admin/"+id,
                    method: 'post',
                    success:function(data)
                    {
                        setTimeout(function(){
                            $('#DeleteProductModal').modal('hide');
                            $('#kt_datatable').DataTable().ajax.reload();
                        }, 20);
                    }
                });
            });
        });
    </script>

@endsection


