@extends('admin.layout.master')
@section('styles')
    <style>

    </style>
@endsection
@section('content')
    <div class="card card-custom" style="margin-top: -5%;">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">System Transactions Logs
                </h3>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-head-custom table-vertical-center" id="example">
                                <thead style="font-weight: bold">
                                <tr>
                                    <th>#</th>
                                    <th>Log Description</th>
                                    <th>Performed On</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
           var table = $('#example').DataTable( {
                orderCellsTop: true,
                fixedHeader: true,
                ajax: APP_URL +'/admin/datatables/get-transactions-logs',
                dom: 'Bfrtip',
                buttons: [
                    'colvis','copy', 'excel', 'print',
                    {
                        extend: 'csvHtml5',
                        text: 'CSV',
                        exportOptions: {
                            columns: [0,1, 2,3,4, 5,6],
                        },
                        footer: true,
                    },
                    'pageLength'
                ],
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false, orderable: false},
                    {data: 'description', name: 'description'},
                    {data: 'created_at', name: 'created_at'},
                ],

            } );
    </script>
@endsection

