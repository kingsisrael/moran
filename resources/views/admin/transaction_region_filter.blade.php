@extends('admin.layout.master')
@section('content')
       <div class="row" style="margin-top: -6%;">
                <div class="col-md-12">
                    <div class="card card-custom">
                        <div class="card-header flex-wrap border-0 pt-6 pb-0">
                            <div class="card-title">
                                <h3 class="card-label">Monthly Transacted Amount : {{$current}}</h3>
                            </div>
                                 <div class="card-toolbar">
                                                    @if(auth()->user()->hasRole('admin'))
                                                        <form  class="form-inline row justify-content-center" method="post" action="{{route('admin.dashboard.post')}}">
                                                            @csrf
                                                            <div class="col-md-4" style=" margin-left: 90px !important;">
                                                                <label for="month">Select Month</label>
                                                                <div class="input-group">
                                                                    <input name='month' id='month' class="form-control" value="{{$current}}" type="month" min="2021-04" max="{{now()->format('Y-m')}}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="region">Select Channel</label>
                                                                <div class="input-group">
                                                                    <select class="js-example-basic-single form-control{{ $errors->has('channel') ? ' is-invalid' : '' }}" name="channel" id="channel" required>
                                                                        <option value="all">All</option>
                                                                        <option value="MPESA">MPESA</option>
                                                                        <option value="CARD">CARD</option>
                                                                        <option value="PAYPAL">PAYPAL</option>
                                                                    </select>
                                                                </div>
                                                                @if ($errors->has('channel'))
                                                                    <span class="text-danger" role="alert">
                                                                    <strong>{{ $errors->first('channel') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>
                                                            <div class="col-md-1">
                                                                <button class="btn btn-primary">Filter</button>
                                                            </div>
                                                        </form>
                                                    @endif
                                                </div>
                        </div>

                        <div class="card-header">
                            <div class="card-header-right">
                                <ul class="list-unstyled card-option">
                                    <li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-block">
                            <canvas id="transactionsChart" width="100%" height="40"></canvas>
                        </div>
                    </div>
                </div>
            </div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
    <script>
        /**
         * Transactions Graph
         **/
        ( function ( $ ) {
            var repaymentsChart = {
                init: function () {
                    // -- Set new default font family and font color to mimic Bootstrap's default styling
                    Chart.defaults.global.defaultFontFamily = 'Poppins';
                    Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
                    Chart.defaults.global.defaultFontColor = '#292b2c';
                    this.ajaxGetPaymentsMonthlyData();
                },

                ajaxGetPaymentsMonthlyData: function () {
                    // var payments_month_array
                    // console.log( payments_month_array );
                    repaymentsChart.createCompletedPaymentsChart();
                },

                /**
                 * Created the Completed Payments Chart
                 */
                createCompletedPaymentsChart: function () {
                    var ctx = document.getElementById("transactionsChart");
                    var myLineChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ["04-May","06-May","11-May","13-May","26-May"], // The response got from the ajax request containing all month names in the database
                            datasets: [{
                                label: "Books Count",
                                type: 'line',
                                lineTension: 0.3,
                                backgroundColor: "rgba(234,99,99,0.72)",
                                borderColor: "rgba(238,42,43, 0.8)",
                                pointBorderColor: "#fff",
                                pointBackgroundColor: "rgba(238,42,43, 0.8)",
                                pointRadius: 5,
                                pointHoverRadius: 5,
                                pointHoverBackgroundColor: "rgba(219,141,13, 0.8)",
                                pointHitRadius: 20,
                                pointBorderWidth: 2,
                                yAxisID: 'A',
                                data: [4,40,5,8,20] // The response got from the ajax request containing data for the completed jobs in the corresponding months
                            },
                                {
                                    label: "Total Paid (Ksh.)",
                                    type: 'bar',
                                    backgroundColor: "rgb(28, 69, 123)",
                                    borderColor: "rgba(162,31,37, 0.8)",
                                    pointRadius: 5,
                                    pointBackgroundColor: "rgba(171,53,58, 0.8)",
                                    pointBorderColor: "rgba(255,255,255,0.8)",
                                    pointHoverRadius: 5,
                                    pointHoverBackgroundColor: "rgba(171,53,58, 0.8)",
                                    pointHitRadius: 20,
                                    pointBorderWidth: 2,
                                    yAxisID: 'B',
                                    data: ["901.50","22930.00","1800.00","8280.00","14000.00"] // The response got from the ajax request containing data for the completed jobs in the corresponding months
                                }
                            ],
                        },
                        options: {
                            scales: {
                                xAxes: [{
                                    time: {
                                        unit: 'date'
                                    },
                                    gridLines: {
                                        display: false
                                    },
                                    ticks: {
                                        maxTicksLimit: 7
                                    }
                                }],
                                yAxes: [{
                                    id:'A',
                                    position: 'right',
                                    ticks: {
                                        min: 0,
                                        max: 90, // The response got from the ajax request containing max limit for y axis
                                        maxTicksLimit: 5
                                    },
                                    gridLines: {
                                        display:false
                                    }
                                },
                                    {id:'B',
                                        position: 'left',
                                        ticks: {
                                            min: 0,
                                            max: 22940, // The response got from the ajax request containing max limit for y axis
                                            maxTicksLimit: 5
                                        },
                                        gridLines: {
                                            color: "rgba(0, 0, 0, .125)",
                                        }
                                    },
                                ],
                            },
                            legend: {
                                display: true
                            }
                        }
                    });
                }
            };
            repaymentsChart.init();
        } )( jQuery );
    </script>
@endsection
