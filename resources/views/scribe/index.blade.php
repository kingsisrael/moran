<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Laravel Documentation</title>

    <link href="https://fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="{{ asset("vendor/scribe/css/style.css") }}" media="screen" />
        <link rel="stylesheet" href="{{ asset("vendor/scribe/css/print.css") }}" media="print" />
        <script src="{{ asset("vendor/scribe/js/all.js") }}"></script>

        <link rel="stylesheet" href="{{ asset("vendor/scribe/css/highlight-darcula.css") }}" media="" />
        <script src="{{ asset("vendor/scribe/js/highlight.pack.js") }}"></script>
    <script>hljs.initHighlightingOnLoad();</script>

</head>

<body class="" data-languages="[&quot;bash&quot;,&quot;javascript&quot;]">
<a href="#" id="nav-button">
      <span>
        NAV
            <img src="{{ asset("vendor/scribe/images/navbar.png") }}" alt="-image" class=""/>
      </span>
</a>
<div class="tocify-wrapper">
                <div class="lang-selector">
                            <a href="#" data-language-name="bash">bash</a>
                            <a href="#" data-language-name="javascript">javascript</a>
                    </div>
        <div class="search">
        <input type="text" class="search" id="input-search" placeholder="Search">
    </div>
    <ul class="search-results"></ul>

    <ul id="toc">
    </ul>

            <ul class="toc-footer" id="toc-footer">
                            <li><a href="{{ route("scribe.postman") }}">View Postman collection</a></li>
                            <li><a href="{{ route("scribe.openapi") }}">View OpenAPI (Swagger) spec</a></li>
                            <li><a href='http://github.com/knuckleswtf/scribe'>Documentation powered by Scribe ✍</a></li>
                    </ul>
            <ul class="toc-footer" id="last-updated">
            <li>Last updated: October 21 2021</li>
        </ul>
</div>
<div class="page-wrapper">
    <div class="dark-box"></div>
    <div class="content">
        <h1>Introduction</h1>
<p>Moran  App API Documentation</p>
<p>This documentation aims to provide all the information you need to work with our API.</p>
<aside>As you scroll, you'll see code examples for working with the API in different programming languages in the dark area to the right (or as part of the content on mobile).
You can switch the language used with the tabs at the top right (or from the nav menu at the top left on mobile).</aside>
<script src="https://cdn.jsdelivr.net/npm/lodash@4.17.10/lodash.min.js"></script>
<script>
    var baseUrl = "http://localhost";
</script>
<script src="{{ asset("vendor/scribe/js/tryitout-2.7.10.js") }}"></script>
<blockquote>
<p>Base URL</p>
</blockquote>
<pre><code class="language-yaml">http://localhost</code></pre><h1>Authenticating requests</h1>
<p>This API is not authenticated.</p><h1>Endpoints</h1>
<h2>api/buy-book</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost/api/buy-book" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/buy-book"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre>
<div id="execution-results-POSTapi-buy-book" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-buy-book"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-buy-book"></code></pre>
</div>
<div id="execution-error-POSTapi-buy-book" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-buy-book"></code></pre>
</div>
<form id="form-POSTapi-buy-book" data-method="POST" data-path="api/buy-book" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-buy-book', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-buy-book" onclick="tryItOut('POSTapi-buy-book');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-buy-book" onclick="cancelTryOut('POSTapi-buy-book');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-buy-book" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/buy-book</code></b>
</p>
</form>
<h2>api/paypal</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost/api/paypal" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/paypal"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre>
<div id="execution-results-POSTapi-paypal" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-paypal"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-paypal"></code></pre>
</div>
<div id="execution-error-POSTapi-paypal" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-paypal"></code></pre>
</div>
<form id="form-POSTapi-paypal" data-method="POST" data-path="api/paypal" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-paypal', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-paypal" onclick="tryItOut('POSTapi-paypal');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-paypal" onclick="cancelTryOut('POSTapi-paypal');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-paypal" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/paypal</code></b>
</p>
</form><h1>Login</h1>
<p>APIs for user authentication</p>
<h2>Register User</h2>
<p>Registers user to the system.
A bearer token is provided after successful registration which you can store and use for authentication while</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost/api/register" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"first_name":"beatae","last_name":"quidem","email":"repellat","phone":"quia","password":"nostrum","password_confirm":"ea"}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/register"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "first_name": "beatae",
    "last_name": "quidem",
    "email": "repellat",
    "phone": "quia",
    "password": "nostrum",
    "password_confirm": "ea"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<div id="execution-results-POSTapi-register" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-register"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-register"></code></pre>
</div>
<div id="execution-error-POSTapi-register" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-register"></code></pre>
</div>
<form id="form-POSTapi-register" data-method="POST" data-path="api/register" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-register', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-register" onclick="tryItOut('POSTapi-register');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-register" onclick="cancelTryOut('POSTapi-register');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-register" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/register</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>first_name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="first_name" data-endpoint="POSTapi-register" data-component="body" required  hidden>
<br>
First Name.
</p>
<p>
<b><code>last_name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="last_name" data-endpoint="POSTapi-register" data-component="body" required  hidden>
<br>
Last Name.
</p>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="email" data-endpoint="POSTapi-register" data-component="body" required  hidden>
<br>
Email Address.
</p>
<p>
<b><code>phone</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="phone" data-endpoint="POSTapi-register" data-component="body" required  hidden>
<br>
Phone Number, prefixed.
</p>
<p>
<b><code>password</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="password" name="password" data-endpoint="POSTapi-register" data-component="body" required  hidden>
<br>
Password min 8 characters.
</p>
<p>
<b><code>password_confirm</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="password" name="password_confirm" data-endpoint="POSTapi-register" data-component="body" required  hidden>
<br>
Password, must match password.
</p>

</form>
<h2>Login</h2>
<p>Log a user into the system. After successful login, a bearer token is returned which you may store and use for
authentication for guarded routes. Note that this token has an expiry duration therefore you should implement
a mechanism to check whether the token has expired before requiring the user to login again.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost/api/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"email":"maxime","password":"aut"}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "email": "maxime",
    "password": "aut"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<div id="execution-results-POSTapi-login" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-login"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-login"></code></pre>
</div>
<div id="execution-error-POSTapi-login" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-login"></code></pre>
</div>
<form id="form-POSTapi-login" data-method="POST" data-path="api/login" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-login', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-login" onclick="tryItOut('POSTapi-login');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-login" onclick="cancelTryOut('POSTapi-login');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-login" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/login</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="email" data-endpoint="POSTapi-login" data-component="body" required  hidden>
<br>
Email address.
</p>
<p>
<b><code>password</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="password" name="password" data-endpoint="POSTapi-login" data-component="body" required  hidden>
<br>
Password.
</p>

</form>
<h2>Logout</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Log a user out of the system.</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost/api/logout" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/logout"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre>
<div id="execution-results-POSTapi-logout" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-logout"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-logout"></code></pre>
</div>
<div id="execution-error-POSTapi-logout" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-logout"></code></pre>
</div>
<form id="form-POSTapi-logout" data-method="POST" data-path="api/logout" data-authed="1" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-logout', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-logout" onclick="tryItOut('POSTapi-logout');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-logout" onclick="cancelTryOut('POSTapi-logout');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-logout" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/logout</code></b>
</p>
<p>
<label id="auth-POSTapi-logout" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="POSTapi-logout" data-component="header"></label>
</p>
</form><h1>User Profile Management</h1>
<p>APIs for managing user profile and phone numbers</p>
<h2>api/all-book</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/api/all-book" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/all-book"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "message": {
        "books": [
            {
                "id": 1,
                "isbn": "9789966346544",
                "title": "When no one is watching",
                "description": "Richard is an adolescent boy who is confronted with so many temptations. First, he tries to\r\ncheat in an exam after failing to\r\nrevise.",
                "file_type": null,
                "edition": 1,
                "publisher": "Moran (E. A.) Publishers Limited",
                "copyright": null,
                "language": "English",
                "price": "180",
                "author_id": null,
                "authors": [
                    "Ibrahim Omondi"
                ],
                "published": true,
                "publication_date": "2018-12-01",
                "embargo_date": null,
                "category_id": null,
                "level_two_category_id": 1,
                "level_three_category_id": 6,
                "level_four_category_id": null,
                "tag": null,
                "tags": [
                    {
                        "id": 1,
                        "slug": "grade-7",
                        "name": "Grade 7",
                        "suggest": false,
                        "count": 2,
                        "tag_group_id": null,
                        "description": null
                    }
                ],
                "image": "20210902201056.jpg",
                "created_at": "2021-09-02T20:10:56.000000Z",
                "updated_at": "2021-09-02T20:10:56.000000Z",
                "tagged": [
                    {
                        "id": 1,
                        "taggable_id": 1,
                        "taggable_type": "App\\Models\\Book",
                        "tag_name": "Grade 7",
                        "tag_slug": "grade-7",
                        "tag": {
                            "id": 1,
                            "slug": "grade-7",
                            "name": "Grade 7",
                            "suggest": false,
                            "count": 2,
                            "tag_group_id": null,
                            "description": null
                        }
                    }
                ]
            },
            {
                "id": 2,
                "isbn": "9789966346575",
                "title": "A taste of bitterness",
                "description": "In A Taste of Bitterness, Imelda is confronted with very tough choices. On one hand, there are the\r\nclassmates who expect her to join",
                "file_type": null,
                "edition": 1,
                "publisher": "Moran (E. A.) Publishers Limited",
                "copyright": "Moran (E. A.) Publishers Limited",
                "language": "English",
                "price": "200",
                "author_id": null,
                "authors": [
                    "Merceline Kakoro"
                ],
                "published": true,
                "publication_date": "2013-01-01",
                "embargo_date": null,
                "category_id": null,
                "level_two_category_id": 1,
                "level_three_category_id": 6,
                "level_four_category_id": null,
                "tag": null,
                "tags": [
                    {
                        "id": 2,
                        "slug": "grade-8",
                        "name": "Grade 8",
                        "suggest": false,
                        "count": 1,
                        "tag_group_id": null,
                        "description": null
                    }
                ],
                "image": "20210902202524.jpg",
                "created_at": "2021-09-02T20:25:24.000000Z",
                "updated_at": "2021-09-02T20:25:24.000000Z",
                "tagged": [
                    {
                        "id": 2,
                        "taggable_id": 2,
                        "taggable_type": "App\\Models\\Book",
                        "tag_name": "Grade 8",
                        "tag_slug": "grade-8",
                        "tag": {
                            "id": 2,
                            "slug": "grade-8",
                            "name": "Grade 8",
                            "suggest": false,
                            "count": 1,
                            "tag_group_id": null,
                            "description": null
                        }
                    }
                ]
            },
            {
                "id": 3,
                "isbn": "9789966346544",
                "title": "When no one is watching",
                "description": "When no one is watching",
                "file_type": null,
                "edition": 3,
                "publisher": "Moran (E. A.) Publishers Limited",
                "copyright": "Moran (E. A.) Publishers Limited",
                "language": "english",
                "price": "186",
                "author_id": null,
                "authors": [
                    "Ibrahim Omondi"
                ],
                "published": true,
                "publication_date": "2018-12-01",
                "embargo_date": null,
                "category_id": null,
                "level_two_category_id": 1,
                "level_three_category_id": 6,
                "level_four_category_id": null,
                "tag": null,
                "tags": [
                    {
                        "id": 1,
                        "slug": "grade-7",
                        "name": "Grade 7",
                        "suggest": false,
                        "count": 2,
                        "tag_group_id": null,
                        "description": null
                    }
                ],
                "image": "2073773129.jpg",
                "created_at": "2021-09-03T06:30:46.000000Z",
                "updated_at": "2021-09-03T06:30:46.000000Z",
                "tagged": [
                    {
                        "id": 3,
                        "taggable_id": 3,
                        "taggable_type": "App\\Models\\Book",
                        "tag_name": "Grade 7",
                        "tag_slug": "grade-7",
                        "tag": {
                            "id": 1,
                            "slug": "grade-7",
                            "name": "Grade 7",
                            "suggest": false,
                            "count": 2,
                            "tag_group_id": null,
                            "description": null
                        }
                    }
                ]
            },
            {
                "id": 4,
                "isbn": "9789966346285",
                "title": "Hannah and Tabitha\tGod is not selfish",
                "description": "These stories teach us that God answers our prayers.Hannah did not have a child",
                "file_type": null,
                "edition": 1,
                "publisher": "Moran (E. A.) Publishers Limited",
                "copyright": "Moran (E. A.) Publishers Limited",
                "language": "English",
                "price": "136",
                "author_id": null,
                "authors": [
                    "Dancan Sabwa,Leah Rotich"
                ],
                "published": true,
                "publication_date": "2013-01-01",
                "embargo_date": null,
                "category_id": null,
                "level_two_category_id": 1,
                "level_three_category_id": 4,
                "level_four_category_id": null,
                "tag": null,
                "tags": [],
                "image": "338489698.jpg",
                "created_at": "2021-09-10T09:35:38.000000Z",
                "updated_at": "2021-09-10T09:35:38.000000Z",
                "tagged": []
            }
        ]
    }
}</code></pre>
<div id="execution-results-GETapi-all-book" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-all-book"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-all-book"></code></pre>
</div>
<div id="execution-error-GETapi-all-book" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-all-book"></code></pre>
</div>
<form id="form-GETapi-all-book" data-method="GET" data-path="api/all-book" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-all-book', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-all-book" onclick="tryItOut('GETapi-all-book');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-all-book" onclick="cancelTryOut('GETapi-all-book');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-all-book" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/all-book</code></b>
</p>
</form>
    </div>
    <div class="dark-box">
                    <div class="lang-selector">
                                    <a href="#" data-language-name="bash">bash</a>
                                    <a href="#" data-language-name="javascript">javascript</a>
                            </div>
            </div>
</div>
<script>
    $(function () {
        var languages = ["bash","javascript"];
        setupLanguages(languages);
    });
</script>
</body>
</html>