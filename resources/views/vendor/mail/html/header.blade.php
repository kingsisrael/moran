<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'Laravel')
<img src="{{asset('assets/media/logos/moran.png')}}" class="logo" alt="Moran Logo">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
