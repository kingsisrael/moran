<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BookPayment;
use App\Models\Jambopay;
use App\Models\User;
use Bryceandy\Laravel_Pesapal\Models\Transaction;
use Bryceandy\Laravel_Pesapal\Pesapal\OAuthSignatureMethod_HMAC_SHA1;
use Bryceandy\Laravel_Pesapal\Pesapal\OAuthConsumer;
use Bryceandy\Laravel_Pesapal\Pesapal\OAuthRequest;
use Bryceandy\Laravel_Pesapal\Pesapal\pesapalCheckStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class TransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactionsCount = BookPayment::all()->count();
        return view('admin.transaction.index', compact('transactionsCount'));
    }
    public function pesapal()
    {
        $transactionCount = Jambopay::where('Status', 'SUCCESS')->count();
        return view('admin.transaction.pesapal-index', compact('transactionCount'));
    }
    public function getTransactions()
    {
        $transactions = \App\Models\Transaction::orderBy('created_at', 'desc')->get();
        return Datatables::of($transactions)
            ->addColumn('customer_details', function ($transactions){
                return $transactions->user()->first()->first_name.' '
                    .$transactions->user()->first()->last_name.'<br>'.
                    $transactions->user()->first()->email;
            })
            ->addColumn('vendor_details', function ($transactions){
                return $transactions->vendor()->first()->first_name.' '
                    .$transactions->vendor()->first()->last_name.'<br>'.
                    $transactions->transactionable->vendor_msisdn;
            })
            ->addColumn('phone_number', function ($transactions){
                return $transactions->transactionable->customer_msisdn;
            })
            ->addColumn('transaction_approved', function ($transactions){
                if ($transactions->approved == true) {
                    return '<span class="label label-lg font-weight-bold label-light-success label-inline">Approved</span>';
                }else {
                    return '<span class="label label-lg font-weight-bold label-light-danger label-inline">Pending</span>';
                }
            })
            ->addColumn('transaction_status', function ($transactions){
                if ($transactions->status == true) {
                    return '<span class="label label-lg font-weight-bold label-light-success label-inline">Complete</span>';
                }else {
                    return '<span class="label label-lg font-weight-bold label-light-warning label-inline">Pending</span>';
                }
            })
            ->addColumn('action', function ($transactions) {
                if ($transactions->approved == false){
                    return '<div class="dropdown dropdown-inline">
								<a href="" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown">
	                                <i class="la la-cog"></i>
	                            </a>
							  	<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
									<ul class="nav nav-hoverable flex-column">
							    		<li class="nav-item"><a class="nav-link" href="'.route('admin.transactions.show',$transactions->reference_number).'"><i class="nav-icon la la-street-view"></i><span class="nav-text">Show Details</span></a></li>
									</ul>
							  	</div>
							</div>
							<a href="'.route('admin.approve.transaction', $transactions->reference_number).'" class="btn btn-sm btn-clean btn-icon" title="Approve">
								<i class="la la-flag text-warning"></i>
							</a>

						';
                }
                return '<div class="dropdown dropdown-inline">
								<a href="" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown">
	                                <i class="la la-cog"></i>
	                            </a>
							  	<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
									<ul class="nav nav-hoverable flex-column">
							    		<li class="nav-item"><a class="nav-link" href="'.route('admin.transactions.show',$transactions->reference_number).'"><i class="nav-icon la la-street-view"></i><span class="nav-text">Show Details</span></a></li>
									</ul>
							  	</div>
							</div>
							<a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Approved">
								<i class="la la-flag text-success"></i>
							</a>

						';
            })
            ->rawColumns(['customer_details', 'vendor_details', 'action', 'transaction_status','transaction_approved'])
            ->make(true);
    }
    public function getUsersJambopayTransactions($id)
    {
        $type = Jambopay::where('user_id', $id)->orderBy('created_at', 'DESC')->get();
        return Datatables::of($type)
            ->addIndexColumn()
            ->editColumn('status', function ($type) {
                if($type->status == 1){
                    return '<h6><span class="badge badge-success">COMPLETED</span></h6>';
                }
                else{
                    return '<h6><span class="badge badge-danger">INVALID</span></h6>';
                }
            })
            ->editColumn('created_at', function ($type) {
                return Carbon::parse($type->created_at)->format('d M Y h:i');
            })
            ->rawColumns(['status','user_details'])
            ->make(true);
    }
    public function getUsersTransactions($id)
    {
        $transactions = \App\Models\BookPayment::where('user_id',$id)->orderBy('created_at', 'DESC')->get();
        return Datatables::of($transactions)
            ->addIndexColumn()
            ->editColumn('order_number', function ($type) {
                return $type->order_number;
            })
            ->editColumn('mpesaReceiptNumber', function ($type) {
                return $type->mpesaReceiptNumber;
            })
            ->editColumn('amount', function ($type) {
                return $type->amount;
            })
            ->editColumn('phoneNumber', function ($type) {
                return $type->phoneNumber;
            })
            ->editColumn('transactionDate', function ($type) {
                return Carbon::parse($type->transactionDate)->format('d M Y h:i');
            })
            ->editColumn('status', function ($type) {
                if($type->status == true){
                    return '<h6><span class="badge badge-success">COMPLETE</span></h6>';
                }else{
                    return '<h6><span class="badge badge-danger">FAILED</span></h6>';
                }
            })
            ->rawColumns(['status','user_details'])
            ->make(true);
    }
    public function show(string $reference_number)
    {
        $page_title = $reference_number;
        $page_description = 'Transaction Details';
        $transaction = \App\Models\Transaction::with(['user', 'transactionable'])->where('reference_number', '=', $reference_number)->firstOrFail();
        $vendors = User::role('vendor')->with('phone_numbers')->where(['is_active'=>true, 'online'=>true])
            ->whereHas('phone_numbers',function ($q){
                $q->where('provider_id', '=', 1);
            })
            ->get();

        return view('admin.transaction.show', compact('page_title', 'page_description', 'transaction', 'vendors'));
    }
    public function approve($reference_number){
        $transaction = \App\Models\Transaction::where('reference_number', '=', $reference_number)->firstOrFail();
        $transaction->update(['approved'=>true]);

        return Redirect::back()->with('success', 'Transaction Approved');
    }

    public function details()
    {
        return view ('laravel_pesapal::details');
    }

    public function payment(Request $request)
    {

        //pesapal params
        $token = $params = NULL;
        $consumer_key 		= '8plKT0y3q7PPEGtLXYc22FFvE4KEBEUJ';
        $consumer_secret 	= 'qf3bi+dBUl+C+gTb6F2AOKLKKz0=';

        $signature_method = new OAuthSignatureMethod_HMAC_SHA1();

        // if(env('PESAPAL_IS_LIVE') == 'true')
        // {
        //     $iframelink = 'https://www.pesapal.com/api/PostPesapalDirectOrderV4';
        // }
        // else
        // {
        //    $iframelink = 'https://www.pesapal.com/api/PostPesapalDirectOrderV4';
        // }
        $iframelink = 'https://www.pesapal.com/api/PostPesapalDirectOrderV4';

        //get form details
        $amount =1;
        $amount = intval(number_format($amount, 0));
        $currency = 'KES';
        $desc = 'Payments to XYZ Company';
        $type = 'MERCHANT';
        $ref =  str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',5);
        $reference 	=  substr(str_shuffle($ref),0,10);
        $first_name = 'Racheal';
        $last_name = 'Mbanya';
        $email = 'rachealwaceke@gmail.com';
        $phonenumber = '254708832182';

        $callback_url = 'http://127.0.0.1:8000/redirect';
        $userExists = DB::table('users')->where('email', $email)->first();
        Transaction::create([
            'user_id' => 1, 'phone' => $phonenumber, 'amount' => $amount, 'currency' => $currency, 'description' => $desc, 'reference' => $reference
        ]);

        /*Do not touch this xml variable in any way as it is the source of errors when you try to be clever and add extra spaces inside it*/
        $post_xml	= "<?xml version=\"1.0\" encoding=\"utf-8\"?>
               <PesapalDirectOrderInfo
                    xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
                      xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"
                      Currency=\"".$currency."\"
                      Amount=\"".$amount."\"
                      Description=\"".$desc."\"
                      Type=\"".$type."\"
                      Reference=\"".$reference."\"
                      FirstName=\"".$first_name."\"
                      LastName=\"".$last_name."\"
                      Email=\"".$email."\"
                      PhoneNumber=\"".$phonenumber."\"
                      xmlns=\"http://www.pesapal.com\" />";
        $post_xml = htmlentities($post_xml);

        $consumer = new OAuthConsumer($consumer_key, $consumer_secret);

        //post transaction to pesapal
        $iframe_src = OAuthRequest::from_consumer_and_token($consumer, $token, "GET", $iframelink, $params);
        $iframe_src->set_parameter("oauth_callback", $callback_url);
        $iframe_src->set_parameter("pesapal_request_data", $post_xml);
        $iframe_src->sign_request($signature_method, $consumer, $token);

        return view ('admin.transaction.iframe', compact('iframe_src'));
        // if($userExists) {
        //    $user_id = $userExists->id;
        // } else{
        //    return response()->json([
        //        'status' => 'error',
        //        'message' => "No user found",
        //    ]);
        // }

    }

    public function failedMpesaTransactions()
    {
        $transactionsCount = BookPayment::all()->count();
        return view('admin.transaction.failed-mpesa-transactions', compact('transactionsCount'));
    }

}
