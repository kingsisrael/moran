<?php

namespace App\Http\Controllers;

use App\Models\BookPayment;
use App\Models\PesaPal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use OAuthConsumer;
use OAuthRequest;
use OAuthSignatureMethod_HMAC_SHA1;
use pesapalCheckStatus;
use Symfony\Component\HttpFoundation\Response;

class PesaPalController extends Controller
{

    public function pesapal(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'currency' => 'required',
                'phone_number' => 'required',
                'description' => 'required',
                'amount' => 'required',

            ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], Response::HTTP_BAD_REQUEST);
        }
      //  include(app_path() . '\functions\OAuth.php');
        $api = 'https://demo.pesapal.com';
        $token = $params = NULL;
        $iframelink = $api . '/api/PostPesapalDirectOrderV4';
        //Kenyan keys
        $consumer_key = "8plKT0y3q7PPEGtLXYc22FFvE4KEBEUJ";
        $consumer_secret = "qf3bi+dBUl+C+gTb6F2AOKLKKz0=";

        $signature_method = new OAuthSignatureMethod_HMAC_SHA1();
        $consumer = new OAuthConsumer($consumer_key, $consumer_secret);

            $ref = str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', 5);
            $reference = substr(str_shuffle($ref), 0, 10);

        $amounts = str_replace(',', '', $request->get('amount')); // remove thousands seperator if included
        $amount = number_format($amounts, 2); //format amount to 2 decimal places

        $desc = $request->get('description');
        $type = 'MERCHANT';
        $first_name = $request->get('first_name');
        $last_name = $request->get('last_name');
        $email = $request->get('email');
        $phonenumber = $request->get('phone_number');
        $currency = $request->get('currency');
        $status	='PLACED';
        $userId = '1';
        $callback_url = 'http://172.104.245.14/moran/redirect'; //URL user to be redirected to after payment
        //Record order in your database.
        if (PesaPal::where('referenceNo', $reference )->exists()) {
            return response()->json(['message' => 'Could not enter transaction details..try again'], Response::HTTP_BAD_REQUEST);
        }
        else{
            $form_data = array(
                'currency' => $currency,
                'amount' => $amount,
                'status' => $status,
                'referenceNo' => $reference,
                'userId' => $userId,
            );
            $pesa = new PesaPal();
            $pesa->currency = $currency;
            $pesa->amount = $amount;
            $pesa->status = $status;
            $pesa->referenceNo = $reference;
            $pesa->userId = $userId;
            $pesa->save();

            if($pesa){
                $post_xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
				   <PesapalDirectOrderInfo
						xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
					  	xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"
					  	Currency=\"" . $currency . "\"
					  	Amount=\"" . $amount . "\"
					  	Description=\"" . $desc . "\"
					  	Type=\"" . $type . "\"
					  	Reference=\"" . $reference . "\"
					  	FirstName=\"" . $first_name . "\"
					  	LastName=\"" . $last_name . "\"
					  	Email=\"" . $email . "\"
					  	PhoneNumber=\"" . $phonenumber . "\"
					  	xmlns=\"http://www.pesapal.com\" />";
                $post_xml = htmlentities($post_xml);
                //post transaction to pesapal
                $iframe_src = OAuthRequest::from_consumer_and_token($consumer, $token, "GET", $iframelink, $params);
                $iframe_src->set_parameter("oauth_callback", $callback_url);
                $iframe_src->set_parameter("pesapal_request_data", $post_xml);
                $iframe_src->sign_request($signature_method, $consumer, $token);
                return response()->json(['message' => 'Sent to paypal'], Response::HTTP_BAD_REQUEST);

            }
            else{
                return response()->json(['message' => 'Something went wrong...try again'], Response::HTTP_BAD_REQUEST);
            }
        }

    }
    public function redirect(Request $request)
    {
        include(app_path() . '\functions\checkStatus.php');

        $pesapalTrackingId = null;
        $pesapalNotification = null;
        $pesapalMerchantReference = null;
        $checkStatus = new pesapalCheckStatus();
        if (isset($request->pesapal_merchant_reference)) {
            $pesapalMerchantReference =$request->pesapal_merchant_reference;
        }
        if (isset($request->pesapal_transaction_tracking_id)) {
            $pesapalTrackingId =$request->pesapal_transaction_tracking_id;
        }
        if (isset($request->pesapal_transaction_tracking_id)) {
            $pesapalTrackingId =$request->pesapal_transaction_tracking_id;
        }

        if(isset($_GET['pesapal_notification_type']))
            $pesapalNotification=$_GET['pesapal_notification_type'];
        $transactionDetails	= $checkStatus->getTransactionDetails($pesapalMerchantReference,$pesapalTrackingId);

        $value	= array("COMPLETED"=>"Paid","PENDING"=>"Pending","INVALID"=>"Cancelled","FAILED"=>"Cancelled");
        $status	= $value[$transactionDetails['status']];

        $pesa = PesaPal::where('referenceNo', $pesapalMerchantReference)->update([
            'status' => $transactionDetails['status'],
            'paymentMethod' => $transactionDetails['payment_method'],
            'trackingId' => $transactionDetails['pesapal_transaction_tracking_id'],
        ]);
    }
}
