<?php

namespace App\Http\Controllers;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Knox\Pesapal\OAuth\OAuthConsumer;
use Knox\Pesapal\OAuth\OAuthRequest;
use Knox\Pesapal\OAuth\OAuthSignatureMethod_HMAC_SHA1;
use Knox\Pesapal\Pesapal;


class PaymentsController extends Controller
{
    public function payment(Request $request){
        $token = $params = NULL;
        $consumer_key 		= '8plKT0y3q7PPEGtLXYc22FFvE4KEBEUJ';
        $consumer_secret 	= 'qf3bi+dBUl+C+gTb6F2AOKLKKz0=';
        $amount =1;
        $amount = intval(number_format($amount, 0));
        $currency = 'KES';
        $desc = 'Payments to XYZ Company';
        $type = 'MERCHANT';
        $ref =  str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',5);
        $reference 	=  substr(str_shuffle($ref),0,10);
        $first_name = 'Racheal';
        $last_name = 'Mbanya';
        $email = 'rachealwaceke@gmail.com';
        $phonenumber = '254708832182';

//        Transaction::create([
//            'user_id' => 20, 'phone' => $phonenumber, 'amount' => $amount, 'currency' => $currency, 'description' => $desc, 'reference' => $reference
//        ]);

        $details = array(
            'amount' => $amount,
            'description' => $desc,
            'type' => $type,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'phonenumber' => $phonenumber,
            'reference' => $reference,
            'height'=>'400px',
            'currency' => $currency
        );
        $iframe= (new \Knox\Pesapal\Pesapal)->makePayment($details);
        return view ('admin.transaction.iframe', compact('iframe'));

    }
    public function paymentsuccess(Request $request)//just tells u payment has gone thru..but not confirmed
    {
        if(isset($_GET['pesapal_merchant_reference']))
            $pesapalMerchantReference = $_GET['pesapal_merchant_reference'];

        if(isset($_GET['pesapal_transaction_tracking_id']))
            $pesapalTrackingId = $_GET['pesapal_transaction_tracking_id'];
        $status = 'PENDING';
        $update = Transaction::whereReference($pesapalMerchantReference)->first();
        $update->update([
            'status' => $status,
            'tracking_id' => $pesapalTrackingId
        ]);
        return view('admin.transaction.callback_example', compact('update'));

    }
    //This method just tells u that there is a change in pesapal for your transaction..
    //u need to now query status..retrieve the change...CANCELLED? CONFIRMED?
    public function paymentconfirmation(Request $request)
    {
        $trackingid = $request->input('pesapal_transaction_tracking_id');
        $merchant_reference = $request->input('pesapal_merchant_reference');
        $pesapal_notification_type= $request->input('pesapal_notification_type');

        //use the above to retrieve payment status now..
        $this->checkpaymentstatus($trackingid,$merchant_reference,$pesapal_notification_type);
    }
    //Confirm status of transaction and update the DB
    public function checkpaymentstatus($trackingid,$merchant_reference,$pesapal_notification_type){
        $status=$this->getMerchantStatus($merchant_reference);

        $update = Transaction::where('tracking_id',$trackingid)->first();
        $update->update([
            'status' => $status,
            'payment_method' => "PESAPAL"
        ]);
        return "success";
    }
    function getMerchantStatus($pesapal_merchant_reference)
    {

        $consumer_key 		= '8plKT0y3q7PPEGtLXYc22FFvE4KEBEUJ';
        $consumer_secret 	= 'qf3bi+dBUl+C+gTb6F2AOKLKKz0=';
        $statusrequestAPI = 'https://www.pesapal.com/api/querypaymentstatusbymerchantref';

        $token = $params = NULL;
        $consumer = new OAuthConsumer($consumer_key, $consumer_secret);
        $signature_method = new OAuthSignatureMethod_HMAC_SHA1();

        //get transaction status
        $request_status = OAuthRequest::from_consumer_and_token($consumer, $token, "GET", $statusrequestAPI, $params);
        $request_status->set_parameter("pesapal_merchant_reference", $pesapal_merchant_reference);
        $request_status->sign_request($signature_method, $consumer, $token);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $request_status);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        if (defined('CURL_PROXY_REQUIRED')) {
            if (CURL_PROXY_REQUIRED == 'True') {
                $proxy_tunnel_flag = (defined('CURL_PROXY_TUNNEL_FLAG') && strtoupper(CURL_PROXY_TUNNEL_FLAG) == 'FALSE') ? false : true;
                curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, $proxy_tunnel_flag);
                curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
                curl_setopt($ch, CURLOPT_PROXY, CURL_PROXY_SERVER_DETAILS);
            }
        }

        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $raw_header = substr($response, 0, $header_size - 4);
        $headerArray = explode("\r\n\r\n", $raw_header);
        $header = $headerArray[count($headerArray) - 1];

        //transaction status
        $elements = preg_split("/=/", substr($response, $header_size));
        $status = $elements[1];

        curl_close($ch);

        return $status;
    }

}
