<?php

namespace App\Http\Controllers;

use App\Models\CategoryLevelFour;
use App\Models\CategoryLevelThree;
use App\Models\CategoryLevelTwo;
use App\Models\MainCategory;
use Illuminate\Http\Request;

class LevelThreeCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = MainCategory::query()->orderby("id","asc")->select('id','name')->get();
      $two = CategoryLevelTwo::query()->orderby("id","asc")->select('id','name')->get();

        return view('admin.catalogue.level-three-category',compact('category','two'));

    }
    function fetchdata(Request $request)
    {
        $id = $request->input('id');
        $category = CategoryLevelThree::find($id);
//        $mains = MainCategory::findOrFail($category->category_id);

        $output = array(
            'name'    =>  $category->name,
//            'category_id'    =>  $mains->id,

//            'sub_category'     =>  $category->sub_category
        );
        echo json_encode($output);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $category = CategoryLevelThree::find($request->id);

        CategoryLevelFour::find($category->id)->delete();
        
        $deleted = $category->delete();

        $success_output = '';
        $error_output = '';

        if ($deleted) {
            $success_output = '<div class="alert alert-success">Level Three Category Deleted Successfully</div>';
        } else {
            $error_output = '<div class="alert alert-error">Failed to delete category</div>';
        }

        $output = array(
            'error' => $error_output,
            'success' => $success_output
        );

        echo json_encode($output);
    }
}
