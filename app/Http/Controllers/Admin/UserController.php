<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Book;
use App\Models\BookPayment;
use App\Models\Jambopay;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserLibrary;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{

    public function __construct()
    {
      $this->middleware('auth:admin');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users.index');
    }
    public function weekly(){
        return view('admin.users.weekly');
    }
    public function bought(){
        return view('admin.users.bought');
    }
    public function test()
    {
        $books = Book::all();
        return $books['id'];
        $tags = [];
        foreach ($books->tags as $tag) {
            array_push($materials_offered, $tag->name);
        }
        return  implode(", ", $tags);
        return $tags;
    }

    /**
     * Get Users DataTable
     *
     * @return \Illuminate\Http\Response
     */
    public function getUsers()
    {
        $users = User::role('user')->orderBy('created_at', 'DESC')->get();
        return Datatables::of($users)
            ->addColumn('action', function ($users) {
                return '<div class="dropdown dropdown-inline">
								<a href="" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown">
	                                <i class="la la-cog"></i>
	                            </a>
							  	<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
									<ul class="nav nav-hoverable flex-column">
							    		<li class="nav-item"><a class="nav-link" href="'.route('admin.app-users.show', $users->id).'"><i class="nav-icon la la-user"></i><span class="nav-text">Details</span></a></li>
							    		<li class="nav-item"><a class="nav-link notify" href="#" id="'.$users->id.'"><i class="nav-icon la la-circle"></i><span class="nav-text">Send Notification</span></a></li>
							    		<li class="nav-item"><a class="nav-link" href="#"><i class="nav-icon la la-stop-circle"></i><span class="nav-text">Block User</span></a></li>
							    	</ul>
							  	</div>
							</div>

						';
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' =>  'required|regex:/^[a-zA-Z]+$/u|min:4|max:12',
            'last_name' =>  'required|regex:/^[a-zA-Z]+$/u|min:4|max:12',
            'email' => 'required|email|unique:users',
            'phone_number' => 'required|unique:users',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $input = $request->only(
            'first_name', 'last_name', 'email', 'password', 'phone_number'
        );
        $input['password'] = Hash::make($input['password']);
        $check_phone_number = User::where('phone_number', '=', '254'.substr($input['phone_number'], -9))->exists();
        if ($check_phone_number){
            return Redirect::back()->withInput()->with('error', 'Phone Number already exists');
        }
        // remove non digits including spaces, - and +
        try {
            //$passcode = $this->passcode();
            $user = User::create([
                'first_name' => $input['first_name'],
                'last_name' => $input['last_name'],
                'email' => $input['email'],
                'phone_number' => '254'.substr($input['phone_number'], -9),
                'password' => $input['password'],
                'is_active' => true,
                'passcode' => mt_rand(1000,9999)
            ]);
            $user->assignRole('buyer');
            return Redirect::route('admin.app-users.index')->with('flash_success', 'User created successfully');
        } catch (\Exception $exception) {
            return Redirect::route('admin.app-users.create')->with('error', 'Something went wrong');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $user = User::findOrFail($id);
            $totalTransaction = $this->getUserTotalTransaction($user->id);
            $transactionCount = $this->transactionCount($user->id);
            $mpesa_bought_books_count = BookPayment::where('user_id', $id)->where('status', true)->count();
            $jambopay_bought_books_count = Jambopay::where('user_id', $id)->where('Status', 'SUCCESS')->count();
            $totalBoughtBooks = UserLibrary::where('user_id', $user->id)->count();
            $books = Book::all(['id', 'title']);
            $emails = collect(['kings@deveint.com', 'milimokings@gmail.com', 'isaac@deveint.com', 'valentine@deveint.com', 'rwaceke@moranpublishers.co.ke', 'md@deveint.com', 'coderpass@gmail.com', 'davidkinyanjui052@gmail.com', 'swathuo@moranpublishers.co.ke', 'solomon.mbaire@gmail.com']);
            return view('admin.users.show',compact('user','totalTransaction','transactionCount', 'totalBoughtBooks', 'books', 'emails'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }

    }
    function getUserTotalTransaction($id) {
        $transaction = BookPayment::where('user_id', $id)->where('status', true)
            ->get();
        $mpesa_transactions =  $transaction->sum('amount');
        $jambopay = Jambopay::where('user_id', $id)->where('Status', 'SUCCESS')->get();
        $jambopay_transactions = $jambopay->sum('Amount');
        return (int) $mpesa_transactions + (int) $jambopay_transactions;
    }
    function transactionCount($id) {
        $mpesa_count = BookPayment::where('user_id', $id)->where('status', true)
            ->count();
        $jambopay_count = Jambopay::where('user_id', $id)->where('Status', 'SUCCESS')->count();

        return (int) $mpesa_count + (int) $jambopay_count;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = Crypt::decrypt($id);
            $user = User::findOrFail($id);
            $phones = $user->phone_numbers()->where('user_id','=', $id)->get();
            return view('admin.users.edit',compact('user','phones'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'phone_number'=> 'required',
        ]);

        if ($validator->fails()) {
            return back()->with('flash_error', 'User Not Found');
            // return redirect()->route('admin.app-users.edit')->withErrors($validator)->withInput();
        }

        try {
            $user = User::findOrFail($id);
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->save();

            PhoneNumber::where('user_id', $id)
                ->update(['phone_number' => $request->phone_number]);
            return redirect()->route('admin.app-users.index')->with('flash_success', 'User Updated successfully');
        }
        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'User Not Found');
        }
    }

    public function userData(Request $request)
    {
        $id = $request->input('id');
        $user = User::find($id);
        $output = array(
            'full_name' => $user->first_name.' '.$user->last_name,
            'email' => $user->email,
            'phone_number' => $user->phone_number,
            'created_at' => $user->created_at->diffForHumans(),
            'points' => $user->points
        );
        echo json_encode($output);
    }

    function send_firebase_push_notification($token, $app_name, $push_message)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $notification = [
            'title' => $app_name, //notification title
            'body' => $push_message, //message body of the notification
        ];
        $fcmNotification = [
            'to'  => $token, //device token (smartphones unique identifier)
            'notification' =>  $notification,
        ];
        $headers = [
            'Authorization: key='.config('services.firebase_credentials.server_key'), //firebase server key
            'Content-Type: application/json'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        Log::info("response got: ". $result);
    }

    public function userNotify(Request $request)
    {
        $error_array = array();
        $success_output = '';

        // Make request to firebase cloud messaging with notification data
        $userDetails = User::find($request->id);
        if ($userDetails != NULL) {
            $this->send_firebase_push_notification($userDetails->token, $request->title, $request->content);
            $success_output = '<div class="alert alert-success">Notification sent successfully</div>';
        } else {
            $error_array[0] = 'Notification not sent. User does not have token.';
        }

        $output = array(
            'error'     =>  $error_array,
            'success'   =>  $success_output
        );
        echo json_encode($output);
    }

    public function getUserBooks($id)
    {
        $user = User::find($id);
        $library = $user->library;
        return DataTables::of($library)
            ->addIndexColumn()
            ->editColumn('book_title', function($library) {
                return $library->book->title;
            })
            ->editColumn('bought_on', function($library) {
                return Carbon::parse($library->created_at)->format('d M Y');
            })
            ->make(true);
    }

    public function addUserBook(Request $request)
    {
        $rules = [
            'book_id' => ['required']
        ];
        $validator = Validator::make($request->all(), $rules);
        $error_array = array();
        $success_output = '';
        if ($validator->fails())
        {
            foreach ($validator->messages()->getMessages() as $field_name => $messages)
            {
                $error_array[] = $messages;
            }
        } else {
            UserLibrary::create([
                'user_id' => $request->user_id,
                'book_id' => $request->book_id
            ]);

            $user = User::find($request->user_id);
            $book = Book::find($request->book_id);

            activity()
                ->causedBy(Auth::user())
                ->inLog('general')
                ->log(Auth::user()->first_name.' '.Auth::user()->last_name.' added the book '.$book->title.' to '.$user->first_name.' '.$user->last_name.'\'s library ');

            $success_output = '<div class="alert alert-success">Transaction Updated Successful</div>';
        }

        $output = array(
            'error'     =>  $error_array,
            'success'   =>  $success_output
        );
        echo json_encode($output);
    }

    public function logger(Request $request)
    {
        info($request);
    }
}
