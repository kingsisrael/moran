<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\SendSMS;
use App\Mail\AdminCreated;
use App\Models\Admin;
use App\Models\BookPayment;
use App\Models\Category;
use App\Models\CategoryLevelFour;
use App\Models\CategoryLevelThree;
use App\Models\CategoryLevelTwo;
use App\Models\Jambopay;
use App\Models\LevelTwoCategory;
use App\Models\MpesaPayment;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserLibrary;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Permission;
use DB;
class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users.index-admin-user');
    }

    public function mpesaData()
    {
        activity()
            ->causedBy(Auth::user())
            ->inLog('general')
            ->log(Auth::user()->first_name.' '.Auth::user()->last_name.' accessed the Mpesa Logs');

        $type = BookPayment::orderBy('transactionDate', 'DESC')->where('status', true)->get();
        return Datatables::of($type)
            ->addIndexColumn()
            ->editColumn('order_number', function ($type) {
                return $type->order_number;
            })
            ->addColumn('user_details', function ($type){
                return $type->user()->first()->first_name.' '
                    .$type->user()->first()->last_name.'<br>'.
                    $type->user()->first()->email;
            })
            ->editColumn('mpesaReceiptNumber', function ($type) {
                return $type->mpesaReceiptNumber;
            })
            ->editColumn('amount', function ($type) {
                return $type->amount;
            })
            ->editColumn('phoneNumber', function ($type) {
                return $type->phoneNumber;
            })
            ->editColumn('status', function ($type) {
                if($type->status == true){
                    return '<h6><span class="badge badge-success">COMPLETE</span></h6>';
                }else{
                    return '<h6><span class="badge badge-danger">FAILED</span></h6>';
                }
            })
            ->editColumn('transactionDate', function ($type) {
                return Carbon::parse($type->transactionDate)->format('d M Y h:i');
            })
            ->editColumn('action', function ($type) {
                return '<a class="edit-transaction" id="'.$type->id.'" href="'.route('admin.mpesa.edit',Crypt::encrypt($type->id)).'"><i class="la la-cog"></i><span class="">Edit</span></a>';
            })
            ->rawColumns(['status','user_details', 'action'])
            ->make(true);
    }

    public function mpesaDataFailed()
    {
        activity()
            ->causedBy(Auth::user())
            ->inLog('general')
            ->log(Auth::user()->first_name.' '.Auth::user()->last_name.' accessed the Mpesa logs');
        
        $type = BookPayment::orderBy('transactionDate', 'DESC')->where('status', false)->get();
        return Datatables::of($type)
            ->addIndexColumn()
            ->editColumn('order_number', function ($type) {
                return $type->order_number;
            })
            ->addColumn('user_details', function ($type){
                return $type->user()->first()->first_name.' '
                    .$type->user()->first()->last_name.'<br>'.
                    $type->user()->first()->email;
            })
            ->editColumn('mpesaReceiptNumber', function ($type) {
                return $type->mpesaReceiptNumber;
            })
            ->editColumn('amount', function ($type) {
                return $type->amount;
            })
            ->editColumn('phoneNumber', function ($type) {
                return $type->phoneNumber;
            })
            ->editColumn('status', function ($type) {
                if($type->status == true){
                    return '<h6><span class="badge badge-success">COMPLETE</span></h6>';
                }else{
                    return '<h6><span class="badge badge-danger">FAILED</span></h6>';
                }
            })
            ->editColumn('transactionDate', function ($type) {
                return Carbon::parse($type->transactionDate)->format('d M Y h:i');
            })
            ->editColumn('action', function ($type) {
                return '<a class="edit-transaction" id="'.$type->id.'" href="'.route('admin.mpesa.edit',Crypt::encrypt($type->id)).'"><i class="la la-cog"></i><span class="">Edit</span></a>';
            })
            ->rawColumns(['status','user_details', 'action'])
            ->make(true);
    }

    public function PalData()
    {
        activity()
            ->causedBy(Auth::user())
            ->inLog('general')
            ->log(Auth::user()->first_name.' '.Auth::user()->last_name.' accessed the Jambopay logs');
        
        $type = Jambopay::orderBy('created_at', 'DESC')->where('status', 1)->get();
        return Datatables::of($type)
            ->addIndexColumn()
            ->addColumn('user_details', function ($type){
                return $type->user()->first()->first_name.' '
                    .$type->user()->first()->last_name.'<br>'.
                    $type->user()->first()->email;
            })
            ->editColumn('status', function ($type) {
                if($type->status == 1){
                    return '<h6><span class="badge badge-success">COMPLETED</span></h6>';
                }
                else{
                    return '<h6><span class="badge badge-danger">INVALID</span></h6>';
                }
            })
            ->editColumn('created_at', function ($type) {
                return Carbon::parse($type->created_at)->format('d M Y h:i');
            })
            ->rawColumns(['status','user_details'])
            ->make(true);
    }

    public function pesapalData()
    {
        $type = Transaction::orderBy('created_at', 'DESC')->get();
        return Datatables::of($type)
            ->addIndexColumn()
            ->addColumn('user_id', function ($type){
                return $type->user()->first()->first_name.' '
                    .$type->user()->first()->last_name;
            })
            ->editColumn('phone', function ($type) {
                return $type->phone;
            })
            ->editColumn('amount', function ($type) {
                return $type->amount;
            })
            ->editColumn('currency', function ($type) {
                return $type->currency;
            })
            ->editColumn('status', function ($type) {
                if($type->status == 'COMPLETED'){
                    return '<h6><span class="badge badge-success">COMPLETED</span></h6>';
                }
                if($type->status == 'PENDING'){
                    return '<h6><span class="badge badge-secondary">PENDING</span></h6>';
                }
                else{
                    return '<h6><span class="badge badge-danger">INVALID</span></h6>';
                }
            })
            ->editColumn('reference', function ($type) {
                return $type->reference;
            })
            ->editColumn('tracking_id', function ($type) {
                return $type->tracking_id;
            })
            ->editColumn('payment_method', function ($type) {
                return $type->payment_method;
            })
            ->editColumn('date', function ($type) {
                return Carbon::parse($type->created_at)->format('d M Y h:i');
            })
            ->rawColumns(['status','user_id'])
            ->make(true);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::query()
            ->where('name', '!=', 'user')
            ->get();
        return view('admin.users.create-admin-user', compact('roles'));
    }
    public function getSubCounty($id=0)
    {
        $empData['data'] = CategoryLevelTwo::query()
            ->orderby("id","asc")
            ->select('id','name')
            ->where('category_id','=', $id)
            ->get();
        return response()->json($empData);
    }

    public function getSubThree($id=0)
    {
        $empData['data'] = CategoryLevelThree::query()
            ->orderby("id","asc")
            ->select('id','name')
            ->where('level_two_category_id','=', $id)
            ->get();
        return response()->json($empData);
    }

    public function getSubFour($id=0)
    {
        $empData['data'] = CategoryLevelFour::query()
            ->orderby("id","asc")
            ->select('id','name')
            ->where('level_three_category_id','=', $id)
            ->get();
        return response()->json($empData);
    }
    /**
     * Get Users DataTable
     *
     * @return \Illuminate\Http\Response
     */
    public function getAdminUsers()
    {
        $users = Admin::role(['admin','finance','ict'])->get();
        return Datatables::of($users)
            ->addColumn('role', function ($users){
                if ($users->roles->first()->name == 'ict'){
                    return "IT";
                }
                if ($users->roles->first()->name == 'finance'){
                    return "Finance";
                }
                else{
                    return $users->roles->first()->name ?? '--';

               }
            })
            ->addColumn('action', function ($users) {
                return '<div class="dropdown dropdown-inline">
								<a href="" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown">
	                                <i class="la la-cog"></i>
	                            </a>
							  	<div class="dropdown-menu dropdown-menu dropdown-menu-right">
									<ul class="nav nav-hoverable flex-column">
                                        <li class="nav-item"><a class="nav-link" href="'.route('admin.app-admins.edit',Crypt::encrypt($users->id)).'"><i class="nav-icon la la-user"></i><span class="nav-text">Edit</span></a></li>
                                        <li class="nav-item">
                                            <form action="'.route('admin.status.update', $users->id).'" method="POST">
                                                '.@csrf_field().'
                                                <input type="hidden" name="status" value="false">
                                                <input type="submit" class="btn" value="Activate/Suspend Account">
                                            </form>
                                        </li>
							    	</ul>
							  	</div>
							</div>
						';
            })
            ->make(true);
    }
    public function delete(Request $request, $id)
    {
        $data = Admin::findOrFail($id);
        $data->delete();
        return response()->json(['success'=>'Admin User deleted successfully']);

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
            $validator = Validator::make($request->all(), [
                'first_name' =>  'required',
                'last_name' =>  'required',
                'email' => 'required|email|unique:admins',
                'phone_number' => 'required|unique:admins',
                'password' => 'required',
                'role' => 'required',
            ]);
            if ($validator->fails()) {
                return redirect()->route('admin.app-admins.create')
                    ->withErrors($validator)
                    ->withInput()
                    ->with('error', $validator->errors()->first());

            }
            $password = $request->password;
            $check_phone_number = Admin::query()->where('phone_number', '=', '254'.substr($request->phone_number, -9))->exists();
            if ($check_phone_number){
                return Redirect::back()->withInput()->with('error', 'Phone Number already exists');
            }
            try {
                //$passcode = $this->passcode();
                $user = Admin::create([
                    'first_name' => $request->first_name,
                    'last_name' =>$request->last_name,
                    'email' => $request->email,
                    'phone_number' => '254'.substr($request->phone_number, -9),
                    'password' => Hash::make($request->password),
                ]);
                $user->assignRole($request->role);
                $details = [
                    'name'=>$user->first_name.' '.$user->last_name,
                    'email'=>$user->email,
                    'password'=>$password,
                ];
                // if ($user) {
                //     $user->notify(
                //       new \App\Notifications\AdminCreated($details)
                //     );
                // }
                // temporary
                SendSMS::dispatch($user->phone_number,$password,$user->first_name);
                return redirect()->route('admin.app-admins.index')->with('message','Admin created Successfully');
            } catch (\Exception $exception) {

                return back()->with('error', 'Error Saving the Record...try again');

            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = Crypt::decrypt($id);
        $user = Admin::query()->findOrFail($id);
        $role = $user->roles->first()->name;
       return view('admin.users.show-admin',compact('user','role'));
    }

    public function statusUpdate(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->with('error', 'User Not Found');
        }
        try {
            $user = Admin::query()->findOrFail($id);
            if ($user->id == Auth::id()) {
                return Redirect::route('admin.app-admins.index')->with('message', 'You are not permitted to suspend yourself');
            }else {
                $user->status = !$user->status;
                $user->save();
                return Redirect::route('admin.app-admins.index')->with('message', 'User Status Updated successfully');
            }
        }
        catch (ModelNotFoundException $e) {
            return back()->with('error', 'Error Try Again...');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::query()
            ->where('name', '!=', 'user')
            ->get();
        $id = Crypt::decrypt($id);
        $user = Admin::query()->findOrFail($id);
        return view('admin.users.edit-admin-user', compact('user','roles'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'first_name' =>  'required|regex:/^[a-zA-Z]+$/u|min:4|max:12',
            'last_name' =>  'required|regex:/^[a-zA-Z]+$/u|min:4|max:12',
            'email' => 'required|email',
            'phone_number' => 'required',
            'role' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        try {
            $admin = Admin::findOrFail($id);
            $admin->first_name = $request->first_name;
            $admin->last_name = $request->last_name;
            $admin->email = $request->email;
            $admin->phone_number = $request->phone_number;
            if ($request->password != NULL || $request->password != '') {
                $admin->password = Hash::make($request->password);
            }
            $admin->updated_at = Carbon::now();
            $admin->save();
            return redirect()->route('admin.app-admins.index')->with('message', 'Admin Updated successfully');
        }
        catch (ModelNotFoundException $e) {
            return back()->with('error', 'Admin Record Not Found');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getTransaction(Request $request)
    {
        $id = $request->input('id');
        $transaction = BookPayment::find($id);
        $output = array(
            'mpesaReceiptNumber' => $transaction->mpesaReceiptNumber,
            'status' => $transaction->status,
            'phone_number' => $transaction->phoneNumber,
            'amount' => $transaction->amount
        );

        echo json_encode($output);
    }

    public function updateTransaction(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'status' => 'required',
            'mpesaReceiptNumber' => ['required_if:status,1'],
            'amount' => ['required_if:status,1'],
            'phone_number' => ['required_if:status,1']
        ], 
        [
            'mpesaReceiptNumber.required' => 'Please enter the MPESA receipt number',
            'amount.required' => 'Please enter the amount paid'
        ]);
        $error_array = array();
        $success_output = '';
        if ($validation->fails())
        {
            foreach ($validation->messages()->getMessages() as $field_name => $messages)
            {
                $error_array[] = $messages;
            }
        } else {
            $transaction = BookPayment::find($request->id);
            $transaction->update([
                'mpesaReceiptNumber' => $request->mpesaReceiptNumber,
                'status' => $request->status,
                'phone_number' => $request->phone_number,
                'amount' => $request->amount
            ]);

            UserLibrary::create([
                'user_id' => $transaction->user_id,
                'book_id' => $transaction->book_id
            ]);

            activity()
                ->causedBy(Auth::user())
                ->inLog('general')
                ->log(Auth::user()->first_name.' '.Auth::user()->last_name.' updated the Mpesa transaction '.$request->mpesaReceiptNumber);

            $success_output = '<div class="alert alert-success">Transaction Updated Successful</div>';
        }

        $output = array(
            'error'     =>  $error_array,
            'success'   =>  $success_output
        );
        echo json_encode($output);
    }
}
