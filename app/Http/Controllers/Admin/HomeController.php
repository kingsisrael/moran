<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Author;
use App\Models\BookPayment;
use App\Models\Category;
use App\Models\CategoryLevelFour;
use App\Models\CategoryLevelThree;
use App\Models\CategoryLevelTwo;
use App\Models\Jambopay;
use App\Models\Language;
use App\Models\LevelTwoCategory;
use App\Models\MainCategory;
use App\Models\MpesaPayment;
use App\Models\PesaPal;
use App\Models\Provider;
use App\Models\Publisher;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class HomeController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $current = Carbon::now()->format('M-Y');
        $userCount = User::all()->count();
        $count = User::all()->count();
        $books = BookPayment::where('status', 1)->count();

        // Get weekly sales
        $start_date = Carbon::now()->subWeek();
        $end_date = Carbon::now();
        // Get mpesa payments for the past week
        $mpesa_week_payments = MpesaPayment::whereBetween('created_at', [$start_date, $end_date])->sum('amount');
        $jambopay_week_payments = Jambopay::whereBetween('created_at', [$start_date, $end_date])->sum('Amount');
        $pesapay_week_payments = PesaPal::whereBetween('created_at', [$start_date, $end_date])->sum('amount');
        
        $total_weekly_payments = (double) $mpesa_week_payments + (double) $jambopay_week_payments + (double) $pesapay_week_payments;

        // Get total sales
        $mpesa_payments = MpesaPayment::sum('amount');
        $jambopay_payments = Jambopay::sum('Amount');
        $pesapal_payments = Pesapal::sum('amount');
        
        $total_sales = (double) $mpesa_payments + (double) $jambopay_payments + (double) + $pesapal_payments;

        // dd($this->totalUsersPerMonth());
        $usersPerMonthChartData = $this->totalUsersPerMonth();
        $transactionPerMonthChartData = $this->getTransactionsPerMonth();
        $dailyTransactions = $this->transactionsInPastWeek();

        return view('admin.dashboard', compact('books','userCount','current', 'total_weekly_payments', 'total_sales', 'usersPerMonthChartData', 'transactionPerMonthChartData', 'dailyTransactions'));
    }
    
    public function disbursed_payments_filter(Request $request)
    {
        $current = Carbon::now()->format('M-Y');
        return view('admin.transaction_region_filter',compact('current'));
    }

    /**
     * Get Teleco Providers View
     *
     * @return \Illuminate\Http\Response
     */
    public function viewProviders()
    {
        return view('admin.catalogue.index');
    }

    /**
     * Get Teleco Providers DataTable
     *
     * @return \Illuminate\Http\Response
     */
    public function getCategory()
    {
        $users = MainCategory::orderBy('created_at', 'DESC');
        return Datatables::of($users)
            ->addIndexColumn()
            ->editColumn('action', function ($providers) {
                if ($providers->sub_category == true){
                    return '<div class="dropdown dropdown-inline">
								<a href="" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown">
	                                <i class="la la-cog"></i>
	                            </a>
							  	<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
									<ul class="nav nav-hoverable flex-column">
							    		<li class="nav-item edit"><a class="nav-link edit" href="#" id="'.$providers->id.'"><i class="nav-icon la la-edit"></i><span class="nav-text">Edit Details</span></a></li>
							    		<li class="nav-item"><a class="nav-link delete d-none" href="'.route('admin.catalogue.delete',$providers->id).'"><i class="nav-icon la la-user"></i><span class="nav-text">Delete</span></a></li>
							    	</ul>
							  	</div>
							</div>
						';
                }
                else{
                    return '<div class="dropdown dropdown-inline">
								<a href="" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown">
	                                <i class="la la-cog"></i>
	                            </a>
							  	<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
									<ul class="nav nav-hoverable flex-column">
							    		<li class="nav-item" ><a class="nav-link edit" href="#" id="'.$providers->id.'"><i class="nav-icon la la-edit"></i><span class="nav-text">Edit Details</span></a></li>
							    		<li class="nav-item"><a class="nav-link d-none" href="'.route('admin.catalogue.show',$providers->id).'"><i class="nav-icon la la-user"></i><span class="nav-text">View Details</span></a></li>
							    		<li class="nav-item"><a class="nav-link delete d-none" href="'.route('admin.catalogue.delete',$providers->id).'"><i class="nav-icon la la-user"></i><span class="nav-text">Delete</span></a></li>
							    	</ul>
							  	</div>
							</div>
						';
                }

            })
            ->editColumn('created_at', function ($providers){
                return Carbon::parse($providers->created_at)->format('Y-m-d h:i:s');
            })
            ->make(true);
    }

    function postdata(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            // 'sub_category'  => 'required',
        ]);
        $error_array = array();
        $success_output = '';
        if ($validation->fails())
        {
            foreach ($validation->messages()->getMessages() as $field_name => $messages)
            {
                $error_array[] = $messages;
            }
        }
        else
        {
            if($request->get('button_action') == 'insert')
            {
                $image = $request->file('image');
                $destinationPath = 'category';
                if(!is_dir($destinationPath)) {
                    mkdir($destinationPath, 0755, true);
                }
                $image_name = rand() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('category'), $image_name);

                $category = new MainCategory([
                    'name'    =>  $request->get('name'),
                    'image'  =>  $image_name,
                    // 'sub_category'     =>  $request->get('sub_category')
                ]);
                $category->save();
                $success_output = '<div class="alert alert-success">Main Category Create Successfully</div>';
            }
            if($request->get('button_action') == 'update')
            {
                $category = MainCategory::find($request->get('id'));
                $category->name = $request->get('name');
                // $category->sub_category = $request->get('sub_category');
                $category->save();
                $success_output = '<div class="alert alert-success">Updated Successful</div>';
            }

        }
        $output = array(
            'error'     =>  $error_array,
            'success'   =>  $success_output
        );
        echo json_encode($output);
    }

    function postdatatwo(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        $error_array = array();
        $success_output = '';
        if ($validation->fails())
        {
            foreach ($validation->messages()->getMessages() as $field_name => $messages)
            {
                $error_array[] = $messages;
            }
        }
        else
        {
            if($request->get('button_action') == 'insert')
            {
                //    $image = $request->file('image');
                //    $destinationPath = 'images';
                //    if(!is_dir($destinationPath)) {
                //        mkdir($destinationPath, 0755, true);
                //    }
                //    $image_name = rand() . '.' . $image->getClientOriginalExtension();
                //    $image->move(public_path('images'), $image_name);
                $category = new CategoryLevelTwo([
                    'name'    =>  $request->get('name'),
                    'category_id'    =>  $request->get('category_id'),
                    //

                ]);
                $category->save();
                $success_output = '<div class="alert alert-success">Level Two Category Create Successfully</div>';
            }
            if($request->get('button_action') == 'update')
            {
                $category = CategoryLevelTwo::find($request->get('id'));
                $category->name = $request->get('name');
                $category->category_id = $request->get('category_id');
                $category->save();
                $success_output = '<div class="alert alert-success">Level Two Category Updated Successful</div>';
            }

        }
        $output = array(
            'error'     =>  $error_array,
            'success'   =>  $success_output
        );
        echo json_encode($output);
    }

    function postdatathree(Request $request)
    {
        $validation = Validator::make($request->all(), [
        //    'category_id' => 'required',
            'level_two_category_id' => 'required',
            'name' => 'required',

        ]);
        $error_array = array();
        $success_output = '';
        if ($validation->fails())
        {
            foreach ($validation->messages()->getMessages() as $field_name => $messages)
            {
                $error_array[] = $messages;
            }
        }
        else
        {
            if($request->get('button_action') == 'insert')
            {
                $category = new CategoryLevelThree([
                    'name'    =>  $request->get('name'),
                //    'category_id'    =>  $request->get('category_id'),
                    'level_two_category_id'    =>  $request->get('level_two_category_id'),

                ]);
                $category->save();
                $success_output = '<div class="alert alert-success">Level Three Category Create Successfully</div>';
            }
            if($request->get('button_action') == 'update')
            {
                $category = CategoryLevelThree::find($request->get('id'));
                $category->name = $request->get('name');
                $category->level_two_category_id = $request->get('level_two_category_id');
                $category->save();
                $success_output = '<div class="alert alert-success">Level Three Category Updated Successful</div>';
            }
        }
        $output = array(
            'error'     =>  $error_array,
            'success'   =>  $success_output
        );
        echo json_encode($output);
    }

    function postdatafour(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'level_three_category_id' => 'required',
        //    'category_id' => 'required',
        //    'level_two_category_id' => 'required',
        ]);
        $error_array = array();
        $success_output = '';
        if ($validation->fails())
        {
            foreach ($validation->messages()->getMessages() as $field_name => $messages)
            {
                $error_array[] = $messages;
            }
        }
        else
        {
            if($request->get('button_action') == 'insert')
            {
                $category = new CategoryLevelFour([
                    'name'    =>  $request->get('name'),
                    'level_three_category_id'    =>  $request->get('level_three_category_id'),
                //    'category_id'    =>  $request->get('category_id'),
                //    'level_two_category_id'    =>  $request->get('level_two_category_id'),
                ]);
                $category->save();
                $success_output = '<div class="alert alert-success">Level Four Category Create Successfully</div>';
            }
            if($request->get('button_action') == 'update')
            {
                $category = CategoryLevelFour::find($request->get('id'));
                $category->name = $request->get('name');
                $category->level_three_category_id = $request->get('level_three_category_id');
                $category->save();
                $success_output = '<div class="alert alert-success">Level Four Category Updated Successful</div>';
            }
        }
        $output = array(
            'error'     =>  $error_array,
            'success'   =>  $success_output
        );
        echo json_encode($output);
    }

    function fetchdata(Request $request)
    {
        $id = $request->input('id');
        $category = MainCategory::find($id);
        $output = array(
            'name'    =>  $category->name,
            'sub_category'     =>  $category->sub_category
        );
        echo json_encode($output);
    }

    public function deletePost($id) {
        $post = MainCategory::findOrFail($id);
        $post->delete();
        return redirect()->back()->with('error', 'Main Category has been deleted successfully!');
    }

    public function deletelanguages($id) {
        $post = Language::findOrFail($id);
        $post->delete();
        return redirect()->back()->with('error', 'Language has been deleted successfully!');
    }

    public function deletepublishers($id) {
        $post = Publisher::findOrFail($id);
        $post->delete();
        return redirect()->back()->with('error', 'Publisher has been deleted successfully!');
    }

    public function deleteAuthors($id) {
        $post = Author::findOrFail($id);
        $post->delete();
        return redirect()->back()->with('error', 'Author has been deleted successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = MainCategory::findOrFail($id);
        $data->delete();
    }

    public function getCategoryTwo()
    {
        // $providers = CategoryLevelTwo::with(['category' => function ($q){
        //     $q->orderBy('created_at', 'DESC');
        // }])->get();
        $providers = CategoryLevelTwo::orderBy('created_at', 'DESC');

        return Datatables::of($providers)
            ->addIndexColumn()

            ->addColumn('category', function ($providers){
                return $providers->category['name'];
            })
            ->editColumn('action', function ($providers) {
                return '<div class="dropdown dropdown-inline">
								<a href="" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown">
	                                <i class="la la-cog"></i>
	                            </a>
							  	<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
									<ul class="nav nav-hoverable flex-column">
							    		<li class="nav-item edit"><a class="nav-link edit" href="#" id="'.$providers->id.'"><i class="nav-icon la la-edit"></i><span class="nav-text">Edit Details</span></a></li>
							    		<li class="nav-item"><a class="nav-link delete d-none" id="'.$providers->id.'" href="'.route('admin.catalogue.delete',$providers->id).'"><i class="nav-icon la la-user"></i><span class="nav-text">Delete</span></a></li>

							    	</ul>
							  	</div>
							</div>
						';
            })
            ->editColumn('created_at', function ($providers){
                return Carbon::parse($providers->created_at)->format('Y-m-d h:i:s');
            })
            ->make(true);
    }
    
    public function getCategoryThree()
    {
        $providers = CategoryLevelThree::orderBy('created_at', 'DESC');

        return Datatables::of($providers)
            ->addIndexColumn()
            ->addColumn('leve_two', function ($providers){
                return $providers->threes['name'];
            })
            ->editColumn('action', function ($providers) {
                return '<div class="dropdown dropdown-inline">
								<a href="" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown">
	                                <i class="la la-cog"></i>
	                            </a>
							  	<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
									<ul class="nav nav-hoverable flex-column">
							    		<li class="nav-item edit"><a class="nav-link edit" href="#" id="'.$providers->id.'"><i class="nav-icon la la-edit"></i><span class="nav-text">Edit Details</span></a></li>
							    		<li class="nav-item"><a class="nav-link delete d-none" id="'.$providers->id.'" href="'.route('admin.catalogue.delete',$providers->id).'"><i class="nav-icon la la-user"></i><span class="nav-text">Delete</span></a></li>

							    	</ul>
							  	</div>
							</div>
						';
            })
            ->editColumn('created_at', function ($providers){
                return Carbon::parse($providers->created_at)->format('Y-m-d h:i:s');
            })
            ->make(true);
    }

    public function getCategoryFour()
    {
        $providers = CategoryLevelFour::orderBy('created_at', 'DESC');
        return Datatables::of($providers)
            ->addIndexColumn()

            ->addColumn('leve_three', function ($providers){
                return $providers->fours['name'];
            })
            ->editColumn('action', function ($providers) {
                return '<div class="dropdown dropdown-inline">
								<a href="" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown">
	                                <i class="la la-cog"></i>
	                            </a>
							  	<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
									<ul class="nav nav-hoverable flex-column">
							    		<li class="nav-item edit"><a class="nav-link edit" href="#" id="'.$providers->id.'"><i class="nav-icon la la-edit"></i><span class="nav-text">Edit Details</span></a></li>
							    		<li class="nav-item"><a class="nav-link delete d-none" id="'.$providers->id.'" href="'.route('admin.catalogue.delete',$providers->id).'"><i class="nav-icon la la-user"></i><span class="nav-text">Delete</span></a></li>
							    	</ul>
							  	</div>
							</div>
						';
            })
            ->editColumn('created_at', function ($providers){
                return Carbon::parse($providers->created_at)->format('Y-m-d h:i:s');
            })
            ->make(true);
    }

    function getMonthlyPostData() {
        $month = Carbon::now()->format('m');
        $year = Carbon::now()->format('Y');
        $monthly_post_count_array = array();
        $monthly_transaction = array();
        $day_mon_array = $this->getAllMonthsDays($month, $year);
        $days_array = $day_mon_array['days_array'];
        $month = $day_mon_array['month'];
        $days_array_dates = $day_mon_array['days_array_dates'];
        $month_name_array = array();
        $month_name_array_dates = array();

        if ( ! empty( $days_array ) ) {
            foreach ( $days_array as $day_no => $day_name ){
                $monthly_post_count = $this->getMonthlyAmountCounts($day_no ,$month, $year);
                $monthly_count = $this->getDailyAmountCounts($day_no ,$month, $year);
                array_push( $monthly_post_count_array, $monthly_post_count );
                array_push( $monthly_transaction, $monthly_count );

                array_push( $month_name_array, $day_name );
            }
            foreach ( $days_array_dates as $day_no => $day_namee ){
                array_push( $month_name_array_dates, $day_namee );
            }
        }
        if (!empty($monthly_post_count_array)){
            $max_no = max( $monthly_post_count_array );
            $max = round(( $max_no + 10/2 ) / 10 ) * 10;
        }else{
            $max_no = $max = 0;
        }
        if (!empty($monthly_transaction)){
            $max_no_daily = max( $monthly_transaction );
            $max_daily = round(( $max_no_daily + 10/2 ) / 10 ) * 10;
        }else{
            $max_no_daily = $max_daily = 0;
        }

        return array(
            'months' => $month_name_array,
            'month' => $month_name_array_dates,
            'post_count_data' => $monthly_post_count_array,
            'max' => $max,
            'max_daily' => $max_daily,
            'daily_count' => $monthly_transaction,
        );
    }

    /*
    * Dashboard's Transactions Chart Data
    */
    public function getMonthlyTransactionsData($month, $year) {
        $monthly_post_count_array = array();
        $monthly_transaction = array();
        $day_mon_array = $this->getAllMonthsDays($month,$year);
        $days_array = $day_mon_array['days_array'];
        $month = $day_mon_array['month'];
        $days_array_dates = $day_mon_array['days_array_dates'];
        $month_name_array = array();
        $month_name_array_dates = array();

        if ( ! empty( $days_array ) ) {
            foreach ( $days_array as $day_no => $day_name ){
                $monthly_post_count = $this->getMonthlyAmountCounts( $day_no ,$month,$year);
                $monthly_count = $this->getDailyAmountCounts( $day_no ,$month,$year);
                array_push( $monthly_post_count_array, $monthly_post_count );
                array_push( $monthly_transaction, $monthly_count );
                array_push( $month_name_array, $day_name );
            }
            foreach ( $days_array_dates as $day_no => $day_namee ){
                array_push( $month_name_array_dates, $day_namee );
            }
        }
        if (!empty($monthly_post_count_array)){
            $max_no = max( $monthly_post_count_array );
            $max = round(( $max_no + 10/2 ) / 10 ) * 10;
        }else{
            $max_no = $max = 0;
        }
        if (!empty($monthly_transaction)){
            $max_no_daily = max( $monthly_transaction );
            $max_daily = round(( $max_no_daily + 10/2 ) / 10 ) * 10;
        }else{
            $max_no_daily = $max_daily = 0;
        }

        return array(
            'months' => $month_name_array,
            'labels' => $month_name_array_dates,
            'comments' => $monthly_post_count_array,
            'ubuntus' => $monthly_post_count_array,
            'max_Y_axis' => $max,
            'max_daily' => $max_daily,
            'daily_count' => $monthly_transaction,
        );
    }

    function getAllMonthsDays($month,$yr){
        if (!$yr){
            $year = Carbon::now()->year;
        }
        if ($yr){
            $year=$yr;
        }

        $days_array = array();
        $days_array_dates = array();
        $posts_dates = Transaction::whereMonth( 'created_at',$month )
            ->whereYear('created_at', $year)
            ->orderBy('created_at','asc')
            ->pluck( 'created_at');
        $posts_dates = json_decode( $posts_dates );
        if ( ! empty( $posts_dates ) ) {
            foreach ( $posts_dates as $unformatted_date ) {
                try {
                    $date = new \DateTime($unformatted_date);
                } catch (\Exception $e) {
                }
                $day_dates = Carbon::parse($date)->isoFormat('MMM Do');
                $day_no = $date->format( 'd' );
                $day_name = $date->format( 'D' );
                $days_array[ $day_no ] = $day_name;
                $days_array_dates[ $day_dates ] = $day_dates;
            }
        }
        $days_array = array(
            'days_array' => $days_array,
            'month' => $month,
            'days_array_dates' => $days_array_dates,
        );
        return $days_array;
    }

    function getAllMonthsTransaction(){
        $year = Carbon::now()->year;
        $posts_dates = Transaction::whereYear('created_at', $year)
            ->where('status', true)
            ->orderBy('created_at','asc')
            ->pluck( 'created_at');

        $posts_dates = json_decode( $posts_dates );
        
        if ( ! empty( $posts_dates ) ) {
            foreach ( $posts_dates as $unformatted_date ) {
                try {
                    $date = new \DateTime($unformatted_date);
                } catch (\Exception $e) {
                }
                $day_dates = $date->format( 'F' );
                $day_no = $date->format( 'm' );
                $day_name = $date->format( 'M' );
                $days_array[ $day_no ] = $day_name;
                $days_array_dates[ $day_dates ] = $day_dates;
            }
        }
        $days_array = array(
            'months_array' => $days_array,
            'months_arrays_names' => $days_array_dates,
        );
        return $days_array;
    }

    function getAllMonthsUsers(){
        $year = Carbon::now()->year;
        $posts_dates = User::whereYear('created_at', $year)
            ->orderBy('created_at','asc')
            ->pluck( 'created_at');

        $posts_dates = json_decode( $posts_dates );
        
        if ( ! empty( $posts_dates ) ) {
            foreach ( $posts_dates as $unformatted_date ) {
                try {
                    $date = new \DateTime($unformatted_date);
                } catch (\Exception $e) {
                }
                $day_dates = $date->format( 'F' );
                $day_no = $date->format( 'm' );
                $day_name = $date->format( 'M' );
                $days_array[ $day_no ] = $day_name;
                $days_array_dates[ $day_dates ] = $day_dates;
            }
        }
        $days_array = array(
            'months_array_users' => $days_array,
            'months_arrays_names_users' => $days_array_dates,
        );
        return $days_array;
    }

    function getMonthlyAmountCounts($day,$month,$yr) {
        if (!$yr){
            $year = Carbon::now()->year;
        }
        if ($yr){
            $year=$yr;
        }
        return Transaction::whereDay( 'created_at', $day)
            ->whereMonth('created_at', $month)
            ->whereYear('created_at', $year)
            ->get()
            ->sum('amount');
    }

    function getMonthsTransactionsTotal($month) {
        $year = Carbon::now()->year;
        return Transaction::whereMonth('created_at', $month)
            ->whereYear('created_at', $year)
            ->where('status', true)
            ->get()
            ->sum('amount');
    }

    function getMonthsUsersTotal($month) {
        $year = Carbon::now()->year;
        return User::whereMonth('created_at', $month)
            ->whereYear('created_at', $year)
            ->count();
    }

    function getDailyAmountCounts($day,$month,$yr) {
        if (!$yr){
            $year = Carbon::now()->year;
        }
        if ($yr){
            $year=$yr;
        }
        return Transaction::whereDay( 'created_at', $day)
            ->whereMonth('created_at', $month)
            ->where('status', true)
            ->whereYear('created_at', $year)
            ->count();
    }

    function getMonthlyAmountCountsEngagement($day,$month) {
        $monthly_post_count = Transaction::whereDay( 'created_at', $day)
            ->where('status', true)
            ->whereMonth('created_at', $month)
            ->get();
        return $monthly_post_count->sum('amount');
    }

    function getDailyAmountCountsEngagement($day,$month) {
        $monthly_count = Transaction::whereDay( 'created_at', $day)
            ->where('status', true)
            ->whereMonth('created_at', $month)
            ->count();
        return $monthly_count;
    }

    //weekly filters
    function getMonthlyPostDataWeekly($month, $year=null) {
        if ($month == 2){
            $monthly_post_count_array = array();
            $monthly_transaction = array();
            $day_mon_array = $this->getAllMonthsDays($month,$year);
            $days_array = $day_mon_array['days_array'];
            $month = $day_mon_array['month'];
            $days_array_dates = $day_mon_array['days_array_dates'];

            //get users
            $users_days_array = $this->getAllWeeklyUsersDaysMonthly();
            $days_array_users = $users_days_array['users_days_array'];
            $days_array_dates_users = $users_days_array['users_days_array_dates'];


            $month_name_array = array();
            $month_name_array_dates = array();
            $total = $this->getTotalMonthlyAmount();

            if ( ! empty( $days_array ) ) {
                foreach ( $days_array as $day_no => $day_name ){
                    $monthly_post_count = $this->getMonthlyAmountCounts( $day_no ,$month,$year);
                    $monthly_count = $this->getDailyAmountCounts( $day_no ,$month,$year);
                    array_push( $monthly_post_count_array, $monthly_post_count );
                    array_push( $monthly_transaction, $monthly_count );
                    array_push( $month_name_array, $day_name );
                }
                foreach ( $days_array_dates as $day_no => $day_namee ){
                    array_push( $month_name_array_dates, $day_namee );
                }
            }

            //for users
            $week_name_array_users = array();

            $weekly_post_count_array_users = array();
            $weekly_transaction_users = array();
            if ( ! empty( $days_array_users ) ) {
                foreach ( $days_array_users as $day_no_user => $day_name_user ){
                    $week_post_count_users = $this->getUsersCountsMonthly($day_no_user);
                    array_push( $weekly_post_count_array_users, $week_post_count_users );

                    array_push( $week_name_array_users, $day_name_user );
                }
                foreach ( $days_array_dates_users as $day_no_user => $day_name_users ){
                    array_push( $weekly_transaction_users, $day_name_users );
                }
            }

            if (!empty($monthly_post_count_array)){
                $max_no = max( $monthly_post_count_array );
                $max = round(( $max_no + 10/2 ) / 10 ) * 10;
            }else{
                $max_no = $max = 0;
            }
            if (!empty($monthly_transaction)){
                $max_no_daily = max( $monthly_transaction );
                $max_daily = round(( $max_no_daily + 10/2 ) / 10 ) * 10;
            }else{
                $max_no_daily = $max_daily = 0;
            }
            if (!empty($weekly_post_count_array_users)){
                //max users
                $max_no_daily_users = max( $weekly_post_count_array_users );
                $max_daily_users = round(( $max_no_daily_users + 10/2 ) / 10 ) * 10;
            }else{
                $max_no_daily_users = $max_daily_users = 0;
            }

            $daily_post_data_array = array(
                'text'=>'Weekly Transaction',
                'months' => $month_name_array,
                'labels' => $month_name_array_dates,
                'labels_users' => $weekly_transaction_users,
                'comments' => $monthly_post_count_array,
                'categories_users' => $weekly_post_count_array_users,
                'max_Y_axis' => $max,
                'total_new_users'=>array_sum($weekly_post_count_array_users),
                'max_users' => $max_daily_users,
                'max_daily' => $max_daily,
                'daily_count' => $monthly_transaction,
                'total_weekly_amount'=>number_format($total),
            );
        }
        if ($month == 2021){
            $monthly_post_count_array = array();
            $monthly_transaction = array();
            $months_names = $this->getAllMonthsTransaction();
            $months_arrays_names = $months_names['months_arrays_names']; //x-axis-transcations
            $month_name = $months_names['months_array']; //months ie 2

            $monthly_transaction_count = array();
            $months_arrays_name_users = array();
            $months_arrays_name_transaction = array();

            //here
            if ( ! empty( $month_name ) ) {
                foreach ( $month_name as $month_no => $day_name ){
                    $monthly_post_count = $this->getMonthsTransactionsTotal( $month_no);
                    array_push( $monthly_transaction_count, $monthly_post_count );
                }
                foreach ( $months_arrays_names as $months_arrays_name => $day_namee ){
                    array_push( $months_arrays_name_transaction, $day_namee );
                }

            }
            //users
            $monthly_users_count = array();

            $months_names_users = $this->getAllMonthsUsers();

            $months_arrays_users = $months_names_users['months_arrays_names_users']; //x-axis-users
            $month_users = $months_names_users['months_array_users']; //months ie 2

            if ( ! empty( $month_users ) ) {
                foreach ( $month_users as $month_user => $day_names ){
                    $monthly_count_users = $this->getMonthsUsersTotal($month_user);
                    array_push( $monthly_users_count, $monthly_count_users );
                }
                foreach ( $months_arrays_users as $months_arrays_name => $day_namee ){
                    array_push( $months_arrays_name_users, $day_namee );
                }
            }
            $total_transactions = array_sum($monthly_transaction_count);
            $total_users = array_sum($monthly_users_count);

            if (!empty($monthly_users_count)){
                $max_no = max( $monthly_users_count );
                $max = round(( $max_no + 10/2 ) / 10 ) * 10;
            }else{
                $max_no = $max = 0;
            }
            if (!empty($monthly_transaction_count)){
                $max_no = max( $monthly_transaction_count );
                $max = round(( $max_no + 10/2 ) / 10 ) * 10;
            }else{
                $max_no = $max = 0;
            }
            $daily_post_data_array = array(
                'months' => $months_arrays_name_users,
                'labels' => $months_arrays_name_transaction,
                'labels_users' => $months_arrays_name_users,
                'comments' => $monthly_transaction_count,
                'categories_users' => $monthly_users_count,
                'max_Y_axis' => $max,
                'total_new_users'=>$total_users,
                'max_users' => $max_no,
                'max_daily' => $max_no,
                'daily_count' => $monthly_transaction,
                'total_weekly_amount'=>$total_transactions,
            );
        }

        else{
            $monthly_post_count_array = array();
            $monthly_transaction = array();
            $day_mon_array = $this->getAllMonthsDaysWeekly();
            $days_array = $day_mon_array['days_array'];
            $days_array_dates = $day_mon_array['days_array_dates'];
            //get users
            $users_days_array = $this->getAllWeeklyUsersDays();
            $days_array_users = $users_days_array['users_days_array'];
            $days_array_dates_users = $users_days_array['users_days_array_dates'];

            $total = $this->getTotalWeeklyAmount();
            $month_name_array = array();
            $month_name_array_dates = array();

            if ( ! empty( $days_array ) ) {
                foreach ( $days_array as $day_no => $day_name ){
                    $monthly_post_count = $this->getMonthlyAmountCountsWeekly($day_no);
                    $monthly_count = $this->getDailyAmountCounts( $day_no ,$month,$year);
                    array_push( $monthly_post_count_array, $monthly_post_count );
                    array_push( $monthly_transaction, $monthly_count );

                    array_push( $month_name_array, $day_name );
                }
                foreach ( $days_array_dates as $day_no => $day_namee ){
                    array_push( $month_name_array_dates, $day_namee );
                }
            }

            //for users
            $week_name_array_users = array();

            $weekly_post_count_array_users = array();
            $weekly_transaction_users = array();
            if ( ! empty( $days_array_users ) ) {
                foreach ( $days_array_users as $day_no_user => $day_name_user ){
                    $week_post_count_users = $this->getUsersCountsWeekly($day_no_user);
                    array_push( $weekly_post_count_array_users, $week_post_count_users );

                    array_push( $week_name_array_users, $day_name_user );
                }
                foreach ( $days_array_dates_users as $day_no_user => $day_name_users ){
                    array_push( $weekly_transaction_users, $day_name_users );
                }
            }

            $total_users = array_sum($weekly_post_count_array_users);

            if (!empty($monthly_post_count_array)){
                $max_no = max( $monthly_post_count_array );
                $max = round(( $max_no + 10/2 ) / 10 ) * 10;
            }else{
                $max_no = $max = 0;
            }
            if (!empty($monthly_transaction)){
                $max_no_daily = max( $monthly_transaction );
                $max_daily = round(( $max_no_daily + 10/2 ) / 10 ) * 10;
            }else{
                $max_no_daily = $max_daily = 0;
            }
            if (!empty($weekly_post_count_array_users)){
                //max users
                $max_no_daily_users = max( $weekly_post_count_array_users );
                $max_daily_users = round(( $max_no_daily_users + 10/2 ) / 10 ) * 10;
            }else{
                $max_no_daily_users = $max_daily_users = 0;
            }

            $daily_post_data_array = array(
                'text'=>'Weekly Transaction',
                'months' => $month_name_array,
                'labels' => $month_name_array_dates,
                'labels_users' => $weekly_transaction_users,
                'comments' => $monthly_post_count_array,
                'categories_users' => $weekly_post_count_array_users,
                'max_Y_axis' => $max,
                'total_new_users'=>$total_users,
                'max_users' => $max_daily_users,
                'max_daily' => $max_daily,
                'daily_count' => $monthly_transaction,
                'total_weekly_amount'=>number_format($total),
            );
        }
        return $daily_post_data_array;
    }

    function getAllMonthsDaysWeekly(){
        $days_array = array();
        $days_array_dates = array();
        $posts_dates = Transaction ::whereBetween('created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])
            ->where('status', true)
            ->orderBy('created_at','asc')
            ->pluck( 'created_at');
        $posts_dates = json_decode( $posts_dates );
        if ( ! empty( $posts_dates ) ) {
            foreach ( $posts_dates as $unformatted_date ) {
                try {
                    $date = new \DateTime($unformatted_date);
                } catch (\Exception $e) {
                }
                $day_dates = Carbon::parse($date)->isoFormat('MMM Do');
                $day_no = $date->format( 'd' );
                $day_name = $date->format( 'D' );
                $days_array[ $day_no ] = $day_name;
                $days_array_dates[ $day_dates ] = $day_dates;
            }
        }
        $days_array = array(
            'days_array' => $days_array,
            'days_array_dates' => $days_array_dates,
        );
        return $days_array;
    }

    function getMonthlyAmountCountsWeekly($day) {
        $date = \Carbon\Carbon::today()->subDays(7);
        $days = Transaction ::whereBetween('created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])
            ->where('status', true)
            ->whereDay( 'created_at', $day)
            ->get();
        return $days->sum('amount');
    }

    function getTotalWeeklyAmount() {
        $weekly_count = Transaction ::whereBetween('created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])
            ->where('status', true)
            ->get();
        return $weekly_count->sum('amount');
    }

    function getTotalMonthlyAmount() {
        $weekly_count = Transaction ::whereBetween('created_at', [Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth()])
            ->where('status', true)
            ->get();
        return $weekly_count->sum('amount');
    }

    function getTotalWeeklyUsers() {
        $weekly_count = User::whereBetween('created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])
            ->orderBy('created_at','asc')
            ->get();
        return $weekly_count->sum('id');
    }

    //all users
    function getAllWeeklyUsersDays(){
        $days_array = array();
        $days_array_dates = array();
        $posts_dates = User::whereBetween('created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])
            ->orderBy('created_at','asc')
            ->pluck( 'created_at');
        $days_array = array();
        $days_array_dates = array();

        $posts_dates = json_decode( $posts_dates );
        if ( ! empty( $posts_dates ) ) {
            foreach ( $posts_dates as $unformatted_date ) {
                try {
                    $date = new \DateTime($unformatted_date);
                } catch (\Exception $e) {
                }
                $day_dates = Carbon::parse($date)->isoFormat('MMM Do');
                $day_no = $date->format( 'd' );
                $day_name = $date->format( 'D' );
                $days_array[ $day_no ] = $day_name;
                $days_array_dates[ $day_dates ] = $day_dates;
            }
        }
        $users_days_array = array(
            'users_days_array' => $days_array,
            'users_days_array_dates' => $days_array_dates,
        );
        return $users_days_array;
    }

    function getUsersCountsWeekly($day) {
        $days = User::whereBetween('created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])
            ->whereDay( 'created_at', $day)
            ->get();
        return $days->count('id');
    }

    function getUsersCountsMonthly($day) {
        $days = User::whereBetween('created_at', [Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth()])
            ->whereDay( 'created_at', $day)
            ->get();
        return $days->count('id');
    }
    //all users monthly
    function getAllWeeklyUsersDaysMonthly(){
        $days_array = array();
        $days_array_dates = array();
        $posts_dates = User::whereBetween('created_at', [Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth()])
            ->orderBy('created_at','asc')
            ->pluck( 'created_at');
        $days_array = array();
        $days_array_dates = array();

        $posts_dates = json_decode( $posts_dates );
        if ( ! empty( $posts_dates ) ) {
            foreach ( $posts_dates as $unformatted_date ) {
                try {
                    $date = new \DateTime($unformatted_date);
                } catch (\Exception $e) {
                }
                $day_dates = Carbon::parse($date)->isoFormat('MMM Do');
                $day_no = $date->format( 'd' );
                $day_name = $date->format( 'D' );
                $days_array[ $day_no ] = $day_name;
                $days_array_dates[ $day_dates ] = $day_dates;
            }
        }
        $users_days_array = array(
            'users_days_array' => $days_array,
            'users_days_array_dates' => $days_array_dates,
        );
        return $users_days_array;
    }

    public function totalUsersPerMonth()
    {
        // Get past 4 months
        $months = [];
        $totalUsers = [];
        $days = [0, 29, 59, 89];
        foreach($days as $day) {
            array_push($months, Carbon::now()->subDays($day));
        }
        
        // Get total users for each month
        foreach ($months as $key => $month) {
            $users = User::whereBetween('created_at', [Carbon::parse($month)->startOfMonth(), Carbon::parse($month)->endOfMonth()])->count();
            array_push($totalUsers, $users);
        }
        // Format months
        $monthsFormatted = [];
        foreach ($months as $key => $month) {
            array_push($monthsFormatted, $month->format('M'));
        }
        return array(
            'months' => $monthsFormatted,
            'totalUsers' => $totalUsers
        );
    }

    public function getTransactionsPerMonth()
    {
        // Get past 4 months
        $months = [];
        $totalAmount = [];
        $transactionsCount = [];
        $days = [0, 29, 59, 89];
        foreach($days as $day) {
            array_push($months, Carbon::now()->subDays($day));
        }
        // for ($i=1; $i < 5; $i++) { 
        //     array_push($months, Carbon::now()->subMonths($i));
        // }
        // Get total users for each month
        foreach ($months as $key => $month) {
            $transactionsData = BookPayment::whereBetween('created_at', [Carbon::parse($month)->startOfMonth(), Carbon::parse($month)->endOfMonth()])->whereNotNull('amount')->get();
            array_push($totalAmount, $transactionsData->sum('amount'));
            array_push($transactionsCount, $transactionsData->count());
        }
        // Format months
        $monthsFormatted = [];
        foreach ($months as $key => $month) {
            array_push($monthsFormatted, $month->format('M'));
        }
        return array(
            'months' => $monthsFormatted,
            'totalAmount' => $totalAmount,
            'transactionsCount' => $transactionsCount
        );
    }

    public function transactionsInPastWeek()
    {
        // Get past 5 days
        $days = [];
        $totalAmount = [];
        $totalBooks = [];
        $daysFormatted = [];
        for ($i=1; $i < 6; $i++) { 
            array_push($days, Carbon::now()->subDays($i));
        }

        // Get todays data
        $todaysData = BookPayment::whereBetween('created_at', [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])->whereNotNull('amount')->where('completed', true)->get();
        array_push($totalAmount, $todaysData->sum('amount'));
        array_push($totalBooks, $todaysData->count());

        // Get total transactions daily for the past 5 days
        foreach ($days as $key => $day) {
            $booksData = BookPayment::whereBetween('created_at', [Carbon::parse($day)->startOfDay(), Carbon::parse($day)->endOfDay()])->whereNotNull('amount')->where('completed', true)->get();
            array_push($totalAmount, $booksData->sum('amount'));
            array_push($totalBooks, $booksData->count());
        }

        // Add todays date to formatted date
        array_push($daysFormatted, Carbon::now()->format('d-M'));

        // Format days
        foreach ($days as $key => $day) {
            array_push($daysFormatted, $day->format('d-M'));
        }

        return array(
            'days' => $daysFormatted,
            'totalAmount' => $totalAmount,
            'totalBooks' => $totalBooks
        );
    }
}
