<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Author;
use App\Models\Publisher;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class AuthorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.authors.index');
    }
    /**
     * Get Users DataTable
     *
     * @return \Illuminate\Http\Response
     */
    public function getAuthors()
    {
        $users = Author::all();
        return Datatables::of($users)
            ->addColumn('action', function ($users) {
                return '<div class="dropdown dropdown-inline">
								<a href="" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown">
	                                <i class="la la-cog"></i>
	                            </a>
							  	<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
									<ul class="nav nav-hoverable flex-column">
							    		<li class="nav-item" ><a class="nav-link edit" href="#" id="'.$users->id.'"><i class="nav-icon la la-edit"></i><span class="nav-text">Edit Details</span></a></li>
							    		<li class="nav-item"><a class="nav-link" href="'.route('admin.authors.delete',$users->id).'"><i class="nav-icon la la-user"></i><span class="nav-text">Delete</span></a></li>
							    	</ul>
							  	</div>
							</div>
						';
            })
            ->editColumn('created_at', function ($providers){
                return Carbon::parse($providers->created_at)->format('Y-m-d h:i:s');
            })
            ->make(true);
    }
    function fetchdat(Request $request)
    {
        $id = $request->input('id');
        $category = Author::find($id);
        $output = array(
            'full_name'    =>  $category->full_name,
            'email'    =>  $category->email,
            'phone_number'    =>  $category->phone_number,
        );
        echo json_encode($output);
    }

    function addAuthor(Request $request)
    {
       // return $request->all();
        $validation = Validator::make($request->all(), [
            'full_name' => 'required',
            'email' => 'required_if:phone_number,NULL',
            'phone_number' => 'required_if:email,NULL',
        ], [
            'email.required_if' => 'Please enter the email',
            'phone_number.required_if' => 'Please enter the phone number',
        ]);
        $error_array = array();
        $success_output = '';
        if ($validation->fails())
        {
            foreach ($validation->messages()->getMessages() as $field_name => $messages)
            {
                $error_array[] = $messages;
            }
        }
        else
        {
            if($request->get('button_action') == 'insert')
            {
                $full_name = $request->input('full_name');
                $email = $request->input('email');
                $phone_number = $request->input('phone_number');
                $created_at = Carbon::now();
                $updated_at = Carbon::now();

                $data = array(
                    'full_name' => $full_name,
                    "email" => $email == '' ? NULL : $email,
                    "phone_number" => $phone_number == '' ? NULL : $phone_number, 
                    "created_at" => $created_at,
                    "updated_at" => $updated_at
                );
                DB::table('authors')->insert($data);

                activity()
                    ->causedBy(Auth::user())
                    ->inLog('general')
                    ->log(Auth::user()->first_name.' '.Auth::user()->last_name.' added an Author, '.$full_name);
                
                $success_output = '<div class="alert alert-success">Author Create Successfully</div>';
            }
            if($request->get('button_action') == 'update')
            {
                $author = Author::find($request->get('id'));
                $author->full_name = $request->get('full_name');
                $author->email = $request->get('email');
                $author->phone_number = $request->get('phone_number');
                $author->save();
                activity()
                    ->causedBy(Auth::user())
                    ->inLog('general')
                    ->log(Auth::user()->first_name.' '.Auth::user()->last_name.' updated the Author, '.$author->full_name);
                
                $success_output = '<div class="alert alert-success">Author Updated Successful</div>';
            }

        }
        $output = array(
            'error'     =>  $error_array,
            'success'   =>  $success_output
        );
        echo json_encode($output);
    }
    public function dest($id)
    {
        $data = Author::findOrFail($id);
        $data->delete();
        activity()
            ->causedBy(Auth::user())
            ->inLog('general')
            ->log(Auth::user()->first_name.' '.Auth::user()->last_name.' deleted the author '.$data->first_name.' '.$data->last_name);
    }
    public function destroys($id)
    {
        $data = Author::findOrFail($id);
        $data->delete();
        activity()
            ->causedBy(Auth::user())
            ->inLog('general')
            ->log(Auth::user()->first_name.' '.Auth::user()->last_name.' deleted the author '.$data->first_name.' '.$data->last_name);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    function fetchdatasss(Request $request)
    {
        $id = $request->input('id');
        $category = Author::find($id);
        $output = array(
            'name'    =>  $category->name,
        );
        echo json_encode($output);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function show(Author $author)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function edit(Author $author)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Author $author)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function destroy(Author $author)
    {
        //
    }
}
