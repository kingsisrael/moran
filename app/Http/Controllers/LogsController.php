<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;
use Yajra\DataTables\Facades\DataTables;

class LogsController extends Controller
{
    public function generalLogs()
    {
        return view('admin.logs.general-logs');
    }

    public function transactionsLogs()
    {
        return view('admin.logs.transactions-logs');
    }

    public function generalLogsTabledata()
    {
        $logs = Activity::inLog('general')->orderBy('created_at', 'DESC')->get();
        return Datatables::of($logs)
            ->addIndexColumn()
            ->editColumn('created_at', function($logs) {
                return Carbon::parse($logs->created_at)->format('d M Y h:i');
            })

            ->make(true);
    }

    public function transactionsLogsTabledata()
    {
        $logs = Activity::inLog('transactions')->orderBy('created_at', 'DESC')->get();
        return Datatables::of($logs)
            ->addIndexColumn()
            ->editColumn('created_at', function($logs) {
                return Carbon::parse($logs->created_at)->format('d M Y h:i');
            })

            ->make(true);
    }
}
