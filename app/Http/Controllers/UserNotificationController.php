<?php

namespace App\Http\Controllers;

use App\Models\UserNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserNotificationController extends Controller
{
    public function getUserNotifications()
    {
        if (!Auth::check()) {
            return response()->json([
                'message' => 'Please login to view Notifications'
            ], 401);
        }

        $notifications = UserNotification::where('user_id', auth()->user()->id)->where('isRead', false)->get();

        return response()->json([
            'notifications' => $notifications,
            'notifications_count' => $notifications->count()
        ], 200);
    }

    public function getUserNotification($id)
    {
        if (!Auth::check()) {
            return response()->json([
                'message' => 'Please login to view Notifications'
            ], 401);
        }

        $notification = UserNotification::where('id', $id)->where('user_id', auth()->user()->id)->where('isRead', false)->first();

        return response()->json($notification, 200);
    }

    public function markNotificationAsRead(Request $request)
    {
        if (!Auth::check()) {
            return response()->json([
                'message' => 'Please login to perform this action'
            ], 401);
        }

        $this->validate($request, [
            'notification_id' => ['required']
        ]);

        $notification = UserNotification::find($request->notification_id);
        $notification->isRead = true;
        $notification->save();

        return response()->json([
            'message' => 'Notification marked as read'
        ], 200);
    }

    public function markAllNotificationsAsRead()
    {
        if (!Auth::check()) {
            return response()->json([
                'message' => 'Please login to perform this action'
            ], 401);
        }

        $notifications = UserNotification::where('user_id', auth()->user()->id)->where('isRead', false)->get();

        if ($notifications->count()) {
            collect($notifications)->each(function ($notification) {
                $notification->isRead = true;
                $notification->save();
            });
        }

        return response()->json([
            'message' => 'All notifications marked as read'
        ], 200);
    }
}
