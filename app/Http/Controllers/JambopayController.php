<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\BookPayment;
use App\Models\Jambopay;
use App\Models\User;
use App\Models\UserLibrary;
use App\Models\UserNotification;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class JambopayController extends Controller
{
    public function accessToken()
    {
        $url = 'https://checkout.jambopay.com/Token';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Authorization: Bearer zpzQh7Ohg1UsZAQ3UJ8tvwhBeLM7kS5JdAq4fN8Cawfnce-QUYML8NWezXbmqcpEwoUIiWs9DcRRsFUHMGJpAsoEWUlnrANUqLyT_7zRNMY5GEDGAcna-A2DpSKXZdlsJOUx68AqLhRUYa7MjOJPn1HGo1xkQUHK74C-KCVcu2OjxQJHjoY3XfFkruBmjXtGbcaUkkfeUp9NkgPXthjZ8b2_mWsAR-NeDojNKlsP4Oka2McbLsrBvVAoXEyH6Ul-SQOrHQMSvTmWDUXmKZzR31-T6qbRPw9NgSyk6thzAU5ezOtPs5ozL4t4N0t0aK-p2PtEOYhodt2brd92Y6ZCnFygeKHjTRxjw0tIYpSwz5PKYonnMWiYAG7crlu-7mc21JkNo0TUc4dhvW1gVeWY2hEybpHx5_AfiqE6W7pAL78rExIQCuBZoYGhPGmYZnauSMCw6geGwPdfTUng0WqW2g2g4eJ9OJSZ5RdCnpDvUpuCTk08dR7DGarHpegzf2AyUZwrrX5HYeKVzfqT_erQPQ',
                'Content-Type: application/x-www-form-urlencoded',
                'Cookie: .AspNet.Cookies=Fu7W5t8i35uUUqMN_0hbhGQTlVq6fLUA019OWX1NBeiOskPja7aSn5EPvSQA-lpS4faI3IBbNTXxGENemG9Jh_gew5xdNyGP1dizlR-KPwsAotIChSIpb0Ak4daRY71XH_Dvkp7dQdTxaBSFYHvTlz5HQa5yoADkmqLP-bpPoagozDcGw7hOcRl3xTMf58NKZcKObV0IueZdarfhh_hZRxBLZ9lLy_Wgq-f46GWMo8NEUFadvD3etiOFD6qNZ5AgJNxBP3e-USmqzmxynlj-e4qmxFuT-YJAzAUn5EUR6VSA68c_LnLWODH6qNWQ-N49MSpi12YynTKHSBa1rAKxdGDGwjSZDVyWqH8Vl0m1xTENOvXbsydFtjG5feiaXLk3mezD3_hkLFllHaWp-HmZ-yKOi6nOlXNlDuyfqE7W5lSDwebLpOKfpWoj_Es_NKrGwZceSRbjjCi2ZDGrdehu4M6olYHzZzL8jL4rIkxGx_I'
            )
        );
        curl_setopt($curl, CURLOPT_POSTFIELDS,
            'username=commerce%40moranpublishers.co.ke&Grant_type=password&Password=Moran01!&clientKey=4abb3108464d4cf4adaa8e5197ed6433ec38354657f7448f99a17f81f3979cf4dfaafb72515c418b9c46b1d85d46bc2e',
        );

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        $access_token=json_decode($curl_response);
        curl_close($curl);
        return $access_token->access_token;
    }

    public function jambopay(Request $request)
    {
        // $prefix = "#";
        // $order_number = IdGenerator::generate(['table' => 'jambopays','field'=>'order_number', 'length' => 8, 'prefix'  =>$prefix]);
        $bookMpesa = BookPayment::where('book_id', $request->book_id)->where('user_id', $request->user_id)->where('status', 1)->first();
        if ($bookMpesa) {
            return response()->json(['message' => 'This Book already bought'], 400);
        }

        $bookjp = Jambopay::where('book_id', $request->book_id)->where('user_id', $request->user_id)->first();
        if ($bookjp) {
            return response()->json(['message' => 'This Book already bought'], 400);
        }

        $book = Book::find($request->book_id);

        $jp = Jambopay::create([
            'user_id' => $request->user_id,
            'Amount' => $book->price,
            'Order_Id' => $request->rand,
            'order_number' => 6,
            'book_id' => $request->book_id,
            'book_name' => $request->name_book,
        ]);

        if($jp){
            $user = User::find($request->user_id);
            return response()->json(
                [
                    'success' => true,
                    'user' => $user,
                    'book' => $book,
                    'message' => $jp->Order_Id,
                    'token' => $this->accessToken(),
                ]
            );
        }

    }

    public function checkout(Request $request){
        DB::table('jambopays')
            ->where('Order_Id', $request->Order_Id)
            ->update([
                'Amount'=>$request->Amount,
                'Currency'=>$request->Currency,
                'Receipt'=>$request->Receipt,
                'Secure_Hash'=>$request->Checksum,
                'Status'=>$request->Status,
                'Timestamp'=>$request->Timestamp,
                'completed' => 1,
                'status' => 1,
                'channel' => "Jambopay",
            ]);
        
        $transaction = Jambopay::where('Order_Id', $request->Order_Id)->first();
        $book = Book::find($transaction->book_id);
        $user = User::find($transaction->user_id);

        $user->library()->create([
            'book_id' => $book->id
        ]);

        activity()
            ->inLog('transactions')
            ->causedBy($user)
            ->log($user->first_name.' '.$user->last_name.' bought the book '.$book->title.' using Jambopay');

        return view('transaction-successful');
    }

    public function showSuccessfulTransaction()
    {
        return view('transaction-successful');
    }

    public function cancelTransaction()
    {
        return view('cancel-transaction');
    }
}
