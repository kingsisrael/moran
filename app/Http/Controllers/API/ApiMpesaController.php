<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Models\BookPayment;
use App\Models\OrderBook;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

use App\Event;
use App\EventPayment;
use App\Jobs\EventOrderEmail;
use App\Jobs\OrderEmail;
use App\Order;
use App\OrderEvent;
use App\Package;
use App\Models\User;
use App\Models\UserLibrary;
use App\Models\UserNotification;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Haruncpi\LaravelIdGenerator\IdGenerator;
class ApiMpesaController extends Controller

{
   public function mpesaPassword()
    {
        //timestamp
        $timestamp = Carbon::rawParse('now')->format('YmdHis');
        //passkey
        $passKey ="9f1a64867deef724e2274631d3f6e637f5e764807b3e3a718e7a34a5165453fd";
        $businessShortCOde =912500;
        //generate password
        $mpesaPassword = base64_encode($businessShortCOde.$passKey.$timestamp);

        return $mpesaPassword;
    }


    public function accessToken()
    {
        $consumer_key="wZGDrdhK38Ivwbt12SfGaG5aA2xSN08x";
        $consumer_secret="tV0OiSBV8W9g49AA";
        $credentials = base64_encode($consumer_key.":".$consumer_secret);
        // $url = "https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials";
        $url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Basic ".$credentials));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        $access_token=json_decode($curl_response);
        curl_close($curl);

        return $access_token->access_token;
    }

    public function tests(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone_number' => 'required|numeric|min:9',
            'book_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => 'Phone Number and book_id are required'], 400);
        }

        //book price
        $id = $request->get('book_id');

        $inLibrary = UserLibrary::where('user_id', auth()->user()->id)->where('book_id', $id)->first();

        if ($inLibrary) {
            return response()->json(['message' => 'This Books is already bought'], 400);
        }

        $bk = BookPayment::where('user_id', Auth::id())->where('book_id', $id)->where('status', 1)->first();
        if ($bk) {
            return response()->json(['message' => 'This Book already bought'], 400);
        }
        
        $bookPayments = BookPayment::all();
        $orderNumbers = collect($bookPayments)->pluck('order_number');
        $order_number = mt_rand(100000, 9999999);
        if($orderNumbers->contains($order_number)) {
            $order_number = mt_rand(100000, 9999999);
        }
        
        $AccountReference = $order_number;
        $package = Book::where('id', '=', $id)->firstOrFail();
        $amount = $package->price;
        $phone = $request->phone_number;
        $PartyA = '254' . substr($phone, -9);
        $BusinessShortCode = '912500';
        $PartyB = $BusinessShortCode;
        $PhoneNumber = '254' . substr($phone, -9);
        $Remark = "Book Purchase";

        $CallBackURL = "https://moranpublishers.co.ke/mpesa/callback";
        $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
        // $url = 'https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
        $curl_post_data = [
            'BusinessShortCode' => $PartyB,
            'Password' => $this->mpesaPassword(),
            'Timestamp' => Carbon::now()->format('YmdHis'),
            'TransactionType' => 'CustomerPayBillOnline',
            'Amount' => $amount,
            'PartyA' => $PartyA,
            'PartyB' => $PartyB,
            'PhoneNumber' => $PhoneNumber,
            'CallBackURL' => $CallBackURL,
            'AccountReference' => $AccountReference,
            'TransactionDesc' => $Remark
        ];
        $data_string = json_encode($curl_post_data);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '. $this->accessToken()));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        $curl_response = curl_exec($curl);
        $result = json_decode($curl_response);
        info(json_encode($result));
        if (isset($result->ResponseCode)) {
            if ($result->ResponseCode == "0") {
                $mpesa_transaction = BookPayment::create([
                    'user_id' => Auth::id(),
                    'book_id' => $package->id,
                    'book_name' => $package->title,
                    'order_number' => $order_number,
                    'phoneNumber' => $PhoneNumber,
                    'MerchantRequestID' => $result->MerchantRequestID,
                    'CheckoutRequestID' => $result->CheckoutRequestID,
                ]);
                if ($mpesa_transaction) {
                    return response()->json(['success' => 'Transaction being processed']);
                }
            }
            else{
                return response()->json(['Error' => $result]);
            }
        }
        elseif ($result->errorCode == "400.002.02") {
            return response()->json(['Error' => $result->errorMessage]);
        }
        elseif ($result->errorCode == "500.002.1001") {
            return response()->json(['Error' => $result->errorMessage]);
        }
        elseif($result->errorCode == "500.001.1001") {
            return response()->json(['Error' => $result]);
        }
        else {
            return response()->json(['Error' => $result]);
        }
    }
    public static function generateLiveTokens()
    {
        $consumer_key = 'wZGDrdhK38Ivwbt12SfGaG5aA2xSN08x';
        $consumer_secret = 'tV0OiSBV8W9g49AA';

        if (!isset($consumer_key) || !isset($consumer_secret)) {
            die("please declare the consumer key and consumer secret as defined in the documentation");
        }
        $url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

        if (!isset($consumer_key) || !isset($consumer_secret)) {
            die("please declare the consumer key and consumer secret as defined in the documentation");
        }

        $client = new Client();
        $credentials = base64_encode($consumer_key.':'.$consumer_secret);

        $res = $client->request('get', $url, [
            'verify' => false,
            'headers' => [
                'Authorization' => 'Basic ' . $credentials,
            ]
        ]);

        $res2 = (string)$res->getBody();
        $obj = json_decode((string)$res->getBody());
        $token = $obj->access_token;
        return $token;


    }

    public static function generateSandBoxTokens()
    {
        $consumer_key = 'KGGBIaoWBP6ixngnC3ngnAd94OK7LL2v';
        $consumer_secret = 'LFrL5I2XPb0VYD1P';
        if (!isset($consumer_key) || !isset($consumer_secret)) {
            die("please declare the consumer key and consumer secret as defined in the documentation");
        }

        $client = new Client();
        $credentials = base64_encode($consumer_key . ':' . $consumer_secret);

        $credentials2 = base64_encode('KGGBIaoWBP6ixngnC3ngnAd94OK7LL2v:LFrL5I2XPb0VYD1P');

        // dd($credentials, $credentials2);
        $res = $client->request('get', 'https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials', [
            'verify' => false,
            'headers' => [
                'Authorization' => 'Basic ' . $credentials,
            ]
        ]);
        $res2 = (string)$res->getBody();
        $obj = json_decode((string)$res->getBody());
        $token = $obj->access_token;
        return $token;
    }
    public function stkPush(Request $request)
    {
        // $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'phone_number' => 'required|numeric|min:9',
            'book_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()
                ->json(['message' => 'Phone Number and book_id are required'], 400);
        }
        $id = $request->get('book_id');//book price

        $bk = BookPayment::where('user_id', Auth::id())->where('book_id', $id)->where('status', 1)->first();
        if ($bk) {
            return response()->json(['message' => 'This Book already bought'], 400);
        }
        $prefix = "#";
        $order_number = IdGenerator::generate(['table' => 'book_payments', 'field' => 'order_number', 'length' => 8, 'prefix'  => $prefix]);
        $package = Book::where('id', '=', $id)->firstOrFail();
        // $amount = 2;
        $title = $order_number;

        // $user = $request->user;
        $amount = 1;
        $phone =  $request->phone_number;
        $BusinessShortCode = '912500';
        $PartyB = $BusinessShortCode;
        $PartyA = '254' . substr($phone, -9);
        $PhoneNumber = '254' . substr($phone, -9);
        $CallBackURL = "https://moranpublishers.co.ke/mpesa/callback";
        // $CallBackURL = "https://enqbsop7j1oz8.x.pipedream.net/";
        $AccountReference = $title;
        $Remark = "Book Purchase";
        // $url = 'https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
        $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
        $token = self::generateLiveToken();

        $curl_post_data = [
            'BusinessShortCode' =>$BusinessShortCode,
            'Password' => $this->lipaNaMpesaPassword(),
            'Timestamp' => Carbon::rawParse('now')->format('YmdHms'),
            'TransactionType' => 'CustomerPayBillOnline',
            'Amount' => $amount,
            'PartyA' => $PartyA,
            'PartyB' => $PartyB,
            'PhoneNumber' => $PhoneNumber,
            'CallBackURL' => $CallBackURL,
            'AccountReference' => $AccountReference,
            'TransactionDesc' => $Remark
        ];

        $data_string = json_encode($curl_post_data);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$token));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        $curl_response = curl_exec($curl);
        $result = json_decode($curl_response);
        // return response()->json($result);
        if (isset($result->ResponseCode)) {
            if ($result->ResponseCode == "0") {
                $mpesa_transaction = BookPayment::create([
                    'user_id' => 1,
                    'book_id' => $package->id,
                    'book_name' => $package->title,
                    'order_number' => $order_number,
                    'phoneNumber' => $PhoneNumber,
                    'MerchantRequestID' => $result->MerchantRequestID,
                    'CheckoutRequestID' => $result->CheckoutRequestID,
                ]);
                if ($mpesa_transaction) {
                    return response()->json(['success!' => $CallBackURL]);

                }
            }
            else{
                return response()->json(['Error!' => $result]);
            }
        }
        elseif ($result->errorCode == "400.002.02") {
            return response()->json(['Error!' => $result->errorMessage]);
        }
        elseif ($result->errorCode == "500.002.1001") {
            return response()->json(['Error!' => $result->errorMessage]);
        }
        elseif($result->errorCode == "500.001.1001") {
            return response()->json(['Error' => $result]);
        }
        else {
            return response()->json(['Error!' => $result]);
        }
    }

    public function lipaNaMpesaPassword()
    {
        $timestamp = Carbon::rawParse('now')->format('YmdHms');
        $passKey="bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919";
        // $passKey = "983a251dff82c3c83590c2873ded11ffbfc52deb53a38c34afab2865cc8640f";
        $businessShortCOde ="912500";
        $mpesaPassword = base64_encode($businessShortCOde.$passKey.$timestamp);

        return $mpesaPassword;
    }

    public function newAccessTokens()
    {
        $consumer_key="wZGDrdhK38Ivwbt12SfGaG5aA2xSN08x";
        $consumer_secret="tV0OiSBV8W9g49AA";
        $credentials = base64_encode($consumer_key.":".$consumer_secret);
        // $url = "https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials";
        $url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Basic ".$credentials,"Content-Type:application/json"));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        $access_token=json_decode($curl_response);
        curl_close($curl);

        return $access_token->access_token;
    }

    public static function generateLiveToken()
    {
        $consumer_key = '2dP5CiVNMKlPViOYzQWgp5v0OxO94BNV';
        $consumer_secret = 'DmzYB6A9nTWGEjt9';
        // $consumer_key = 'wZGDrdhK38Ivwbt12SfGaG5aA2xSN08x';
        // $consumer_secret = 'tV0OiSBV8W9g49AA';

        if (!isset($consumer_key) || !isset($consumer_secret)) {
            die("please declare the consumer key and consumer secret as defined in the documentation");
        }
        $url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

        if (!isset($consumer_key) || !isset($consumer_secret)) {
            die("please declare the consumer key and consumer secret as defined in the documentation");
        }

        $client = new Client();
        $credentials = base64_encode($consumer_key . ':' . $consumer_secret);

        $res = $client->request('get', $url, [
            'verify' => false,
            'headers' => [
                'Authorization' => 'Basic ' . $credentials,
            ]
        ]);

        $res2 = (string)$res->getBody();
        $obj = json_decode((string)$res->getBody());
        $token = $obj->access_token;
        return $token;
    }

    public static function generateSandBoxToken()
    {
        $consumer_key = 'wZGDrdhK38Ivwbt12SfGaG5aA2xSN08x';
        $consumer_secret = 'tV0OiSBV8W9g49AA';
        if (!isset($consumer_key) || !isset($consumer_secret)) {
            die("please declare the consumer key and consumer secret as defined in the documentation");
        }

        $client = new Client();
        $credentials = base64_encode($consumer_key . ':' . $consumer_secret);

        // $credentials2 = base64_encode('r5zgeQurrIsm9Amj9dYnmf3AWGYtIsSx:fljRZNx2VKCyT0UQ');

        // dd($credentials, $credentials2);
        $res = $client->request('get', 'https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials', [
            'verify' => false,
            'headers' => [
                'Authorization' => 'Basic ' . $credentials,
            ]
        ]);
        $res2 = (string)$res->getBody();
        $obj = json_decode((string)$res->getBody());
        $token = $obj->access_token;
        return $token;
    }

    /**************************************USED ON FORM*************************************************/
    public function payments(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'phone_number' => 'required|numeric|min:9',
            'book_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()
                ->json(['message' => 'Phone Number and book_id are required'], 400);
        }
        $id = $request->get('book_id');//book price

        $bk = BookPayment::where('user_id', Auth::id())->where('book_id', $id)->where('status', 1)->first();
        if ($bk) {
            return response()->json(['message' => 'This Book already bought'], 400);
        }
        $prefix = "#";
        // $order_number = IdGenerator::generate(['table' => 'book_payments','field'=>'order_number', 'length' => 8, 'prefix'  =>$prefix]);
        $bookPayments = BookPayment::all()->pluck('order_number');
        $order_number = $prefix.mt_rand(10000, 99999);
        while($bookPayments->contains($order_number)) {
            $order_number = $prefix.mt_rand(10000000, 99999999);
        }
        $package = Book::where('id', '=', $id)->firstOrFail();
        $Amount = (int)$package->price;
        $title = $order_number;
        $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
        $token = self::generateLiveToken();
        $BusinessShortCode = 912500;
        $PartyB = $BusinessShortCode;
        $LipaNaMpesaPasskey = "bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919";
        // set values
        $TransactionType = "CustomerPayBillOnline";
        $PartyA = '254' . substr($request->phone_number, -9);
        $PhoneNumber = '254' . substr($request->phone_number, -9);
        $AccountReference = $title;
        $Remark = "Book Purchase";

        $timestamp= Carbon::rawParse('now')->format('YmdHms');
        $password = base64_encode($BusinessShortCode . $LipaNaMpesaPasskey . $timestamp);
        $CallBackURL = "https://moranpublishers.co.ke/mpesa/callback";

        $client = new Client();
        try {
            $res = $client->request('post', $url, [
                'verify' => false,
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Content-Type' => 'application/json',
                ],
                'body' => json_encode(
                    [
                        'BusinessShortCode' => $BusinessShortCode,
                        'Password' => $password,
                        'Timestamp' => $timestamp,
                        'TransactionType' => $TransactionType,
                        'Amount' => $Amount,
                        'PartyA' => $PartyA,
                        'PartyB' => $PartyB,
                        'PhoneNumber' => $PhoneNumber,
                        'CallBackURL' => $CallBackURL,
                        'AccountReference' => $AccountReference,
                        'TransactionDesc' => $Remark,
                        // 'Remark'=> $Remark
                    ]
                )
            ]);

            $obj = json_decode((string)$res->getBody());

            if ($obj->ResponseCode == 0) {
                DB::transaction(function () use ($order_number, $request, $obj, $package, $PhoneNumber) {
                    // MPESA TRANSACTION
                    $mpesa_transaction = BookPayment::create([
                        'user_id' => Auth::user()->id,
                        'book_id' => $package->id,
                        'book_name' => $package->title,
                        'order_number' => $order_number,
                        'phoneNumber' => $PhoneNumber,
                        'MerchantRequestID' => $obj->MerchantRequestID,
                        'CheckoutRequestID' => $obj->CheckoutRequestID,
                    ]);

                    if (!$mpesa_transaction->save()) {
                        DB::rollback();
                        return false;
                    }
                    //was returning order id
                    return $mpesa_transaction->id;
                });

                return response()->json(['message' => 'An MPESA Prompt has been sent to your phone.'], 200);
            }
            else {
                return response()->json(['message' => 'We seem to be having a problem. please try again later'.$obj], 500);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'We seem to be having a problem. please try again later'.$e], 500);
        }
    }

    public function callback(Request $request)
    {
        // Log::info((string) $data2); exit();
        $callbackJSONData = file_get_contents('php://input');
        $callbackData = json_decode($callbackJSONData);
        info($callbackJSONData);

        /*if payment is done successfully*/
        if ($callbackData->Body->stkCallback->ResultCode == 0) {
            $resultCode = $callbackData->Body->stkCallback->ResultCode;
            $resultDesc = $callbackData->Body->stkCallback->ResultDesc;
            $merchantRequestID = $callbackData->Body->stkCallback->MerchantRequestID;
            $checkoutRequestID = $callbackData->Body->stkCallback->CheckoutRequestID;
            $amount = $callbackData->Body->stkCallback->CallbackMetadata->Item[0]->Value;
            $mpesaReceiptNumber = $callbackData->Body->stkCallback->CallbackMetadata->Item[1]->Value;

            $mpesa_transaction = BookPayment::where('CheckoutRequestID', $checkoutRequestID)->first();

            $mpesa_transaction->update([
                'mpesaReceiptNumber' => $mpesaReceiptNumber,
                'completed' => 1,
                'status' => 1,
                'channel' => "M-PESA",
                'transactionDate' => Carbon::now(),
                'ResultCode' => $resultCode,
                'ResultDesc' => $resultDesc,
                'amount' => $amount,
            ]);

            $user = User::find($mpesa_transaction->user_id);

            $user->library()->create([
                'book_id' => $mpesa_transaction->book_id
            ]);

            activity()
                ->inLog('transactions')
                ->causedBy($user)
                ->log($user->first_name.' '.$user->last_name.' purchased the book '.$mpesa_transaction->book_name.' using MPESA.');
            
            return response()->json([
                'status' => 'success',
                'message' => 'Successfully completed payment',
            ]);
        } 
        // else {
        //     $resultCode = $callbackData->Body->stkCallback->ResultCode;
        //     $resultDesc = $callbackData->Body->stkCallback->ResultDesc;
        //     $merchantRequestID = $callbackData->Body->stkCallback->MerchantRequestID;
        //     $checkoutRequestID = $callbackData->Body->stkCallback->CheckoutRequestID;

        //     // Log::info($amount,$mpesaReceiptNumber,$date,$phoneNumber);exit();
        //     $mpesa_transaction = BookPayment::where('CheckoutRequestID', $checkoutRequestID)->first();
        //     $slug = Str::slug($mpesa_transaction->event_name);


        //     $result = [
        //         "ResultDesc" => 4,
        //         "ResultCode" => $resultCode,
        //         "MerchantRequestID" => $merchantRequestID,
        //         "CheckoutRequestID" => $checkoutRequestID,
        //         "order_id" => $mpesa_transaction->id,
        //         "amount" => null,
        //         "mpesaReceiptNumber" => null,
        //         "transactionDate" => null,
        //         "phoneNumber" => null
        //     ];

        //     $mpesa = BookPayment::where('CheckoutRequestID', $checkoutRequestID)->update([
        //         'mpesaReceiptNumber' => null,
        //         'completed' => 0,
        //         'transactionDate' => Carbon::now(),
        //         'ResultCode' => $resultCode,
        //         'ResultDesc' => $resultDesc,
        //     ]);
        //     return response()->json([
        //         'status' => 'error',
        //         'message' => 'Payment not completed',
        //     ]);
        // }
    }

    public function completes(Request $request)
    {
        $order = BookPayment::where('id', (int)$request->order_id)->first();

        if ($order->completed) {
            Session::put('success', 'Successfully accepted payment');
            return response()->json([
                'status' => 'success',
                'message' => 'Successfully completed payment',

            ]);
        } else {
            Session::put('error', 'Payment not completed');
            return response()->json([
                'status' => 'error',
                'message' => 'Payment not completed',

            ]);
        }

    }

    // retry failed stk
    public function retrys(Request $request)
    {

        $ord = BookPayment::find($request->order);
        if ($ord->completed) {
            return response()->json([
                'status' => 'Success',
                'message' => 'This ticket has been paid. Please click on complete button',
                'order_id' => $request->order

            ]);

        }
        $user = User::find($ord->user_id);
        $client = new Client();
        $live = env("application_status");
        if ($live == "live") {
            // $settings = Setting::first();
            /*$consumer_key = decrypt($settings->consumer_key);
            $consumer_secret = decrypt($settings->consumer_secret);
            dd($consumer_key, $consumer_secret);*/
            $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
            $token = self::generateLiveToken();
            // $BusinessShortCode = decrypt($settings->short_code);
            // $PartyB = $BusinessShortCode;
            // $LipaNaMpesaPasskey = decrypt($settings->lipa_mpesa_key);
            $BusinessShortCode = env('business_short_code');
            $PartyB = $BusinessShortCode;
            $LipaNaMpesaPasskey = env('lipa_mpesa_key');
        } elseif ($live == "sandbox") {
            $url = 'https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
            $token = self::generateSandBoxToken();
            $BusinessShortCode = env('business_short_code');
            $PartyB = $BusinessShortCode;
            $LipaNaMpesaPasskey = env('lipa_mpesa_key')/*"bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919"*/
            ;

        } else {
            return json_encode(["Message" => "invalid application status"]);
        }
        //set values
        $TransactionType = "CustomerPayBillOnline";
        $Amount = (int)$ord->total;
        $PartyA = '254' . substr($user->phone, -9);
        $PhoneNumber = '254' . substr($user->phone, -9);
        $AccountReference = "retry";
        $Remark = "pay for the event";;
        $timestamp = '20' . date("ymdhis");
        $password = base64_encode($BusinessShortCode . $LipaNaMpesaPasskey . $timestamp);
        $CallBackURL = route('callback');
        // $CallBackURL = 'https://www.etik.co.ke/index.php?route=extension/payment/mpesaonline/callback';

        // dd($BusinessShortCode, $LipaNaMpesaPasskey);

        // dd($password, $timestamp, $CallBackURL, $Amount, $PhoneNumber); exit();

        // try {
        $res = $client->request('post', $url, [
            'verify' => false,
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json',
            ],
            'body' => json_encode(
                [
                    'BusinessShortCode' => $BusinessShortCode,
                    'Password' => $password,
                    'Timestamp' => $timestamp,
                    'TransactionType' => $TransactionType,
                    'Amount' => $Amount,
                    'PartyA' => $PhoneNumber,
                    'PartyB' => $PartyB,
                    'PhoneNumber' => $PhoneNumber,
                    'CallBackURL' => $CallBackURL,
                    'AccountReference' => $AccountReference,
                    'TransactionDesc' => $Remark,
                    // 'Remark'=> $Remark
                ]
            )
        ]);


        $obj = json_decode((string)$res->getBody());

        if ($obj->ResponseCode == 0) {
            $result = DB::transaction(function () use ($request, $obj) {


                $order1 = OrderEvent::where('id', $request->order)->first();
                $order = OrderEvent::create([
                    'total' => (int)$order1->total,
                    'user_id' => (int)$order1->user_id,
                    'ettype_id' => (int)$order1->ettype_id,
                    'quantity' => (int)$order1->quantity,
                    'MerchantRequestID' => $obj->MerchantRequestID,
                    'CheckoutRequestID' => $obj->CheckoutRequestID,
                ]);

                if (!$order) {
                    DB::rollback();
                    return false;
                }

                return $order->id;
            });


            return response()->json([
                'status' => 'Success',
                'message' => 'Resent the M-Pesa prompt to your phone',
                'order_id' => $result

            ]);
        }
    }

    public function payments_pending($orderId)
    {
        $order = EventPayment::find(decrypt($orderId));
        return view('Users.Accounts.payments-pending', compact('order'));
    }

    public function payments_pendings($orderId)
    {
        $order = EventPayment::find(decrypt($orderId));
        return view('Users.Accounts.payments-pending', compact('order'));
    }

    public function checks_payment_status($orderId)
    {
        $order = EventPayment::find(decrypt($orderId));
        if ($order->completed == true) {
            return $order;
        }
        return false;
    }

    public function checks_payment_complete($orderId)
    {
        $order = EventPayment::find(decrypt($orderId));
        $slug = Str::slug($order->event_name);
        if ($order->completed == true) {
            //return  redirect()->route('event.show',$slug)->with('status', 'Mpesa Payment made successfully!');
            return Redirect::to('/ticket/payment-success')->with('status', 'Mpesa Payment made successfully!');

        } elseif ($order->ResultCode == 1031 || $order->ResultCode == 1032) {
            //return  redirect()->route('event.show',$slug)->with('error', 'Payment Failed, Request Cancelled by User');
            return redirect()->route('event.show', $slug)->with('error', 'Payment Failed, Request Cancelled by User');
            // return Redirect::to('event.show')->with('error', 'Payment Failed, Request Cancelled by User');

        } elseif ($order->ResultCode == 1037) {
            // return Redirect::to('account')->with('error', 'Payment Failed, Connection Timed Out');
            return redirect()->route('event.show', $slug)->with('error', 'Payment Failed, Connection Timed Out');
        } elseif ($order->ResultCode == 1) {
            // return Redirect::to('account')->with('error', 'Payment Failed, Insufficient funds in your MPesa Account');
            return redirect()->route('event.show', $slug)->with('error', 'Payment Failed, Insufficient funds in your MPesa Account');

        } elseif ($order->ResultCode == null) {
            // return  redirect()->route('event.show',$slug)->with('error', 'Payment not received, click finish once a confirmation text has been received.');

            return Redirect::back()->with('error', 'Payment not received, click finish once a confirmation text has been received.');

        } else {
            return redirect()->route('event.show', $slug)->with('error', 'Sorry something went wrong, contact hello@toffeetribe.com for assistance.');
            // return Redirect::to('account')->with('error', 'Sorry something went wrong, contact hello@toffeetribe.com for assistance');
        }
    }


}

