<?php

namespace App\Http\Controllers\API;

use App\Mail\PasswordResetCode;
use App\Mail\PasswordResetSuccess;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Validation\Rules\Password as RulesPassword;
use Symfony\Component\HttpFoundation\Response;

class NewPasswordController extends Controller
{
    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(),
            ['email' => 'required|email']);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], \Illuminate\Http\Response::HTTP_BAD_REQUEST);
        }
        $exists = User::where('email', $request->email)->exists();
        if (!$exists) {
            return response()->json(['message' => 'User does not exist'], Response::HTTP_BAD_REQUEST);
        }
       // $token = $this->token();
        $user = User::where('email', $request->email)->first();
        try {
            PasswordReset::create([
                'email' => $user->email,
                'token' => $token,
                'created_at'=>Carbon::now()
            ]);
            $details = [
                'name' => $user->first_name,
                'token' => $token,
                'to' => $user->email,
            ];
            Mail::send(new PasswordResetCode($details));
        } catch (\Exception $e) {
            info($e->getMessage());
            return response()->json(['message' => 'Error sending token. Try again'], Response::HTTP_BAD_REQUEST);
        }
        return response()->json(['message' => 'Reset Token successfully sent to '.$request->email], Response::HTTP_OK);
    }
    public function submitResetPasswordForm(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required'
        ]);

        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return response()->json(['message' => 'User not found with that email'], Response::HTTP_BAD_REQUEST);
        }
        $user->update(['password' => Hash::make($request->password)]);

        return response()->json(['message' => 'Password has been reset sucessfully'], Response::HTTP_OK);

    }
    public function updatePassword(Request $request)
    {
        $user = Auth::guard('api')->user();
        if ($user){
            $validator = Validator::make(
                $request->all(),
                [
                    'email' => 'required',
                    'password' => 'required',
                    'password_confirm' => 'required|same:password',
                ]);
            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()->first(), compact('user')], \Illuminate\Http\Response::HTTP_BAD_REQUEST);
            }
        }
        else{
            $validator = Validator::make(
                $request->all(),
                [
                    'email' => 'required',
                    'token' => 'required',
                    'password' => 'required',
                    'password_confirm' => 'required|same:password',
                ]);
            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()->first()], Response::HTTP_BAD_REQUEST);
            }
            if (!$this->verifyResetToken($request->email, $request->token)) {
                return response()->json(['message' => 'Invalid token'], Response::HTTP_BAD_REQUEST);
            }
        }

        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return response()->json(['message' => 'User not found with that email'], Response::HTTP_BAD_REQUEST);
        }
        $user->update(['password' => Hash::make($request->password)]);
        if (PasswordReset::where('email', $request->email)->exists()){
            PasswordReset::where('email', $request->email)->delete();
        }
        $details = [
            'name' => $user->first_name,
            'to' => $user->email,
        ];

        return response()->json(['message' => 'Password has been reset sucessfully'], Response::HTTP_OK);
    }

}
