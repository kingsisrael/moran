<?php

namespace App\Http\Controllers\API;

use DateTime;
use DatePeriod;
use DateInterval;

use Carbon\Carbon;
use App\Models\Book;
use App\Models\User;
use App\Jobs\SendSms;
use App\Models\Rating;
use App\Models\Jambopay;
use App\Models\OrderBook;
use App\Models\BookPayment;
use App\Models\Transaction;
use App\Models\UserLibrary;
use App\Models\MainCategory;
use App\Models\UserFavorite;
use Illuminate\Http\Request;
use App\Models\CategoryLevelTwo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\LevelTwoCategoryController;

/**
 * @group User Profile Management
 *
 * APIs for managing user profile and phone numbers
 */
class ProfileController extends Controller
{

    public function __contruct()
    {
        $this->middleware(['auth:sanctum', 'auth.session']);
    }

    /**
     * Edit User's Profile
     *
     * Change User Profile details
     * @bodyParam first_name string required Last Name.
     * @bodyParam last_name string required First Name.
     * @bodyParam email string required Email Address.
     * @authenticated
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editProfile(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email',
            ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], Response::HTTP_BAD_REQUEST);
        }
        $user = Auth::user();
        if($request->hasFile('image')){
            if (! is_dir(public_path('/images'))){
                mkdir(public_path('/images'), 0777);
            }
            $filename = $request->image->getClientOriginalName();
            $request->image->move(public_path('/images'),$filename);;
            $user->image = 'images'.$filename;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->save();
            return response()->json(['message' => "User Profile Picture Successfully Saved"], 200);
        }
        else{
            if ($user){
                $user->update([
                    'first_name'=>$request->first_name,
                    'last_name'=>$request->last_name,
                    'email'=>$request->email,
                ]);

                return response()->json(['message' => 'User Profile Updated Successfully'], Response::HTTP_OK);

            }else{
                return response()->json(['message' => false, 'comment' => 'Invalid user'], Response::HTTP_BAD_REQUEST);
            }
        }

    }

    public function jambopay(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone_number' => 'required|numeric|min:9',
            'book_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()
                ->json(['message' => 'Phone Number and book_id are required'], 400);
        }
        $bk = BookPayment::where('user_id', Auth::id())->where('book_id', $request->book_id)->where('status', 1)->first();
        if ($bk) {
            return response()->json(['message' => 'This Book already bought'], 400);
        }
        $amount = 6;
        return view('welcome', compact('amount'));
    }
    
    public function saveRating(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'rating' => 'required',
                'book_id' => 'required',

            ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], Response::HTTP_BAD_REQUEST);
        }
        $book = DB::table('books')
            ->where('id', $request->book_id)
            ->update([
                'rating' => $request->rating
            ]);
        if ($book){
            return response()->json(['message' => 'Book Rating Updated Successfully'], Response::HTTP_OK);
        }else{
            return response()->json(['message' => false, 'comment' => 'Something went wrong'], Response::HTTP_BAD_REQUEST);
        }
    }

    public function saveCategory(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'user_level_two' => 'required',
            ]);
        $user_level = json_encode($request->user_level_two);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], Response::HTTP_BAD_REQUEST);
        }
        $user = Auth::user();
        if ($user){
            $user->update([
                'user_level_two'=>$user_level,
            ]);
            return response()->json(['message' => 'User Level Two Categories Updated Successfully'], Response::HTTP_OK);
        }else{
            return response()->json(['message' => false, 'comment' => 'Invalid user'], Response::HTTP_BAD_REQUEST);
        }
    }

    public function rating(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'book_id' => 'required',
                'rating' => 'required',
            ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], Response::HTTP_BAD_REQUEST);
        }
        $user = Auth::user();
        if ($user){
            $jp = Rating::create([
                'user_id' => $user->id,
                'book_id' => $request->book_id,
                'rating' => $request->rating,
            ]);
            return response()->json(['message' => 'Ratings Updated Successfully'], Response::HTTP_OK);
        }else{
            return response()->json(['message' => false, 'comment' => 'Invalid user'], Response::HTTP_BAD_REQUEST);
        }
    }

    public function levelTwo()
    {
        $category = CategoryLevelTwo::query()
            ->orderby("id","asc")
            ->distinct()
            ->select('id','name')->get();
        if ($category){
            return response()->json(['category' => $category]);
        }
        else{
            return response()->json(['message' => 'No Category Found'], Response::HTTP_OK);

        }

    }

    public function mainCategoryBooks(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'category_id' => 'required',
            ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], Response::HTTP_BAD_REQUEST);
        }
        $books = Book::orderBy('created_at', 'DESC')->where('category_id', $request->category_id)->get();

        if ($books){
            return response()->json(['book' => $books]);
        }
        else{
            return response()->json(['error' => 'No Books Found'], Response::HTTP_OK);

        }


    }

    /**
     * Delete Account
     *
     * Deactivates user account
     * @authenticated
     */
    public function deleteAccount()
    {
        $profile = Auth::user();
        if ($profile) {
            //delete user access_tokens
            $profile->tokens->each(function ($token){
                $token->delete();
            });
            // Delete User Library
            UserLibrary::where('user_id', $profile->id)->delete();
            UserFavorite::where('user_id', $profile->id)->delete();
            BookPayment::where('user_id', $profile->id)->delete();
            Jambopay::where('user_id', $profile->id)->delete();
            OrderBook::where('user_id', $profile->id)->delete();
            $profile->delete();

            activity()
                ->inLog('general')
                ->causedBy($profile)
                ->log($profile->first_name.' '.$profile->last_name.' deleted their profile');
            
            return response()->json(['message' => true, 'comment' => 'Account has been deleted'], Response::HTTP_OK);
        } else {
            return response()->json(['message' => false, 'comment' => 'Invalid user'], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function passcode($min = 1000, $max = 9999)
    {
        return mt_rand($min, $max);
    }

    public function get_books()
    {
        $books = Book::all();
        return response()->json([
            "message" => [
                "books" => $books,
            ]
        ],200);
    }

    public function levelTwoBooks()
    {
        $att = DB::table('users')
            ->where('id', '=', Auth::id())
            ->first();
        $userss =$att->user_level_two;
        $userz = (trim($userss, '"[]'));
        $myArray = explode(',', $userz);
        if ($userss){
            $query = CategoryLevelTwo::select("*")
                ->whereIn('id', $myArray)
                ->get();
            return response()->json([
                "message" => [
                    "level_two_names" => $query,
                ]
            ],200);
        }
        else{
            $books = Book::where('category_id', '!=', 1)->get();
            return response()->json([
                "message" => [
                    "level_two_names" => "no data",
                ]
            ],200);
        }
    }

    public function get_level_two_books()
    {
        $att = DB::table('users')
            ->where('id', '=', Auth::id())
            ->first();
        $userss =$att->user_level_two;
        $userz = (trim($userss, '"[]'));
        $myArray = explode(',', $userz);
        if ($userss){
            $query = Book::where('url', '!=', null)
                ->orWhere('url', '!=', '')
                ->whereIn('level_two_category_id', $myArray)
                ->inRandomOrder()
                ->get();
            $filtered_books = [];

            foreach ($query as $key => $book) {
                if (file_exists(public_path('images/'.$book->image))) {
                    array_push($filtered_books, $book);
                }
            }

            $user_library = UserFavorite::select('book_id')->where('user_id', auth()->user()->id)->get();
            foreach ($filtered_books as $book) {
                if($user_library->contains($book->id)) {
                    $book['inFavorites'] = true;
                } else {
                    $book['inFavorites'] = false;
                }
            }

            return response()->json([
                "message" => [
                    "count" => count($filtered_books),
                    "books" => $filtered_books,
                ]
            ],200);
        }
        else{
            $books = Book::where('category_id', '!=', 1)->where('url', '!=', null)->orWhere('url', '!=', '')->inRandomOrder()->get();
            $filtered__all_books = [];
            foreach ($books as $key => $book) {
                if (file_exists(public_path('images/'.$book->image))) {
                    array_push($filtered_all_books, $book);
                }
            }
            return response()->json([
                "message" => [
                    "books" => $filtered__all_books,
                ]
            ],200);
        }
    }

    public function get_user_books()
    {
        $userLibrary = UserLibrary::where('user_id', auth()->user()->id)->get();
        if (! $userLibrary->count()) {
            return response()->json([
                "message" => [
                    "books" => "No Books found"
                ]
            ], 200);
        }
        $user_books = [];
        foreach ($userLibrary as $book) {
            array_push($user_books, Book::find($book->book_id));
        }
        $user_library = UserFavorite::select('book_id')->where('user_id', auth()->user()->id)->get();
        foreach ($user_books as $book) {
            if($user_library->contains($book->id)) {
                $book['inFavorites'] = true;
            } else {
                $book['inFavorites'] = false;
            }
        }

        return response()->json([
            "message" => [
                "books" => $user_books,
                "count" => collect($user_books)->count()
            ]
        ], 200);
    }

    public function books()
    {
        $books = Book::where('published', true)->get();
        
        return response()->json(
            [
                'book_details' => $books,
            ],
            Response::HTTP_OK
        );
    }

    public function category()
    {
        $category= MainCategory::all();
        return response()->json([
            "message" => [
                "category" => $category,
            ]
        ],200);
    }

    public function book($id)
    {
        $user_library = UserLibrary::select('book_id')->where('user_id', auth()->user()->id)->get();
        $book = Book::find($id);
        if ($user_library->contains($book->id)) {
            $book['inFavorites'] = true;
        } else {
            $book['inFavorites'] = false;
        }
        return $book;
    }

    public function getUserFavoriteBooks()
    {
        $favorites = UserFavorite::where('user_id', auth()->user()->id)->get();
        $books= [];
        if ($favorites->count()) {
            foreach($favorites as $favorite) {
                array_push($books, Book::find($favorite->book_id));
            }
        }

        return response()->json($books, 200);
    }

    public function addBookToFavorites(Request $request)
    {
        $this->validate($request, [
            'book_id' => ['required']
        ]);

        auth()->user()->favorites()->create([
            'book_id' => $request->book_id
        ]);

        return response()->json(['message' => 'Book added to favorites'], 200);
    }

    public function deleteBookFromFavorites(Request $request)
    {
        $this->validate($request, [
            'book_id' => ['required']
        ]);

        $favorite = UserFavorite::where('user_id', auth()->user()->id)->where('book_id', $request->book_id)->first();

        if ($favorite) {
            $favorite->delete();

            return response()->json(['message' => 'Book Removed from favorites'], 200);
        }

        return response()->json(['message' => 'Book not found in favorites'], 404);
    }

    public function saveToken(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'token' => 'required'
            ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], Response::HTTP_BAD_REQUEST);
        }

        $user = User::find(auth()->user()->id);
        $user->token = $request->token;
        $user->save();

        return response()->json(['message' => $user], 200);
    }
}
