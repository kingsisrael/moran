<?php

namespace App\Http\Controllers\API;
use Exception;
use Carbon\Carbon;
use App\Models\User;
use App\Jobs\SendSms;
use GuzzleHttp\Client;
use App\Jobs\SMSResetOtp;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Jobs\UserRegisterNotification;
use Illuminate\Auth\Events\Registered;
use App\Http\Requests\Auth\LoginRequest;
use Laravel\Socialite\Facades\Socialite;
use App\Jobs\SendSuccessRegistrationMail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;
use Symfony\Component\HttpFoundation\Response;
/**
 * @group Login
 *
 * APIs for user authentication
 */
class AuthController extends Controller
{
    /**
     * Register User
     *
     * Registers user to the system.
     * A bearer token is provided after successful registration which you can store and use for authentication while
     * @bodyParam first_name string required First Name.
     * @bodyParam last_name string required Last Name.
     * @bodyParam email string required Email Address.
     * @bodyParam phone string required Phone Number, prefixed.
     * @bodyParam password string required Password min 8 characters.
     * @bodyParam password_confirm string required Password, must match password.
     */
    public function registers(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'phone_number' => ['required'],
            'password' => 'required|min:8',
            'password_confirm' => 'required|same:password',
        ]);
        if ($validator->fails())
        {
            return response(['errors' => $validator->errors()->all()], 422);
        }
        $request['password'] = Hash::make($request['password']);
        $request['remember_token'] = Str::random(10);
        if (strlen($request->phone_number) == 9) {
            $phone_number = '254' . $request->phone_number;
        } else {
            $phone_number = '254' . substr($request->phone_number, -9);
        }

        $request['phone_number'] = $phone_number;
        $passcode = $this->passcode();
        $request['passcode'] = $passcode;
        $user = User::create($request->toArray());
        $user->assignRole('user');

        $token = $user->createToken('authtoken');

        if ($user != null){
            $user_details = [
                'id'=>$user->id,
                'first_name'=>$user->first_name,
                'last_name'=>$user->last_name,
                'email'=>$user->email,
                'phone_number'=>$user->phone_number,
            ];
            UserRegisterNotification::dispatchAfterResponse($user->phone_number, $user->passcode, $user->first_name);
            // event(new Registered($user));
            SendSuccessRegistrationMail::dispatchAfterResponse($user);

            activity()
                    ->inLog('general')
                    ->causedBy($user)
                    ->log($user->first_name.' '.$user->last_name.' registered in to the application.');
        }

        return response()->json(
            [
                'user_details' => $user_details,
                'token' => $token,
            ],
            Response::HTTP_OK
        );

    }

    public function verify(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'otp' => 'required',
            ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], Response::HTTP_BAD_REQUEST);
        }
        $user =  User::where('passcode', $request->otp)->first();
        if ($user){
            User::where('passcode', $request->otp)
                ->update([
                    'phone_verified_at' => Carbon::now(),
                    'status' => true
                ]);
                activity()
                    ->inLog('general')
                    ->causedBy($user)
                    ->log($user->first_name.' '.$user->last_name.' verified their phone number.');
            
            return response()->json(['message' => 'User Account Verified Successfully', 'email' => $user->email], Response::HTTP_OK);

        }else{
            return response()->json(['message' => false, 'comment' => 'Invalid user OTP'], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Social Login
     */
    public function oauth_login(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
        ]);
        if ($validator->fails())
        {
            return response(['errors' => $validator->errors()->all()], 422);
        }
        try{
            $user = User::where('email', $request->email)->first();
            if($user != null){
                $token = $user->createToken('auth');
                return response()->json([
                    'user_details' => $user,
                    'token' => $token->plainTextToken,
                ]);
            } else {
                $usersName = explode(' ', $request->name);
                $user = User::create([
                    'first_name' => $usersName[0],
                    'last_name' => $usersName[1] ? $usersName[1] : '',
                    'email' => $request->email,
                    'phone_number' => 0,
                    // 'phone_number' => (string)mt_rand(10000000, 99999999),
                    'password' => Hash::make("secretpassword"),
                    'passcode' => $this->passcode(),
                ]);
                $user->assignRole('user');
                if ($user){
                    $users = User::where('id',$user->id)->first();
                    $token = $user->createToken('auth');

                    activity()
                        ->inLog('general')
                        ->causedBy($users)
                        ->log($users->first_name.' '.$users->last_name.' logged in');

                    return response()->json([
                        'user_details' => $users,
                        'token' => $token->plainTextToken,
                    ]);
                }
            }
        }catch(\Exception $exception){
            return response()->json(['message' => $exception], Response::HTTP_BAD_REQUEST);
        }
    }

    public function verifyPasscode(Request $request)
    {

        if ($request->passcode == Auth::user()->passcode) {
            Auth::user()->passcode()->update(['phone_verified_at' => Carbon::now(), 'is_active'=>true]);
            $details = [
                'name' => Auth::user()->first_name. ' '.Auth::user()->last_name,
                'to' => Auth::user()->email,
            ];
           // Mail::send(new Registered($details));

            return response()->json(['message' => 'Passcode verified. Please login'], Response::HTTP_OK);
        }
        else {
            return response()->json(['message' => 'Invalid passcode'], Response::HTTP_BAD_REQUEST);
        }
    }
    public function editProfile(Request $request)
    {
        $id = auth()->user()->id;
        try {
            $user = User::findOrFail($id);
            if($request->has('first_name'))$user->first_name = $request->get('first_name');
            if($request->has('last_name'))$user->last_name = $request->get('last_name');

            if($request->has('email')){
                $user->email = $request->get('email');
            }
            if($request->has('phone_number')){
                $user->phone_number = $request->get('phone_number');
            }
            if($request->has('dob')){
                $user->dob = $request->get('dob');
            }
            if($request->has('country')){
                $user->country = $request->get('country');
            }
            $user->save();

            activity()
                ->inLog('general')
                ->causedBy($user)
                ->log($user->first_name.' '.$user->last_name.' updated their profile.');

            return response()->json(['message' => "User Profile Update Successfully"], 200);

        }
        catch (\Exception $exception) {
            return response()->json(['message' => $exception], Response::HTTP_BAD_REQUEST);
        }
    }
    public function upload(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'image' =>'required|max:5000|mimes:png,jpg,jpeg',
            ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()],400);
        }

        if($request->hasFile('image')){
            if (! is_dir(public_path('/images/profile'))){
                mkdir(public_path('/images/profile'), 0777);
            }

            $filename = $request->image->getClientOriginalName();
            $request->image->move(public_path('images/profile'), $filename);
            $user = auth()->user();
            $user->image = 'images/profile/'.$filename;
            $user->save();

            activity()
                ->inLog('general')
                ->causedBy($user)
                ->log($user->first_name.' '.$user->last_name.' updated their profile.');
            
            return response()->json(['message' => "User Profile Picture Successfully Saved"], 200);
        }

        return response()->json(['message' => 'No image Attached'],400);

    }

    /**
     * Login
     *
     * Log a user into the system. After successful login, a bearer token is returned which you may store and use for
     * authentication for guarded routes. Note that this token has an expiry duration therefore you should implement
     * a mechanism to check whether the token has expired before requiring the user to login again.
     * @bodyParam email string required Email address.
     * @bodyParam password string required Password.
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */

    public function logins(LoginRequest $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        
        $request->authenticate();
        $token = $request->user()->createToken('authtoken');
        
        Auth::logoutOtherDevices($request->password);

        return response()->json(
            [
                'user_details' => $request->user(),
                'token' => $token->plainTextToken,
            ],
            Response::HTTP_OK
        );
    }

    /**
     * Logout
     *
     * Log a user out of the system.
     * @authenticated
     * @return \Illuminate\Http\JsonResponse
     */
    public function logouts(Request $request)
    {

        $request->user()->tokens()->delete();

        return response()->json(
            [
                'message' => 'Logged out'
            ]
        );

    }

    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        $user = User::where('email', $request->email)->first();
            if($user){
                $otp =$this->passcode();
                User::where('email', $request->email)
                    ->update(['passcode' => $otp]);
                SMSResetOtp::dispatch($user->phone_number,$otp,$user->first_name);
                return response()->json(['message' => 'OTP Sent Successfully', 'email' => $user->email], Response::HTTP_OK);

            }
            else{
                return response()->json(['errors' => 'User does not exist....'],Response::HTTP_BAD_REQUEST);
            }
    }

    public function sendSMS(Request $request)
    {
        $response = SMSResetOtp::dispatch($request->phone_number, 'Hello', 'Kings');
        return response()->json($response, 200);
    }
    
    public function submitResetPasswordForm(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp' => 'required',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        $user = User::where('passcode', $request->otp)->first();
        if ($user) {
            return response()->json(['message' => $user], Response::HTTP_OK);
        }
        else {
            return response()->json(['message' => 'Invalid OTP'], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function passcode($min = 100000, $max = 999999)
    {
        return mt_rand($min, $max);
    }
    public function ResetPassword(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required'
        ]);

        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return response()->json(['message' => 'User not found with that email'], Response::HTTP_BAD_REQUEST);
        }
        $user->update(['password' => Hash::make($request->password)]);

        return response()->json(['message' => 'Password has been reset sucessfully', 'email' => $user->email], Response::HTTP_OK);
    }

    /**
     * Get Laravel Passport Token
     * @param $username
     * @param $password
     * @return mixed
     */
    protected function getPassportToken($username, $password)
    {
        $http = new Client;
        $form_params = [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => config('app.passport_client_id'),
                'client_secret' => config('app.passport_client_secret'),
                'username' => $username,
                'password' => $password,
                'scope' => '*',
            ]
        ];
        $response = $http->post(
            config('app.token_url'),
            $form_params
        );
        return json_decode((string)$response->getBody(), true);
    }

    /**
     * Email Unique Check
     *
     * Check whether the provided email in user registration form is unique.
     * @bodyParam email string required Email address.
     */
    public function emailUnique(Request $request)
    {
        $unique = !User::where('email', $request->email)->exists();

        return response()->json(['message' => $unique], Response::HTTP_OK);
    }

    /**
     * Phone Number Unique Check
     *
     * Check whether the provided phone number in user registration form is unique
     * @bodyParam phone_number string required Phone number.
     */

    public function phoneUnique(Request $request)
    {
        $unique = !User::where('email',$request->phone_number)->exists();

        return response()->json(['message' => $unique], Response::HTTP_OK);

    }


    public function logout()
    {
        if (Auth::check()){
            $user = Auth::user()->tokens->each(function ($token, $key){
                $token->delete();
            });
            return response()->json(['message' => 'User Logged Out Successfully'], Response::HTTP_OK);
        }
        return response()->json(['message' => false, 'comment' => 'Invalid user'], Response::HTTP_BAD_REQUEST);
    }

}
