<?php

namespace App\Http\Controllers;

use App\Pesapal\pesapalCheckStatus\pesapalCheckStatus;
use Illuminate\Http\Request;

class CallbackController extends Controller
{
    public function redirect(Request $request)
    {
        $pesapalMerchantReference	= null;
        $pesapalTrackingId 	 = null;
        $checkStatus = new pesapalCheckStatus();

        if(isset($_GET['pesapal_merchant_reference']))
            $pesapalMerchantReference = $_GET['pesapal_merchant_reference'];

        if(isset($_GET['pesapal_transaction_tracking_id']))
            $pesapalTrackingId = $_GET['pesapal_transaction_tracking_id'];

        //obtaining the payment status after a payment
        $status = $checkStatus->checkStatusUsingTrackingIdandMerchantRef($pesapalMerchantReference,$pesapalTrackingId);

        //display the reference and payment status on the callback page
        return view ('laravel_pesapal::callback_example', compact('pesapalMerchantReference', 'status'));
    }

}
