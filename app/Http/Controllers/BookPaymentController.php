<?php

namespace App\Http\Controllers;

use App\EventPayment;
use App\Models\BookPayment;
use App\Models\OrderBook;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;

class BookPaymentController extends Controller
{
    public function callbacks(Request $request)
    {
//         Log::info((string) $data2); exit();
        $callbackJSONData = file_get_contents('php://input');
        $callbackData = json_decode($callbackJSONData);
        info((string)$callbackData->Body->stkCallback->ResultCode);

        /*if payment is done successfully*/
        if ($callbackData->Body->stkCallback->ResultCode == 0) {
            $resultCode = $callbackData->Body->stkCallback->ResultCode;
            $resultDesc = $callbackData->Body->stkCallback->ResultDesc;
            $merchantRequestID = $callbackData->Body->stkCallback->MerchantRequestID;
            $checkoutRequestID = $callbackData->Body->stkCallback->CheckoutRequestID;
            $amount = $callbackData->Body->stkCallback->CallbackMetadata->Item[0]->Value;
            $mpesaReceiptNumber = $callbackData->Body->stkCallback->CallbackMetadata->Item[1]->Value;
            $date = $callbackData->Body->stkCallback->CallbackMetadata->Item[3]->Value;
            $phoneNumber = $callbackData->Body->stkCallback->CallbackMetadata->Item[4]->Value;


            // Log::info($amount,$mpesaReceiptNumber,$date,$phoneNumber);
            $int = date_create($date);
            $transactionDate = Carbon::parse($date)->toDateTimeString();
            $mpesa_transaction = BookPayment::where('CheckoutRequestID', $checkoutRequestID)->first();

            $result = [
                "ResultDesc" => $resultDesc,
                "ResultCode" => $resultCode,
                "MerchantRequestID" => $merchantRequestID,
                "CheckoutRequestID" => $checkoutRequestID,
                "amount" => $amount,
                "mpesaReceiptNumber" => $mpesaReceiptNumber,
                "order_id" => $mpesa_transaction->id,
                "transactionDate" => $transactionDate,
                "phoneNumber" => $phoneNumber
            ];

            $mpesa = BookPayment::where('CheckoutRequestID', $checkoutRequestID)->update([
                'mpesaReceiptNumber' => $mpesaReceiptNumber,
                'completed' => true,
                'transactionDate' => Carbon::now(),
                'ResultCode'=>$resultCode,
                'ResultDesc' => $resultDesc,
                'amount' => $amount,
            ]);

            $mpesa_tran = BookPayment::where('CheckoutRequestID', $checkoutRequestID)->first();
            $package_id = $mpesa_tran->package_id;

            $order = new OrderBook([
                'user_id' => $mpesa_tran->user_id,
                'book_id' => $package_id,
                'order_number' => $mpesa_tran->order_number,
                'amount' => $amount,
            ]);
            $order->save();

            //dispatch(new EventOrderEmail($order));

            return Redirect::to('/account/payment-success')->with('status', 'Mpesa payment made successfully!');
            //not

        }else
        {
            $resultCode = $callbackData->Body->stkCallback->ResultCode;
            $resultDesc = $callbackData->Body->stkCallback->ResultDesc;
            $merchantRequestID = $callbackData->Body->stkCallback->MerchantRequestID;
            $checkoutRequestID = $callbackData->Body->stkCallback->CheckoutRequestID;

            //Log::info($amount,$mpesaReceiptNumber,$date,$phoneNumber);exit();
            $mpesa_transaction = EventPayment::where('CheckoutRequestID', $checkoutRequestID)->first();
            $slug = Str::slug($mpesa_transaction->event_name);



            $result = [
                "ResultDesc" => $resultDesc,
                "ResultCode" => $resultCode,
                "MerchantRequestID" => $merchantRequestID,
                "CheckoutRequestID" => $checkoutRequestID,
                "order_id" => $mpesa_transaction->id,
                "amount" => null,
                "mpesaReceiptNumber" => null,
                "transactionDate" => null,
                "phoneNumber" => null
            ];

            $mpesa = EventPayment::where('CheckoutRequestID', $checkoutRequestID)->update([
                'mpesaReceiptNumber' => null,
                'completed' => false,
                'transactionDate' => Carbon::now(),
                'ResultCode'=>$resultCode,
                'ResultDesc' => $resultDesc,
            ]);

            //return Redirect::to('account')->with('error', 'Callback Failed, Payment was Unsuccessful');
            return  redirect()->route('event.show',$slug)->with('error', 'Callback Failed, Payment was Unsuccessful');
        }
    }

}
