<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'update-status/{}',
        'admin/dashboard',
               'callback',
             '/mpesa/callback',
               '/api/*',
        '/redirect',
        'http://172.104.245.14/moran/redirect',
        'http://172.104.245.14/moran/listenipn',
        '/listenipn',
        '/checkout'
    ];
}
