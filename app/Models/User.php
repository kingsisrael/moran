<?php

namespace App\Models;

use App\Notifications\ResetPasswordNotification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
//use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable, HasRoles, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $guard_name = 'api';
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone_number',
        'status',
        'email_verified_at',
        'phone_verified_at',
        'password',
        'passcode',
        'created_at',
        'updated_at',
        'user_level_two',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'email_verified_at',
        'phone_verified_at',
        'passcode'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_verified_at' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function sendPasswordResetNotification($token)
    {

        $url = 'https://spa.test/reset-password?token=' . $token;

        $this->notify(new ResetPasswordNotification($url));
    }
    public function books()
    {
        return $this->hasMany(Book::class);
    }
    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }
    public function jambopay()
    {
        return $this->hasMany(Jambopay::class);
    }
    /**
     * Get all of the userNotifications for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userNotifications()
    {
        return $this->hasMany(UserNotification::class);
    }
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get all of the library for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function library()
    {
        return $this->hasMany(UserLibrary::class);
    }
    /**
     * Get all of the favorites for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function favorites()
    {
        return $this->hasMany(UserFavorite::class);
    }

    public function bookInLibrary($id)
    {
        $book = UserLibrary::where('user_id', $this->id)->where('book_id', $id)->first();

        if (!$book) {
            return false;
        }

        return true;
    }
}
