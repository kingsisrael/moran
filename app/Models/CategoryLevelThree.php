<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryLevelThree extends Model
{
    use HasFactory;
    use \Conner\Tagging\Taggable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name',
        'level_two_category_id',
    ];

    public function threes()
    {
        return $this->belongsTo(CategoryLevelTwo::class,'level_two_category_id');
    }
    public function level_four_category()
    {
        return $this->hasMany(CategoryLevelFour::class);
    }
}
