<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;
    protected $table = 'transactions';

    protected $fillable = [
        'user_id', 'amount', 'currency', 'description', 'reference', 'phone', 'status', 'tracking_id', 'payment_method'
    ];


    protected $casts = [
//        'created_at' => 'datetime:j F Y g:i A',
        'created_at' => 'datetime:Y-m-d h:i:s',
    ];


    public function user()
    {
        return $this->belongsTo(User::class, 'id');
    }


}
