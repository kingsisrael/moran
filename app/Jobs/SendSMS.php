<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendSMS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

   public $phone_number;
    public $password;
    public $first_name;

    /**
     * Create a new job instance.
     *
     * @param $phone_number
     * @param $password
     * @param $first_name
     */
    public function __construct($phone_number,$password,$first_name)
    {
        $this->phone_number = $phone_number;
        $this->password = $password;
        $this->first_name = $first_name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $msg ="Dear $this->first_name Your Moran Publishers Login Password is: $this->password";
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://quicksms.advantasms.com/api/services/sendsms/?apikey=2a84855fb7b904aa6f2770fd864d84f8&partnerID=1378&message='.urlencode($msg).'&shortcode=Moran_info&mobile='.$this->phone_number,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Cookie: PHPSESSID=cqjfp2pdpakct1c21vlllhevuq'
            ),
        ));
        $response = curl_exec($curl);
        if(curl_error($curl)){
            echo 'Request Error:' . curl_error($curl);
        }
        else
        {
            echo $response;
        }
        curl_close($curl);
    }
}
