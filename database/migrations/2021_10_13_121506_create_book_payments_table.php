<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_payments', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned()->index();
            $table->string('order_number');
            $table->string('book_name')->nullable();
            $table->bigInteger('book_id')->unsigned()->index();
            $table->string('phoneNumber')->nullable();
            $table->string('ResultCode')->nullable();
            $table->decimal('amount', 9,2)->nullable();
            $table->string('mpesaReceiptNumber')->nullable();
            $table->string('ResultDesc')->nullable();
            $table->string('MerchantRequestID')->nullable();
            $table->string('CheckoutRequestID')->nullable();
            $table->dateTime('transactionDate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_payments');
    }
}
