<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPointsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('points')->nullable();
            $table->string('notifications')->nullable();
            $table->string('category')->nullable();
            $table->string('sub_category')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('points')->nullable();
            $table->string('notifications')->nullable();
            $table->string('category')->nullable();
            $table->string('sub_category')->nullable();
        });
    }
}
