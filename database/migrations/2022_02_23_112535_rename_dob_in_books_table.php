<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameDobInBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('country')->default('Kenya')->change();
            $table->date('dob')->default('2004-01-01')->change();

        });
        Schema::table('books', function (Blueprint $table) {
            $table->integer('rating')->default(2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('country')->default('Kenya')->change();
            $table->date('dob')->default('2004-01-01')->change();
        });
        Schema::table('books', function (Blueprint $table) {
            $table->integer('rating')->default(2)->change();
        });
    }
}
