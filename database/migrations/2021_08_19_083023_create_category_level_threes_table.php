<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryLevelThreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_level_threes', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('level_two_category_id')->nullable()->references('id')->on('category_level_twos')->onDelete('set null');
            $table->boolean('sub_category')->default(false);
            $table->text('tags')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_level_threes');
    }
}
