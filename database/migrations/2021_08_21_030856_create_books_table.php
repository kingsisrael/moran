<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('isbn');
            $table->string('title');
            $table->text('description')->nullable();

            $table->string('file_type')->nullable();
            $table->string('edition')->nullable();
            $table->text('publisher')->nullable();

            $table->text('copyright')->nullable();
            $table->string('language');
            $table->string('price')->nullable();;
            $table->foreignId('author_id')->nullable()->references('id')
                ->on('authors')->onDelete('set null');

            $table->json('authors')->nullable();;

            $table->boolean('published')->default(true);
            $table->date('publication_date')->nullable();

            $table->date('embargo_date')->nullable();
            $table->foreignId('category_id')->nullable()->references('id')
                ->on('main_categories')->onDelete('set null');

            $table->foreignId('level_two_category_id')->nullable()->references('id')
                ->on('category_level_twos')->onDelete('set null');
            $table->foreignId('level_three_category_id')->nullable()->references('id')
                ->on('category_level_threes')->onDelete('set null');

            $table->foreignId('level_four_category_id')->nullable()->references('id')
                ->on('category_level_fours')->onDelete('set null');

            $table->json('tag')->nullable();
            $table->text('tags')->nullable();;
            $table->string('image')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
