<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPhoneToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('points')->default('10')->change();
            $table->string('notifications')->default('0')->change();
            $table->string('category')->default('3')->change();
            $table->string('sub_category')->default('1')->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('points')->default('10')->change();
            $table->string('notifications')->default('0')->change();
            $table->string('category')->default('3')->change();
            $table->string('sub_category')->default('1')->change();
        });
    }
}
