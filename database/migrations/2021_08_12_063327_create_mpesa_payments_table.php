<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMpesaPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mpesa_payments', function (Blueprint $table) {
            $table->id();
            $table->string('user');
            $table->string('order_number');
            $table->string('category')->nullable();
            $table->string('book_name')->unsigned()->index();
            $table->string('phoneNumber')->nullable();

            $table->string('ResultCode')->nullable();
            $table->decimal('amount', 9,2)->nullable();
            $table->string('mpesaReceiptNumber')->nullable();
            $table->string('ResultDesc')->nullable();
            $table->boolean('status')->default(1);
            $table->string('MerchantRequestID')->nullable();
            $table->string('CheckoutRequestID')->nullable();
            $table->dateTime('transactionDate')->nullable();
            $table->string('channel')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mpesa_payments');
    }
}
