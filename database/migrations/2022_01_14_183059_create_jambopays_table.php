<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJambopaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jambopays', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned()->index();
            $table->string('Order_Id');
            $table->string('book_name')->nullable();
            $table->bigInteger('book_id')->unsigned()->index();
            $table->string('phoneNumber')->nullable();
            $table->string('Status')->nullable();
            $table->string('Receipt')->nullable();
            $table->decimal('Amount', 9,2)->nullable();
            $table->string('Currency')->nullable();
            $table->string('Secure_Hash')->nullable();
            $table->string('Timestamp')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jambopays');
    }
}
