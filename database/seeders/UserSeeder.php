<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'first_name'=>'Moran',
            'last_name'=>'Publishers',
            'email'=>'user@deveint.com',
            'phone_number'=>'25423473035',
            'password'=>Hash::make('secretpassword'),
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        $user->assignRole('app_user');

        $user2 = User::create([
            'first_name'=>'Moran',
            'last_name'=>'Publishers 2',
            'email'=>'user2@deveint.com',
            'phone_number'=>'25423473605',
            'password'=>Hash::make('secretpassword'),
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        $user2->assignRole('app_user');

        $user3 = User::create([
            'first_name'=>'Moran',
            'last_name'=>'Publishers 3',
            'email'=>'user3@deveint.com',
            'phone_number'=>'25423473705',
            'password'=>Hash::make('secretpassword'),
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        $user3->assignRole('app_user');

    }
}
