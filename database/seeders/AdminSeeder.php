<?php

namespace Database\Seeders;

use App\Models\Admin;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dev = Admin::create(
            [
                'first_name'=>'Deveint  ',
                'last_name'=>'Admin',
                'email'=>'admin@deveint.com',
                'phone_number'=>'254725730055',
                'password'=>Hash::make('secretpassword'),
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),
                'status'=>true
            ]);
        $dev->assignRole('admin');

            $itc2 = Admin::create(
                [
                    'first_name'=>'Solomon   ',
                    'last_name'=>'Mbaire',
                    'email'=>'swathuo@moranpublishers.co.ke',
                    'phone_number'=>'254733333009',
                    'password'=>Hash::make('password123'),
                    'created_at'=>Carbon::now(),
                    'updated_at'=>Carbon::now(),
                    'status'=>true
                ]);
            $itc2->assignRole('ict');

            $fin1 = Admin::create(
                [
                    'first_name'=>'Stephen ',
                    'last_name'=>'Muholo',
                    'email'=>'smuholo@moranpublishers.co.ke',
                    'phone_number'=>'254733636505',
                    'password'=>Hash::make('secretpassword'),
                    'created_at'=>Carbon::now(),
                    'updated_at'=>Carbon::now(),
                    'status'=>true
                ]);
            $fin1->assignRole('finance');

        }

}
