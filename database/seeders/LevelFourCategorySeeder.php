<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LevelFourCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_level_fours')->insert([
            ['name' => 'Beginner',
                'level_three_category_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
            ['name' => 'Transition',
                'level_three_category_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
            //8
            ['name' => 'Pearl readers',
                'level_three_category_id' => 8,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
            ['name' => 'Platinum readers',
                'level_three_category_id' => 8,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
            ['name' => 'Mwanzo',
                'level_three_category_id' => 10,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
            ['name' => 'Mpito',
                'level_three_category_id' => 10,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
            ['name' => 'Biology',
                'level_three_category_id' => 39,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
            ['name' => 'Chemistry',
                'level_three_category_id' => 39,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
            ['name' => 'Physics',
                'level_three_category_id' => 39,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],

            ['name' => 'English',
                'level_three_category_id' => 40,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
            ['name' => 'Kiswahili',
                'level_three_category_id' => 40,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
            ['name' => 'Geography',
                'level_three_category_id' => 41,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
            ['name' => 'History and Government',
                'level_three_category_id' => 41,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
            ['name' => 'Agriculture',
                'level_three_category_id' => 42,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
            ['name' => 'Business Studies',
                'level_three_category_id' => 42,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],

            ['name' => 'Biology',
                'level_three_category_id' => 44,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
            ['name' => 'Chemistry',
                'level_three_category_id' => 44,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
            ['name' => 'Physics',
                'level_three_category_id' => 44,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
            ['name' => 'English',
                'level_three_category_id' => 45,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
            ['name' => 'Kiswahili',
                'level_three_category_id' => 45,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
            ['name' => 'Geography',
                'level_three_category_id' => 46,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
            ['name' => 'History and Government',
                'level_three_category_id' => 46,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],

            ['name' => 'Agriculture',
                'level_three_category_id' => 47,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
            ['name' => 'Business Studies',
                'level_three_category_id' => 47,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),],
        ]);

    }
}
