<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            [
                'isbn' => '9789966346490',
                'file_type' => 'EPUB',
                'title' => 'A Note for Alice',
                'description' => 'Indeche finds himself in an awkward situation. Davis, a teenage boy gives him gifts and a letter to take to his sister, Alice. Out of curiosity, Indeche reads the note that Davis has written to Alice.',
                'embargo_date' => Carbon::now(),
                'category_id' => '1',
//                'level_two_category_id' => '1',
//                'level_three_category_id' => '1',
//                'level_four_category_id' => '1',
//                'tags' => 'Grade 4',
//                'author_id' => '1',
                'edition' => '1',
                'publisher' => 'Moran (E. A.) Publishers Limited',

                'publication_date' => Carbon::now(),
                'copyright' => 'Moran (E. A.) Publishers Limited',
                'language' => 'English',
                'price' => '320',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);

    }
}
