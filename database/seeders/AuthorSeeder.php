<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('authors')->insert([
            [
                'first_name'=>'Mureithi',
                'last_name'=>'Maina;',
                'email'=>'maina@deveint.com',
                'phone_number'=>'25433473005',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),
            ],
            [
                'first_name'=>'Sarah ',
                'last_name'=>'Ambiy',
                'email'=>'sarah@deveint.com',
                'phone_number'=>'25463473005',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),
            ],
            [
                'first_name'=>'Ibrahim',
                'last_name'=>'Omondi',
                'email'=>'ibrahim@deveint.com',
                'phone_number'=>'25483473005',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),
            ],
            [
                'first_name'=>'Hukka',
                'last_name'=>' Wario',
                'email'=>'hukka@deveint.com',
                'phone_number'=>'25453473005',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),
            ],

        ]);
    }
}
