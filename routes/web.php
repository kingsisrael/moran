<?php

use App\Http\Controllers\API\ApiMpesaController;
use App\Http\Controllers\BookPaymentController;
use App\Http\Controllers\JambopayController;
use App\Http\Controllers\PaymentsController;
use App\Http\Controllers\PesaPalController;
use App\Http\Controllers\TransactionController;
use App\Models\Book;
use App\Models\BookPayment;
use App\Models\Jambopay;
use App\Models\User;
use App\Models\UserLibrary;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('moran', function () {
    return \Illuminate\Support\Facades\Redirect::to('admin/login');
});

Route::get('/', function () {
    return \Illuminate\Support\Facades\Redirect::to('admin/login');
//    return view('welcome');
});
//Route::get('/dashboard', function () {
//    return view('dashboard');
//})->middleware(['auth'])->name('dashboard');
Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();

    return redirect('/home');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::post('/callbacks', [PesaPalController::class, 'callbacks'])->name('callbacks');
Route::post('/mpesa/callback', [ApiMpesaController::class, 'callback'])->name('callback');

require __DIR__.'/auth.php';

Route::get('/redirect', [PaymentsController::class, 'paymentsuccess'])->name('redirect');
Route::get('/redirects', [\App\Http\Controllers\CallbackController::class, 'redirect'])->name('redirects');

Route::get('/listenipn', [PaymentsController::class, 'paymentconfirmation'])->name('listenipn');
Route::get('/jambopay/{user_id}/{amount}/{book_id}/{name}', function (Request $request) {
    $user_library = UserLibrary::where('user_id', $request->user_id)->where('book_id', $request->book_id)->first();
    if ($user_library) {
        return view('book-bought');
    }
    return view('welcome');
});
Route::post('pay', [JambopayController::class,'jambopay']);

Route::post('/checkout', [JambopayController::class,'checkout'])->name('checkout');
Route::post('/successful-transaction', [JambopayController::class, 'showSuccessfulTransaction']);
Route::post('/cancel-transaction', [JambopayController::class, 'cancelTransaction']);

Route::get('privacy-policy', function () {
    return view('privacy-policy');
});


