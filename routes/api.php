<?php

use App\Models\Book;
use App\Models\User;
use App\Models\BookPayment;
use App\Models\UserFavorite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\UploadController;
use App\Http\Controllers\PesaPalController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\JambopayController;
use App\Http\Controllers\Admin\BookController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\API\ProfileController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\API\ApiMpesaController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\API\NewPasswordController;
use App\Http\Controllers\UserNotificationController;
use App\Http\Controllers\API\EmailVerificationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::middleware('auth:sanctum','verified')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::middleware(['auth:sanctum', 'auth.session'])->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [AuthController::class, 'logins']);
Route::post('register', [AuthController::class, 'registers']);
Route::post('logout', [AuthController::class, 'logouts'])->middleware('auth:sanctum');
Route::post('/verify', [AuthController::class, 'verify']);
Route::post('/edit-profile', [AuthController::class, 'editProfile'])->middleware(['auth:sanctum', 'auth.session']);
Route::post('/add-user-level-two', [ProfileController::class, 'saveCategory'])->middleware(['auth:sanctum', 'auth.session']);
Route::post('/upload-profile',  [AuthController::class, 'upload'])->middleware(['auth:sanctum', 'auth.session']);

Route::post('forgot-password', [AuthController::class, 'forgotPassword']);
Route::post('confirm-otp', [AuthController::class, 'submitResetPasswordForm']);
Route::post('reset-password', [AuthController::class, 'ResetPassword']);

// Route::post('/register', [AuthController::class, 'register']);
// Route::post('/login', [AuthController::class, 'login']);
// Route::post('logout', [AuthController::class, 'logout'])->middleware('auth:api');
Route::post('/buy-book', [ApiMpesaController::class, 'tests'])->middleware('auth:sanctum');
// Route::post('/buy-book/test', [ApiMpesaController::class, 'tests']);
Route::get('/all-book', [ProfileController::class, 'get_level_two_books'])->middleware(['auth:sanctum', 'auth.session']);
Route::post('/paypals', [PesaPalController::class, 'pesapal']);
Route::get('/level-two', [ProfileController::class, 'levelTwo']);
Route::get('/get_user_books', [ProfileController::class, 'get_user_books'])->middleware(['auth:sanctum', 'auth.session']);

// Save User Token
Route::post('/user/token/save', [ProfileController::class, 'saveToken'])->middleware(['auth:sanctum', 'auth.session'])->name('save.user.token');

// Get Book
Route::get('/book/{id}/show', [ProfileController::class, 'book'])->name('book.show')->middleware(['auth:sanctum', 'auth.session']);

//get user books
Route::post('/main-category-books', [ProfileController::class, 'mainCategoryBooks']);
Route::post('/jambopay',[ProfileController::class, 'jambopay']);
Route::get('/get_user_level_two_books',[ProfileController::class, 'levelTwoBooks'])->middleware(['auth:sanctum', 'auth.session']);
Route::post('/update_ratings',[ProfileController::class, 'saveRating']);

// User favorites controller
Route::middleware(['auth:sanctum', 'auth.session'])->group(function() {
    Route::get('/favorites', [ProfileController::class, 'getUserFavoriteBooks']);
    Route::post('/add/favorites', [ProfileController::class, 'addBookToFavorites']);
    Route::post('/remove/favorites', [ProfileController::class, 'deleteBookFromFavorites']);
});

//Route::post('/try', [ApiMpesaController::class, 'stkPush']);
//Route::post('/test', [ApiMpesaController::class, 'tests']);
//Route::get('/token', [JambopayController::class, 'accessToken']);

//Route::get('/mpesa/callback', [ApiMpesaController::class, 'callback'])->name('callback');
Route::get('/category', [ProfileController::class, 'category']);
Route::get('/books', [ProfileController::class, 'books']);
Route::post('/paypal', [TransactionController::class, 'payment']);
// Route::post('social/login', [AuthController::class, 'socialLogin']);

Route::post('social/login',[AuthController::class, 'oauth_login']);
Route::post('book/rating',[ProfileController::class, 'rating']);
Route::get('read/book',[UploadController::class, 'reading']);

Route::get('/user/notifications', [UserNotificationController::class, 'getUserNotifications']);
Route::get('/user/notification/{id}', [UserNotificationController::class, 'getUserNotification']);
Route::post('/user/notification/read', [UserNotificationController::class, 'markNotificationAsRead']);
Route::get('/user/notifications/read/all', [UserNotificationController::class, 'markAllNotificationsAsRead']);

Route::get('/book_payments', function() {
    $book_payments = BookPayment::where('status', true)->orderBy('transactionDate', 'DESC')->get();
    return response()->json($book_payments, 200);
});

Route::post('/successful-transaction', [JambopayController::class, 'showSuccessfulTransaction']);
Route::post('/cancel-transaction', [JambopayController::class, 'cancelTransaction']);

Route::post('send-sms', [AuthController::class, 'sendSms']);
Route::get('books/all', function () {
    $books = Book::where('url', '!=', null)
                ->orWhere('url', '!=', '')
                ->get();

    $filtered_books = [];

    foreach ($books as $key => $book) {
        if (file_exists(public_path('images/'.$book->image))) {
            array_push($filtered_books, $book);
        }
    }
    $user_library = UserFavorite::select('book_id')->where('user_id', auth()->user()->id)->get();
    foreach ($filtered_books as $book) {
        if($user_library->contains($book->id)) {
            $book['inFavorites'] = true;
        } else {
            $book['inFavorites'] = false;
        }
    }

    return response()->json($filtered_books, 200);
})->middleware(['auth:sanctum', 'auth.session']);

Route::get('/book_payments/success', function() {
    $type = BookPayment::where('status', true)->orderBy('updated_at', 'DESC')->get();

    foreach ($type as $key => $transaction) {
        $type['user_details'] = $transaction->user->first_name.' '.$transaction->user->last_name;
    }
    return response()->json($type, 200);
});

Route::get('/book_payments/failed', function() {
    $type = BookPayment::where('status', false)->orderBy('updated_at', 'DESC')->get();

    return response()->json($type, 200);
});

Route::post('/logger', [UserController::class, 'logger']);

Route::get('/account/delete', [ProfileController::class, 'deleteAccount'])->middleware(['auth:sanctum', 'auth.session']);