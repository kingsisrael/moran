<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\AuthorController;
use App\Http\Controllers\Admin\BookController;
use App\Http\Controllers\API\ProfileController;
use App\Http\Controllers\JambopayController;
use App\Http\Controllers\LevelFourCategoryController;
use App\Http\Controllers\LevelThreeCategoryController;
use App\Http\Controllers\LevelTwoCategoryController;
use App\Http\Controllers\MainCategoryController;
use App\Http\Controllers\PaymentsController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\PublisherController;
use App\Http\Controllers\UploadController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\LogsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return \Illuminate\Support\Facades\Redirect::to('admin/dashboard');
});
//homepage
Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard');
//
Route::get('/test', [ProfileController::class, 'get_level_two'])->name('payment');
//
Route::get('/demo', [PaymentsController::class, 'payment'])->name('demo');
Route::get('/jambopay', function () {
    return view('welcome');
});
Route::get('/payment', function () {
    return view('admin.users.payment');
})->middleware(['auth'])->name('payment');

//Route::post('/user-payment', [PesaPalController::class, 'pesapalIframe'])->name('user-payment');

//Route::post('/dashboard', [HomeController::class, 'index'])->name('dashboard.post');
Route::get('/dashboard/monthly-purchases-filter', [HomeController::class, 'disbursed_payments_filter'])->name('dashboard.disbursed_payments_filter');
Route::post('/dashboard/monthly-purchases-filter', [HomeController::class, 'disbursed_payments_filter'])->name('dashboard.post');

//buyers
Route::resource('app-users', UserController::class);
Route::get('app-user', [UserController::class, 'userData'])->name('user.fetchdata');
Route::post('app-user-notify', [UserController::class, 'userNotify'])->name('user.notify');
Route::get('weekly-sales',  [UserController::class, 'weekly'])->name('weekly-sales');
Route::get('books-bought',  [UserController::class, 'bought'])->name('books-bought');

//Route::get('/test', [TransactionController::class, 'payment'])->name('test');

//admin status
Route::post('update-status-admin/{id}', [AdminController::class,'statusUpdate'])->name('status.update');
//jambopay

//app-admins
Route::resource('app-admins', AdminController::class);
Route::resource('languages', LanguageController::class);
Route::resource('publishers', PublisherController::class);
Route::post('delete-admin/{id?}', [AdminController::class,'delete']);

//books
Route::resource('catalogue-books', BookController::class);
Route::get('book/delete/{id}', [BookController::class, 'deleteBook'])->name('book.delete');
Route::post('/books/author/add', [BookController::class, 'addAuthor'])->name('books.author.add');
Route::get('/book/{id}/update/status', [BookController::class, 'changeStatus'])->name('book.status.change');

//authors
Route::resource('catalogue-authors', AuthorController::class);
Route::resource('main-categories', MainCategoryController::class);
Route::resource('level-two-categories', LevelTwoCategoryController::class);
Route::resource('level-three-categories', LevelThreeCategoryController::class);
Route::resource('level-four-categories', LevelFourCategoryController::class);
Route::resource('upload', UploadController::class);
Route::post('files/upload/large', [UploadController::class, 'uploadLargeFiles'])->name('files.upload.large');

// DRM Testing
Route::get('drm/upload', function() {
    return view('book-upload');
});

Route::post('files/upload/large/test', [UploadController::class, 'uploadLargeFilesTest'])->name('files.upload.test');

//authors

//user-management
Route::get('catalogue', [HomeController::class, 'viewProviders'])->name('catalogue.index');
Route::get('catalogue/edit', [HomeController::class, 'viewProviders'])->name('catalogue.edit');
Route::get('catalogue/show', [HomeController::class, 'viewProviders'])->name('catalogue.show');
Route::get('catalogue/delete/{id}', [HomeController::class, 'deletePost'])->name('catalogue.delete');



Route::get('languages/delete/{id}', [HomeController::class, 'deletelanguages'])->name('languages.delete');
Route::get('publishers/delete/{id}', [HomeController::class, 'deletepublishers'])->name('publishers.delete');
Route::get('publishe/delete/{id}', [HomeController::class, 'deleteAuthors'])->name('authors.delete');

//profile update
Route::resource('profile', ProfileController::class);
//transaction
Route::resource('mpesa', TransactionController::class);
Route::get('mpesa-failed', [TransactionController::class, 'failedMpesaTransactions'])->name('mpesa.transactions.failed');
Route::get('jambopay', [TransactionController::class, 'pesapal'])->name('jambopay');

Route::get('approve-transaction/{reference_number}', [TransactionController::class, 'approve'])->name('approve.transaction');

//default list
Route::get('/default', [UserController::class, 'default'])->name('default');

//Route::get('get-mpesa-transactions', [MpesaTransactionController::class, 'get_mpesa_transactions'])->name('get-mpesa-transactions');
//here
Route::get('services/two-datatables', 'AdminController@getUsersDataTables');
Route::get('services/two-datatables/posts','AdminController@getPostsDataTables');
//mpesa dummy
Route::get('mpesa-data', [AdminController::class, 'mpesaData'])->name('mpesa.data');
Route::get('mpesa-data-failed', [AdminController::class, 'mpesaDataFailed'])->name('mpesa.data.failed');
Route::get('pesapal-data', [AdminController::class, 'PalData'])->name('pesapal.data');

Route::get('/getSubCounty/{id}', [AdminController::class, 'getSubCounty'])->name('getSubCounty');
Route::get('/getSubThree/{id}', [AdminController::class, 'getSubThree'])->name('getSubThree');
Route::get('/getSubFour/{id}', [AdminController::class, 'getSubFour'])->name('getSubFour');
Route::post('add-main-category', [BookController::class, 'mainCategoty'])->name('add-main-category');
Route::post('add-main-category-two', [BookController::class, 'mainCategotyTwo'])->name('add-main-category-two');
Route::post('add-main-category-three', [BookController::class, 'mainCategotyThree'])->name('add-main-category-three');
Route::post('add-language', [LanguageController::class, 'mainLanguage'])->name('add-language');
Route::post('add-publisher', [PublisherController::class, 'mainPublisher'])->name('add-publisher');
Route::get('/get_user_level_two_books',[ProfileController::class, 'levelTwoBooks']);
Route::get('/get-transaction', [AdminController::class, 'getTransaction'])->name('ajaxdata.fetchtransaction');
Route::post('update-transaction', [AdminController::class, 'updateTransaction'])->name('ajaxdata.updatetransaction');

//edit main category
//edit
Route::post('ajaxdata/postdata', [HomeController::class, 'postdata'])->name('ajaxdata.postdata');
Route::post('ajaxdata/postdatatwo', [HomeController::class, 'postdatatwo'])->name('ajaxdata.postdatatwo');
Route::post('ajaxdata/postdatathree', [HomeController::class, 'postdatathree'])->name('ajaxdata.postdatathree');
Route::post('ajaxdata/postdatafour', [HomeController::class, 'postdatafour'])->name('ajaxdata.postdatafour');

Route::get('ajaxdata/fetchdatas', [LanguageController::class, 'fetchdatas'])->name('ajaxdata.fetchdatas');
Route::get('ajaxdata/main', [MainCategoryController::class, 'fetchdata'])->name('ajaxdata.main');
Route::get('ajaxdata/two', [LevelTwoCategoryController::class, 'fetchdata'])->name('ajaxdata.two');
Route::get('ajaxdata/three', [LevelThreeCategoryController::class, 'fetchdata'])->name('ajaxdata.three');
Route::get('ajaxdata/four', [LevelFourCategoryController::class, 'fetchdata'])->name('ajaxdata.four');


Route::post('ajaxdata/postdatas', [LanguageController::class, 'postLanguage'])->name('ajaxdata.postdatas');

Route::get('ajaxdata/fetchdatass', [PublisherController::class, 'fetchdatass'])->name('ajaxdata.fetchdatass');
Route::post('ajaxdata/postdatass', [PublisherController::class, 'postLanguages'])->name('ajaxdata.postdatass');
Route::post('destroy/{id}', [PublisherController::class, 'destroys']);
//
//Route::get('ajaxdata/fetchdatasss', [PublisherController::class, 'fetchdatasss'])->name('ajaxdata.fetchdatasss');
//Route::post('ajaxdata/postdatasss', [PublisherController::class, 'postLanguagess'])->name('ajaxdata.postdatasss');
//Route::post('destroy/{id}', [PublisherController::class, 'destroys']);


Route::get('ajaxdata/fetchdat', [AuthorController::class, 'fetchdat'])->name('ajaxdata.fetchda');
Route::post('admin/author/add', [AuthorController::class, 'addAuthor'])->name('add.author');
Route::get('delete-author/{id}', [AuthorController::class, 'delete'])->name('catalogue-authors.delete');

Route::post('destroy/{id}', [HomeController::class, 'destroy']);

//datatable routes
Route::prefix('datatables')->group(function () {
    Route::get('get-app-users', [UserController::class, 'getUsers'])->name('get-app-users');
    Route::get('get-admin-users', [AdminController::class, 'getAdminUsers'])->name('get-admin-users');
    Route::get('get-app-authors', [AuthorController::class, 'getAuthors'])->name('get-app-authors');
    Route::get('get-app-books', [BookController::class, 'getBooks'])->name('get-app-books');
    Route::get('get-app-languages', [LanguageController::class, 'getLanguages'])->name('get-app-languages');
    Route::get('get-app-publishers', [PublisherController::class, 'getpublishers'])->name('get-app-publishers');
    Route::get('get-most-bought-books', [BookController::class, 'getMostBoughtBooks'])->name('get-most-bought-books');

    Route::get('get-app-admins', [AdminController::class, 'getAdmins'])->name('get-app-admins');
    // Route::get('get-app-transactions', [TransactionController::class, 'getTransactions'])->name('get-app-admins');

    Route::get('get-catalogue', [HomeController::class, 'getCategory'])->name('get-catalogue');
    Route::get('get-catalogue-two', [HomeController::class, 'getCategoryTwo'])->name('get-catalogue-two');
    Route::get('get-catalogue-three', [HomeController::class, 'getCategoryThree'])->name('get-catalogue-three');
    Route::get('get-catalogue-four', [HomeController::class, 'getCategoryFour'])->name('get-catalogue-four');
    Route::get('get-transactions/{id}', [TransactionController::class, 'getUsersTransactions'])->name('get-transactions');
    Route::get('get-jambopay-transactions/{id}', [TransactionController::class, 'getUsersJambopayTransactions'])->name('get-jambopay-transactions');
    Route::get('get-user-books/{id}', [UserController::class, 'getUserBooks'])->name('get-user-books');
    Route::post('add-user-book', [UserController::class, 'addUserBook'])->name('add-user-book');
});
//charts routes
Route::prefix('charts')->group(function () {
    //for charts
   Route::get('/transaction-chart-analytics/{month}/{year}', [HomeController::class, 'getMonthlyTransactionsData'])->name('discussions-analytics');
    //area chart for transaction
   Route::get('/analytics-area/{month?}/{year?}', [HomeController::class, 'getMonthlyPostDataWeekly'])->name('discussions-analytics');

   Route::get('/users-per-month', [HomeController::class, 'totalUsersPerMonth'])->name('users-per-month-data');

});

Route::post('/leveltwocategory/delete', [LevelTwoCategoryController::class, 'destroy'])->name('leveltwocategory.delete');
Route::post('/levelthreecategory/delete', [LevelThreeCategoryController::class, 'destroy'])->name('levelthreecategory.delete');
Route::post('/levelfourcategory/delete', [LevelFourCategoryController::class, 'destroy'])->name('levelfourcategory.delete');

Route::get('/logs/general', [LogsController::class, 'generalLogs'])->name('logs.general');
Route::get('/logs/transactions', [LogsController::class, 'transactionsLogs'])->name('logs.transactions');
Route::get('/datatables/get-general-logs', [LogsController::class, 'generalLogsTabledata']);
Route::get('/datatables/get-transactions-logs', [LogsController::class, 'transactionsLogsTabledata']);

//test
//Route::get('/test', [HomeController::class, 'getAllMonthsUsers'])->name('test');

require __DIR__.'/admin_auth.php';
