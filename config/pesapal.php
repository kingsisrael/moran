<?php

return [
    /*
     * Pesapal consumer key
     */
    'consumer_key'    => '8plKT0y3q7PPEGtLXYc22FFvE4KEBEUJ',

    /*
     * Pesapal consumer secret
     */
    'consumer_secret' => 'qf3bi+dBUl+C+gTb6F2AOKLKKz0=',

    /*
     * ISO code for the currency
     */
    'currency'        => env('PESAPAL_CURRENCY', 'KES'),

    /*
     * controller method to call for instant notifications IPN as relative path from App\Http\Controllers\
     * eg "TransactionController@confirmation"
     */
    'ipn'             => env('PESAPAL_IPN'),

    /*
     * Pesapal environment
     */
    'live'            => env('PESAPAL_LIVE', true),

    /*
     * Route name to handle the callback
     * eg Route::get('donepayment', ['as' => 'paymentsuccess', 'uses'=>'PaymentsController@paymentsuccess']);
     * The route name is "paymentsuccess"
     */
    'callback_route'  => 'http://127.0.0.1:8000/redirect',

];
