<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'firebase_credentials' => [
        'fcm_key' => env('FCM_TOKEN'),
        'server_key' => env('GOOGLE_FIREBASE_SERVER_KEY')
    ],

    'facebook' => [
        'client_id' => '910454366315625',
        'client_secret' => '76d4d685e76e3cf79011fe05cbe7ba0e',
        'redirect' => 'https://moran-app-b85d1.firebaseapp.com/__/auth/handler',
    ],

    'google' => [
        'client_id' => '693031585756-p6cjr3av5mk4gl908rffab2bo9ais0su.apps.googleusercontent.com',
        'client_secret' => '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',
        'redirect' => 'https://moran-app-b85d1.firebaseapp.com/__/auth/handler',
    ],
    'github' => [
        'client_id' => env('GITHUB_CLIENT_ID'),
        'client_secret' => env('GITHUB_CLIENT_SECRET'),
        'redirect' => 'http://example.com/callback-url',
    ],
];
